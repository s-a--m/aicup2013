import java.io.*;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Genetic {

    static final int[] arraysSizes = new int[]{
            MyStrategy.W_DANGER.length,
            MyStrategy.W_ATTACK.length,
            MyStrategy.W_HEAL.length,
            MyStrategy.W_HP.length,
            MyStrategy.W_MEDIC.length,
            MyStrategy.W_MOVE.length,
            MyStrategy.W_MOVE_ENEMY.length,
            MyStrategy.W_MOVE_TO.length,
            MyStrategy.W_TACTIC.length,
            MyStrategy.W_MOVE_FIND.length,
    };
    static final int PLAYERS = 4;


    static final int MY_STR = 4;
    static final int NUM_TESTS = 5;
    static final boolean isOnlyOne = false;

    static int ETA_MAX = 2;
    static int META_MAX = MY_STR * ETA_MAX;
    static File[] files = new File[META_MAX];
    static double[][] ddd = new double[META_MAX][];
    static double[] r = new double[META_MAX];
    static double[][] results = new double[META_MAX][NUM_TESTS];

    static final int NUM_THREADS = 2;
    static final Executor EXECUTOR = Executors.newFixedThreadPool(NUM_THREADS);
    static final Random rnd = new Random(System.currentTimeMillis());
    static CountDownLatch countDownLatch;

    static int generation = 0;

    static void reInit(int newEta){
   ETA_MAX = newEta;
   META_MAX = MY_STR * ETA_MAX;
   files = new File[META_MAX];
   ddd = new double[META_MAX][];
   r = new double[META_MAX];
   results = new double[META_MAX][NUM_TESTS];
       init();
    }

    static double lastResult = 0;
    public static void main(String[] args) throws InterruptedException {
        if(args.length>0){
            reInit(Integer.parseInt(args[0]));
        }
        init();
        for (; ; ) {
            long t = System.currentTimeMillis();
            double result = doOne();
            t = System.currentTimeMillis()-t;
            System.out.println("generation "+generation+++" t="+t);
           if(lastResult>0){
               if(result<lastResult){
               //lastResult = result;
               reInit(ETA_MAX*2);
               System.out.println("increasing population: "+ETA_MAX);
               }else{
            //   lastResult = 0;
                   lastResult = result;
               int newEta = ETA_MAX/2;
               reInit(newEta>0?newEta:1);
               System.out.println("decreasing population: "+ETA_MAX);
               }
           }else{
              lastResult = result;
           }
        }
    }

    static void init() {
        for (int eta = 0; eta < ETA_MAX; eta++) {
            int meta = MY_STR * eta;
            for (int i = 0; i < MY_STR; i++) {
                files[meta + i] = new File("/home/sss/Загрузки/aicup/local-runner (3)/" + (meta + i + 1) + ".txt");
            }
        }
    }

    private static int imax = 0;
    private static int imin = 0;

    private static double[] allBest = null;

    private static double doOne() throws InterruptedException {

        for (double[] result : results) {
            Arrays.fill(result, 0);
        }


        Arrays.fill(r, 1);

        countDownLatch = new CountDownLatch(ETA_MAX);
        for (int i = 0; i < MY_STR; i++) {
            for (int eta = 0; eta < ETA_MAX; eta++) {
                int meta = MY_STR * eta;
                ddd[meta + i] = readFile(files[meta + i]);
            }
        }



        for (int eta = 0; eta < ETA_MAX; eta++) {
            if(isOnlyOne)
                runAsyncMy(eta);
            else
                runAsync(eta);
        }

        countDownLatch.await();

        imax = 0;
        imin = 0;
        double sum = 0;
        for (int i = 0; i < META_MAX; i++) {
            if (r[i] > r[imax]) {
                imax = i;
            }
            if (r[i] < r[imin]) {
                imin = i;
            }
            sum+=r[i];
        }

        ddd[imin] = ddd[imax];

        if(r[imax]>lastResult){
            allBest= Arrays.copyOf(ddd[imax],ddd[imax].length);
            writeRecord();
        }

        System.out.println("max result: " + r[imax]);
        System.out.println("max result index: " + imax);
        writeMax(imin, imax);

        mutate(imax, imin);

        for (int i = 0; i < META_MAX; i++) {
            writeArrays(files[i], ddd[i]);
        }
        return r[imax];
    }

    private static void writeRecord() {
        //To change body of created methods use File | Settings | File Templates.
                File file = new File("/home/sss/Загрузки/aicup/local-runner (3)/best.txt");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                System.out.println(e.getMessage());
            }
        } else {
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {

                System.out.println(e.getMessage());
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write("Best result: " + r[imax]);
            writer.newLine();
            writer.write("Best result index: " + imax);
            writer.newLine();
            writer.newLine();

            writer.newLine();
            writer.newLine();
            int i = 0;
            int j = 0;
            for (double d : ddd[imax]) {
                if (i++ >= arraysSizes[j]) {
                    j++;
                    i = 1;
                    writer.newLine();
                    writer.newLine();
                }
                writer.write(String.format(Locale.US, "%.4f, ", d));
            }

            writer.newLine();
            writer.newLine();
            writer.newLine();
            writer.newLine();
            writer.write("index \t result");
            writer.newLine();
            for (int k = 0; k < r.length; k++) {
                writer.write(String.format("%d \t %5.2e \t : \t ",k,r[k]));
                for(int a = 0;a<NUM_TESTS;a++){
                    writer.write("\t\t"+results[k][a]);
                }
                writer.newLine();
            }
            writer.flush();
        } catch (Exception ignore) {

            System.out.println(ignore.getMessage());
        }
    }

    private static void readResults(int eta, int portStart, int c) {
        int meta = MY_STR * eta;
        final File fr = new File("/home/sss/Загрузки/aicup/local-runner (3)/results" + portStart + ".txt");
        try (BufferedReader reader = new BufferedReader(new FileReader(fr))) {
            String line;
            reader.readLine();
            reader.readLine();
            for (int i = 0; i < MY_STR; i++) {
                line = reader.readLine();
                if (null == line) {
                    return;
                }
                String[]ss = line.split(" ");
                String s1 = ss[0];
                int t  = 5-Integer.parseInt(s1);
                String s = ss[1];
                String status = ss[2];
                double d = Double.parseDouble(s);
                results[meta + i][c] = d;
                System.out.println(portStart+i+" : "+d+" : "+status);
                r[meta + i] *= d*t;
            }

        } catch (IOException ignore) {
            System.out.println(ignore.getMessage());
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fr, true))) {
            writer.newLine();
            writer.newLine();
            for (int i = 0; i < MY_STR; i++) {
                writer.newLine();
                writer.write(c + "\t\t:\t\t");
                for (int j = 0; j < c + 1; j++) {
                    writer.write("\t\t" + results[meta + i][j]);
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    private static void writeMax(int imin, int imax) {
        File file = new File("/home/sss/Загрузки/aicup/local-runner (3)/max.txt");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                System.out.println(e.getMessage());
            }
        } else {
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {

                System.out.println(e.getMessage());
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write("Best result: " + r[imax]);
            writer.newLine();
            writer.write("Best result index: " + imax);
            writer.newLine();
            writer.newLine();

            writer.newLine();
            writer.newLine();
            int i = 0;
            int j = 0;
            for (double d : ddd[imax]) {
                if (i++ >= arraysSizes[j]) {
                    j++;
                    i = 1;
                    writer.newLine();
                    writer.newLine();
                }
                writer.write(String.format(Locale.US, "%.4f, ", d));
            }

            writer.newLine();
            writer.newLine();
            writer.newLine();
            writer.newLine();
            writer.write("index \t result");
            writer.newLine();
            for (int k = 0; k < r.length; k++) {
                writer.write(String.format("%d \t %5.2e \t : \t ",k,r[k]));
                for(int a = 0;a<NUM_TESTS;a++){
                    writer.write("\t\t"+results[k][a]);
                }
                writer.newLine();
            }
            writer.flush();
        } catch (Exception ignore) {

            System.out.println(ignore.getMessage());
        }
    }

    private static void mutate(int maxi, int mini) {
        for (int i = 0; i < META_MAX; i++) {
            boolean isGood = r[i] >  r[maxi] / 10;
            if(isGood)
                continue;
            if (i != maxi)
                for (int j = 0; j < ddd[0].length; j++) {

                    ddd[i][j] = ddd[maxi][j];


                    if (rnd.nextDouble() > 0.2) {
                        ddd[i][j] += (rnd.nextDouble() - 0.5d) ;
                    }

                }
        }
    }

      private static void runAsyncMy(final int eta) {
          if(MY_STR>1){
              throw new RuntimeException("use runAsync");
          }
        final int meta = MY_STR * eta;
        EXECUTOR.execute(
                new Runnable() {
                    @Override
                    public void run() {

                        for (int c = 0; c < NUM_TESTS; c++) {
                                final String port = Integer.toString(1 + meta);
                                final String fileName =files[meta].getAbsolutePath();

                            int portStart = 31001 + meta;
                            runRunner( "results" + portStart + ".txt",fileName );
                            readResults(eta, portStart, c);
                        }
                        System.out.printf("%d ended: %6.2e \n",eta,r[eta]);
                        countDownLatch.countDown();
                    }
                });
    }

    private static void runAsync(final int eta) {
        final int meta = MY_STR * eta;
        EXECUTOR.execute(
                new Runnable() {
                    @Override
                    public void run() {

                        for (int c = 0; c < NUM_TESTS; c++) {
                            for (int i = 1; i < MY_STR + 1; i++) {
                                final String port = Integer.toString(31000 + i + meta);
                                final String fileName = (meta + i) + ".txt";
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        runStrategy(port, fileName);
                                    }
                                }).start();
                            }
                            int portStart = 31001 + meta;
                            runLocalRunner(portStart);
                            readResults(eta, portStart, c);
                        }
                        System.out.printf("%d ended: %6.2e \n",eta,r[eta]);
                        countDownLatch.countDown();
                    }
                });
    }

    private static double[] readFile(File file) {
        //       System.out.println(file.getAbsolutePath());
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                System.out.println(e.getMessage());
            }
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String s = reader.readLine();
            String[] ss = null;
            if (null != s)
                ss = s.split(",");
            if (null != ss && ss.length > 1) {
                double[] dd = new double[ss.length];
                for (int i = 0; i < ss.length; i++) {
                    dd[i] = Double.parseDouble(ss[i]);
                }
                return dd;
            } else {
                return readMutateAndWriteArrays(file);
            }


        } catch (IOException ignore) {
            System.out.println(ignore.getMessage());
        }
        return null;
    }

    private static double[] readMutateAndWriteArrays(File file) {
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                System.out.println(e.getMessage());
            }
        } else {
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {

                System.out.println(e.getMessage());
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        try (FileWriter writer = new FileWriter(file)) {
            if(null==allBest){

            writeMutateArray(writer, MyStrategy.W_DANGER);
            writeMutateArray(writer, MyStrategy.W_ATTACK);
            writeMutateArray(writer, MyStrategy.W_HEAL);
            writeMutateArray(writer, MyStrategy.W_HP);
            writeMutateArray(writer, MyStrategy.W_MEDIC);
            writeMutateArray(writer, MyStrategy.W_MOVE);
            writeMutateArray(writer, MyStrategy.W_MOVE_ENEMY);
            writeMutateArray(writer, MyStrategy.W_MOVE_TO);
            writeMutateArray(writer, MyStrategy.W_TACTIC);
            writeMutateArray(writer, MyStrategy.W_MOVE_FIND);
            }else{
                writeMutateArray(writer,allBest);
            }

            writer.flush();
            System.out.println("genom has been mutated and writed into file " + file.getAbsolutePath());
        } catch (Exception ignore) {

            System.out.println(ignore.getMessage());
        }
        return readFile(file);
    }

    private static void writeMutateArray(FileWriter writer, double[] arr) throws IOException {
        for (double d : arr) {
            writer.write(String.valueOf(d+rnd.nextDouble()-0.5d) + ",");
        }
    }


    private static void writeArray(FileWriter writer, double[] arr) throws IOException {
        for (double d : arr) {
            writer.write(String.valueOf(d) + ",");
        }
    }

    private static void writeArrays(File file, double[] dd) {
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                System.out.println(e.getMessage());
            }
        } else {
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {

                System.out.println(e.getMessage());
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {

            writeArray(writer, dd);

            writer.flush();
        } catch (Exception ignore) {

            System.out.println(ignore.getMessage());
        }
    }

    private static void writeArray(BufferedWriter writer, double[] arr) throws IOException {
        for (double d : arr) {
            writer.write(String.valueOf(d) + ",");
        }
    }

    public static void runLocalRunner(int portStart) {
        ProcessBuilder pb = new ProcessBuilder();
        pb.inheritIO();
        List<String> commands = new ArrayList<>();
        commands.add("java");
        commands.add("-cp");
        commands.add(".:local-runner.jar");
        commands.add("Run");
        commands.add("-move-count=50");
        commands.add("-render-to-screen=false");
        commands.add("-render-to-screen-scale=1.0");
        commands.add("-render-to-screen-sync=false");
        commands.add("-results-file=results" + portStart + ".txt");
        commands.add("-debug=false");
        commands.add("-base-adapter-port=" + portStart);
        commands.add("-p1-name=p1");
        commands.add("-p2-name=p2");
        commands.add("-p3-name=p3");
        commands.add("-p4-name=p4");
        commands.add("-p1-team-size=3");
        commands.add("-p2-team-size=3");
        commands.add("-p3-team-size=3");
        commands.add("-p4-team-size=3");
        for (int i = 0; i < MY_STR; i++) {
            commands.add("#LocalTestPlayer");
        }
        for (int j = 0; j < PLAYERS - MY_STR; j++) {
            commands.add("com.a.b.a.a.e.a.class");
        }
        pb.directory(new File("/home/sss/Загрузки/aicup/local-runner (3)/"));

        pb.command(commands);
        Process process;
        try {
            process = pb.start();
        //    System.out.println("runner started " + portStart);
            readStream(process);
            process.waitFor();
        } catch (IOException | InterruptedException e) {
            System.out.println("eRrOr! " + e.getMessage());
            e.printStackTrace();
        }

    }

    private static void readStream(Process process) {
        try (BufferedReader reader =
                     new BufferedReader(new InputStreamReader(process.getInputStream()))) {
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println("runner: " + line);
            }

        } catch (Exception e) {
            System.out.println("error" + e.getMessage());
        }
    }

    public static void runStrategy(String port, String gen) {
        ProcessBuilder pb = new ProcessBuilder();
        pb.inheritIO();
        List<String> commands = new ArrayList<>();
        commands.add("java");
        commands.add("-cp");
        commands.add("/home/sss/Загрузки/aicup/java-cgdk/out/production/java-cgdk/");
        commands.add("Runner");
        commands.add("localhost");
        commands.add(port);
        commands.add("0000000000000000");
        commands.add(gen);
        pb.directory(new File("/home/sss/Загрузки/aicup/local-runner (3)/"));
        pb.command(commands);
        try {
            Process process = pb.start();
           // System.out.println("strategy started " + port);
            readStream(process);
            process.waitFor();
        } catch (IOException | InterruptedException e) {
            System.out.println("eRrOr! " + e.getMessage());
            e.printStackTrace();
        }

    }


    public static void runRunner(String filename, String results) {
        ProcessBuilder pb = new ProcessBuilder();
        pb.inheritIO();
        List<String> commands = new ArrayList<>();
        commands.add("java");
        commands.add("-jar");
        commands.add("/home/sss/Загрузки/aicup/java-cgdk/out/artifacts/localrunners/localrunners.jar");
        commands.add(filename);
        commands.add(results);
        pb.directory(new File("/home/sss/Загрузки/aicup/local-runner (3)/"));
        pb.command(commands);
        try {
            Process process = pb.start();
           // System.out.println("strategy started " + port);
            readStream(process);
            process.waitFor();
        } catch (IOException | InterruptedException e) {
            System.out.println("eRrOr! " + e.getMessage());
            e.printStackTrace();
        }

    }}
