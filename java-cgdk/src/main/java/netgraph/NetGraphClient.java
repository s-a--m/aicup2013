package netgraph;

import model.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.round;

public class NetGraphClient {
    private static final int SCALE = 40;
    BufferedReader in;
    PrintWriter out;
    World world;
    boolean firstRun = true;
    private Socket socket;

    public NetGraphClient(String host, int port) {
        connect(host, port);
    }

    private void connect(String host, int port) {
        try {
            InetAddress ipAddress = InetAddress.getByName(host);
            socket = new Socket(ipAddress, port);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);
            System.out.println(in.readLine());
        } catch (IOException e) {
            runGraphServer();
            try {
                Thread.sleep(300);
            } catch (InterruptedException ez) {
            }
                connect(host, port);
        }
    }

    private void runGraphServer() {
       ProcessBuilder pb = new ProcessBuilder();
        List<String> list = new ArrayList<>();
        list.add("java");
        list.add("-cp");
        list.add("/home/sss/Загрузки/aicup/java-cgdk/out/production/java-cgdk/");
        list.add("netgraph.NetGraphServer");
        pb.command(list);
        pb.inheritIO();
        try {
            pb.start();
        } catch (IOException e) {
           System.out.println("could not start graph server"+e.getMessage());
        }
    }

    public void sendCommand(String format, Object... args) {
        sendCommand(String.format(format, args));
    }

    public void sendCommand(String command) {
        try {
            out.println(command);
            String result = in.readLine();
           // if (!result.equals("OK"))
          //      System.out.println(result);
        } catch (IOException e) {
            socket = null;
            e.printStackTrace();
        }
    }

    public void drawUnit(Unit u) {
        drawCell(u.getX(), u.getY());
    }

    public void drawTrooper(Trooper t) {

        double tx = t.getX() + 0.5;
        double ty = t.getY() + 0.5;
        double hp = (double)t.getHitpoints();
        double maxHp = (double)t.getMaximalHitpoints();
        double hpp = hp/maxHp;
        drawSquare(t.getX(), t.getY()+1-hpp, 0.2,hpp, Colors.RED);
        drawString(String.valueOf(hpp),t.getX(),t.getY());
         if (t.isTeammate()) {
            setColor(Colors.GREEN);
        } else {
            setColor(Colors.YELLOW);
        }
        drawCircle(tx, ty, 0.3);
        double r = t.getShootingRange();
        drawCircle(tx, ty, r);
        String stance;
        switch (t.getStance()) {
            case KNEELING:
                stance = "k";
                break;
            case PRONE:
                stance = "p";
                break;
            case STANDING:
            default:
                stance = "s";
                break;
        }
        drawString(stance, tx, ty);
    }

    public void drawBonus(Bonus b) {
        setColor(Colors.CYAN);
        drawUnit(b);
        double bx = b.getX() + 0.5;
        double by = b.getY() + 0.5;
        switch (b.getType()) {
            case MEDIKIT:
                drawString("m", bx, by);
                break;
            case FIELD_RATION:
                drawString("r", bx, by);
                break;
            case GRENADE:
                drawString("g", bx, by);
                break;
        }
    }

    protected void drawCircle(double x, double y, double r) {
        sendCommand(String.format("drawArc %d %d %d %d %d %d", round((x - r) * SCALE), round((y - r) * SCALE), round(2 * r * SCALE), round(2 * r * SCALE), 0, 360));
    }

    protected void drawLine(double x1, double y1, double x2, double y2) {
        sendCommand("drawLine %d %d %d %d", round(x1 * SCALE), round(y1 * SCALE), round(x2 * SCALE), round(y2 * SCALE));
    }

    protected void setColor(String color) {
        sendCommand("setColor %s", color);
    }

    public void setColor(Colors c) {
        setColor(c.toString());
    }

    protected void clearRect(double x, double y, double width, double height) {
        sendCommand("clearRect %d %d %d %d", round(x * SCALE), round(y * SCALE), round(width * SCALE), round(height * SCALE));
    }

    public void drawString(String string, double x, double y) {
        sendCommand("drawString %s %d %d", string, round(x * SCALE), round(y * SCALE));
    }

    protected void resizeImage(double w, double h) {
        sendCommand("resizeImage %s %d", round(w * SCALE), round(h * SCALE));
    }

    public void update(World world) {
        try {
            Thread.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        sendCommand("update");
    }

    public void drawSquare(double x, double y, double w, double h, Colors c) {
        setColor(c);
        sendCommand(String.format("fill3DRect %d %d %d %d false", Math.round(x * SCALE), Math.round(y * SCALE)
                , Math.round(w * SCALE), Math.round(h * SCALE)));
    }

    private void drawWorld(World world) {
        drawSquare(0, 0, world.getWidth(), world.getHeight(), Colors.WHITE);
        drawCells(world);
        for (final Bonus bonus : world.getBonuses()) {
            drawBonus(bonus);
        }
        for (final Trooper trooper : world.getTroopers()) {
            drawTrooper(trooper);
        }
    }

    public void drawCell(int x, int y) {
        x += 0.1;
        y += 0.1;
        double x2 = (x + 0.9);
        double y2 = (y + 0.9);

        drawLine(x, y, x, y2);
        drawLine(x, y, x2, y);
        drawLine(x2, y, x2, y2);
        drawLine(x, y2, x2, y2);
    }

    public void drawFilledCell(int x, int y, Colors c) {
        drawSquare(x, y, 0.9, 0.9, c);
    }

    private void drawCells(World world) {
        CellType[][] cells = world.getCells();
        for (int x = 0; x < cells.length; x++) {
            for (int y = 0; y < cells[x].length; y++) {
                switch (cells[x][y]) {
                    case FREE:
                        drawFilledCell(x, y, Colors.WHITE);
                        break;
                    case MEDIUM_COVER:
                        drawFilledCell(x, y, Colors.GRAY);
                        break;
                    case LOW_COVER:
                        drawFilledCell(x, y, Colors.LIGHT_GRAY);
                        break;
                    case HIGH_COVER:
                        drawFilledCell(x, y, Colors.BLACK);
                        break;
                }
            }
        }
    }



    public void drawSquare(double v, double v1, double v2, double v3) {
       drawSquare(v,v1,v2,v3,Colors.BLACK);
    }

    public void begin(World world) {
         if (socket == null)
            return;
        if (firstRun) {
            resizeImage(world.getWidth(), world.getHeight());
            firstRun = false;
        }
        this.world = world;
        try {
            Thread.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        drawWorld(world);
        sendCommand("update");
    }

    public void setColorRed() {
        setColor(Colors.RED);
    }

    public void setColorBlack() {
        setColor(Colors.BLACK);
    }

    public void setColorCyan() {
        setColor(Colors.CYAN);
    }

    public void setColorBlue() {
        setColor(Colors.BLUE);
    }


    enum Colors {
        RED("red"), GREEN("green"), BLUE("blue"), WHITE("white"), BLACK("black"),
        GRAY("gray"), LIGHT_GRAY("lightGray"), CYAN("cyan"), YELLOW("yellow"),
        PINK("pink"), ORANGE("orange"), MAGENTA("magenta");
        private final String name;

        Colors(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
