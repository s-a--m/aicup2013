package netgraph;

import javax.swing.*;
import java.io.IOException;

public class NetGraphServer extends JFrame {

    public static final PaintPanel canvas = new PaintPanel();
    public NetGraphServer() {
        setTitle("Net Graph Server");
        setSize(1224, 968);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        add(canvas);
        try {
            new Thread(new Server(8888)).start();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                NetGraphServer ex = new NetGraphServer();
                ex.setVisible(true);

            }
        });
    }
}
