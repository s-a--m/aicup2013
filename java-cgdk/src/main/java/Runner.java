import model.*;

import java.io.*;

public final class Runner {
    private final RemoteProcessClient remoteProcessClient;
    private final String token;
    private final String stoken;

    private Runner(String[] args) throws IOException {
        stoken   =args[3];

        System.out.println("reading file "+stoken);
        readFile(new File(args[3]));
        System.out.println("waiting for connection "+args[1]);
        long t = System.currentTimeMillis();
        remoteProcessClient = new RemoteProcessClient(args[0], Integer.parseInt(args[1]));
        token = args[2];
        System.out.println("connected "+stoken+" "+(System.currentTimeMillis()-t));
    }

    public static void main(String[] args) throws IOException {
        if (args.length > 3) {
           try{
            new Runner(args).run();
           }catch(Exception e){
               System.out.println("exception "+e.getMessage());
               e.printStackTrace();
               throw e;
           }
        } else {
            new Runner(new String[]{"localhost", "31001", "0000000000000000", "debug.txt"}).run();
        }
    }

    private void readFile(File file) {
        System.out.println(file.getAbsolutePath());
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
            }
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String s = reader.readLine();
            String[] ss = null;
            if (null != s)
                ss = s.split(",");
            if (null != ss && ss.length > 1) {
                double[] dd = new double[ss.length];
                for (int i = 0; i < ss.length; i++) {
                    dd[i] = Double.parseDouble(ss[i]);
                }
                readArrays(dd);
            } else {
                writeArrays(file);
            }

        } catch (IOException ignore) {
            System.out.println(ignore.getMessage());
        }
    }

    private void writeArrays(File file) {
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
            }
        } else {
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {

                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        try (FileWriter writer = new FileWriter(file)) {

            writeArray(writer, MyStrategy.W_DANGER);
            writeArray(writer, MyStrategy.W_ATTACK);
            writeArray(writer, MyStrategy.W_HEAL);
            writeArray(writer, MyStrategy.W_HP);
            writeArray(writer, MyStrategy.W_MEDIC);
            writeArray(writer, MyStrategy.W_MOVE);
            writeArray(writer, MyStrategy.W_MOVE_ENEMY);
            writeArray(writer, MyStrategy.W_MOVE_TO);
            writeArray(writer, MyStrategy.W_TACTIC);
            writeArray(writer, MyStrategy.W_MOVE_FIND);

            writer.flush();
        } catch (Exception ignore) {

            System.out.println(ignore.getMessage());
        }
    }

    private void writeArray(FileWriter writer, double[] arr) throws IOException {
        for (double d : arr) {
            writer.write(String.valueOf(d) + ",");
        }
    }

    private void readArrays(double[] dd) {
        int offset = 0;

        offset = readArray(dd, offset, MyStrategy.W_DANGER);
        offset = readArray(dd, offset, MyStrategy.W_ATTACK);
        offset = readArray(dd, offset, MyStrategy.W_HEAL);
        offset = readArray(dd, offset, MyStrategy.W_HP);
        offset = readArray(dd, offset, MyStrategy.W_MEDIC);
        offset = readArray(dd, offset, MyStrategy.W_MOVE);
        offset = readArray(dd, offset, MyStrategy.W_MOVE_ENEMY);
        offset = readArray(dd, offset, MyStrategy.W_MOVE_TO);
        offset = readArray(dd, offset, MyStrategy.W_TACTIC);
        offset = readArray(dd, offset, MyStrategy.W_MOVE_FIND);
    }

    private int readArray(double[] dd, int i, double[] arr) {
        System.arraycopy(dd, i, arr, 0, arr.length);
        return i + arr.length;
    }

    public void run() throws IOException {
        try {
            remoteProcessClient.writeToken(token);
            int teamSize = remoteProcessClient.readTeamSize();
            remoteProcessClient.writeProtocolVersion();
            Game game = remoteProcessClient.readGameContext();

            Strategy[] strategies = new Strategy[teamSize];

            for (int strategyIndex = 0; strategyIndex < teamSize; ++strategyIndex) {
                strategies[strategyIndex] = new MyStrategy();
            }

            PlayerContext playerContext;

            while ((playerContext = remoteProcessClient.readPlayerContext()) != null) {
           //     Strategy.DRAWER.begin(playerContext.getWorld());//todo
                Trooper playerTrooper = playerContext.getTrooper();

                Move move = new Move();
                strategies[playerTrooper.getTeammateIndex()].move(playerTrooper, playerContext.getWorld(), game, move);
                remoteProcessClient.writeMove(move);
            //    Strategy.DRAWER.update(playerContext.getWorld());//todo
            }
        } finally {
            remoteProcessClient.close();
        }
    }
}
