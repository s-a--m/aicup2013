
import model.*;

import java.util.*;
import java.util.List;

public final class MyStrategy implements Strategy {

    public static final double[] W_DANGER = new double[]{

            0.6618, 0.3108, -0.2930, 0.1957, -2.8157, 0.0681, 0.9886, -1.1949, -0.3017, -0.3468, -0.0071, -0.5021, 0.6442,


    };
    public static final double[] W_ATTACK = new double[]{


            0.4234, -0.8835, 0.0865, -0.6806, 1.6768, -0.3659, 1.5355, -0.3697, 0.7397, -1.5154, -1.2299, 0.4722, 0.3695, 0.7035, 1.3408, 1.6918, 1.8162, 1.2528, -0.2068, 2.4231, 0.0366, -0.2993, 0.1862, -0.8515, -1.0962,



    };
    public static final double[] W_HEAL = new double[]{

            1.1862, -0.6752, 0.2478, 0.1020,



    };
    public static final double[] W_HP = new double[]{


            -0.2453, -0.9800, -0.3883, 1.7935, 1.2268, 0.2576, -0.0548, -0.3747, -2.0899, -0.0446, -1.0229, -0.0794, 0.3503,



    };
    public static final double W_MEDIC[] = new double[]{



            -0.7792, -0.2851, -0.1790, 0.8548,



    };
    public static final double[] W_MOVE = new double[]{


            -0.1908, 1.3138, 1.9347, 0.0567, 1.4101, 0.6127, 0.1838, 3.0951, -0.4395, -0.2930, 0.9486, -0.4324,


    };
    public static final double[] W_MOVE_ENEMY = new double[]{

            -0.8462, 1.4099, 0.1170, -0.6003,



    };
    public static final double[] W_MOVE_TO = new double[]{


            0.8289, -1.1967, -0.0061, -0.1931, 0.3927, 0.3553, -1.5986, -0.7472, 0.3494, 0.3721, -0.1020, -0.7338, -1.3372, 1.0121,



    };
    public static final double[] W_TACTIC = new double[]{

            -1.7104, 1.1137, -0.8944, -0.9646, 0.6164, -0.1210, 0.8033, -1.2619, -1.3024, -0.3855, 0.6154, 0.4121, 0.1184, 0.9040, 0.9764,


    };

public static final double[] W_MOVE_FIND = new double[]{

        0.5232, 2.4108, -0.9018, -0.4989, 0.1261, 1.1758, 0.4264,




};

   static  final Map<Long, Me> TEAM = new HashMap<>();
static     final Tactic TACTIC = new Tactic();
  static   TileBasedMap tileMap;
 static    boolean isInit = true;

  private static int MOVE_COST_KNEELING =0 ;
   private static int       MOVE_COST_PRONE = 0;
private static int MOVE_COST_STANDING = 0;
    private static int STANCE_CHANGE_COST =0;
    private static int HEAL_COST = 0;
    private static int EAT_COST = 0;
    private static int THROW_COST = 0;
    private static int MEDIKIT_COST = 0;
    private static Player[] players;
            private static void doInit(World world,Game game) {
//System.out.println(W_DANGER[0]);
        isInit = false;
         MOVE_COST_KNEELING = game.getKneelingMoveCost();
         MOVE_COST_PRONE=game.getProneMoveCost();
                MOVE_COST_STANDING= game.getStandingMoveCost();
                STANCE_CHANGE_COST=game.getStanceChangeCost();
                HEAL_COST = game.getFieldMedicHealCost();
                EAT_COST = game.getFieldRationEatCost();
                THROW_COST = game.getGrenadeThrowCost();
                MEDIKIT_COST = game.getMedikitUseCost();
    players = world.getPlayers();
        Trooper[] troopers = world.getTroopers();
        for (Trooper trooper : troopers) {
            long id = trooper.getId();
            if (trooper.isTeammate()) {
                Me me = new Me();
                me.myId = id;
                me.trooper = trooper;
                 switch(trooper.getType()){
                       case COMMANDER:
                           me.isCommander = true;
                           break;
                        default:
                            break;
                    }
                TEAM.put(id, me);
            }
        }
        CellType[][] cells = world.getCells();
        int width = world.getWidth();
        int height = world.getHeight();
        boolean[][] moveBlockedMap = new boolean[width][height];
        for (int w = 0; w < width; w++) {
            for (int h = 0; h < height; h++) {
                CellType cell = cells[w][h];
                switch (cell) {
                    case FREE:
                        break;
                    case HIGH_COVER:
                    case LOW_COVER:
                    case MEDIUM_COVER:
                        moveBlockedMap[w][h] = true;
                        break;
                }
            }
        }
        tileMap = new TileBasedMap(moveBlockedMap, width, height);
    }

    private static Direction getDirection(Trooper self, MovePath minPath) {
        int sx = self.getX();
        int sy = self.getY();
        int tx = minPath.getNextStepX();
        int ty = minPath.getNextStepY();
        final Direction direction;
        if (sx > tx)
            direction = Direction.WEST;
        else if (sx < tx)
            direction = Direction.EAST;
        else if (sy > ty)
            direction = Direction.NORTH;
        else if (sy < ty)
            direction = Direction.SOUTH;
        else direction = Direction.CURRENT_POINT;
        return direction;
    }

    @Override
    public void move(Trooper self, final World world, Game game, Move move) {
        if (isInit) doInit(world,game);
        doStrategy(self, world, game, move);
    }

    private void doStrategy(Trooper self, final World world, Game game, Move move) {

        int cost  = 0;
        switch(self.getStance()){
           case PRONE:
               cost = game.getProneMoveCost();
               break;
           case STANDING:
               cost = game.getStandingMoveCost();
               break;
           case KNEELING:
               cost = game.getKneelingMoveCost();
               break;
        }
        if (self.getActionPoints() < cost) {
            //System.out.println("low cost");
            return;
        }
        TACTIC.readWorld(world, self, game);
        tileMap.calcDangerMap(world, self);
        tileMap.drawDangerMap();
        TACTIC.chooseAction();
        TACTIC.doAction(move);
        //System.out.println(move.getAction());
    }


    enum TacticActions implements ActionRunnable, TacticCalculator {

  USE_FOOD{
            @Override
            public void run(Move move, ActionContext.ActionContextBuilder c) {
                Trooper player= c.getSelf();
                if (null != player) {
                    move.setAction(ActionType.EAT_FIELD_RATION);
                    move.setX(player.getX());
                    move.setY(player.getY());
                }
            }

            @Override
            public double calc(ActionContext.ActionContextBuilder context) {
                Trooper self = context.getSelf();
                if (!self.isHoldingFieldRation()
                        || (self.getActionPoints() < EAT_COST)
                        ) {
                    return WRONG_CHOISE - 1;
                }
                return TACTIC.calcAwardUseFood(self);
            }
        },
         MOVE_FIELD_MEDIC {
            @Override
            public void run(Move move, ActionContext.ActionContextBuilder c) {
                Trooper self = c.getSelf();
                MovePath path = c.pathMoveMedic;
                if (null != path) {
                    path.draw();
                    move.setAction(ActionType.MOVE);
                    move.setDirection(getDirection(self, path));
                }

            }

            @Override
            public double calc(ActionContext.ActionContextBuilder context) {
                Trooper self = context.getSelf();
                World world = context.getWorld();

                if (!self.getType().equals(TrooperType.FIELD_MEDIC) ) {
                    return WRONG_CHOISE - 1;
                }
                Trooper teamPlayer = TACTIC.getTeamPlayerToHeal(self);
                if (null == teamPlayer||self.equals(teamPlayer)) {
                    return WRONG_CHOISE - 1;
                }
                int[][] siblings = TACTIC.getSiblings(teamPlayer.getX(),
                        teamPlayer.getY());


                List<MovePath> ways = new ArrayList<>();
                PathFinder pathFinder = new MapPathFinder(tileMap,
                        new MapPathFinder.DangerCollisionHeuristic(world.getTroopers()));
                int sx = self.getX();
                int sy = self.getY();//todo already in siblings
                for (int[] sibling : siblings) {
                    int bx = sibling[0];
                    int by = sibling[1];

                    MovePath path = pathFinder.findPath(sx, sy, bx, by);
                    if (null != path)
                        ways.add(path);
                }
                MovePath minPath = null;
                if (!ways.isEmpty()) {
                    minPath = ways.get(0);
                    for (MovePath path : ways) {
                        if (path.getLength() < minPath.getLength()) {
                            minPath = path;
                        }
                    }
                    context.pathMoveMedic = minPath;
                }
                if(null==minPath){
                    return WRONG_CHOISE-1;
                }
                return TACTIC.calcAwardMoveFieldMedic(self, teamPlayer,minPath);
            }
        },
        HEAL_FIELD_MEDIC {
            @Override
            public void run(Move move, ActionContext.ActionContextBuilder c) {
                Trooper player = c.playerToHeal;
                if (null != player) {
                    move.setAction(ActionType.HEAL);
                    move.setX(player.getX());
                    move.setY(player.getY());
                }
            }

            @Override
            public double calc(ActionContext.ActionContextBuilder context) {
                Trooper self = context.getSelf();

                if (!self.getType().equals(TrooperType.FIELD_MEDIC)
                        || (self.getActionPoints() <  HEAL_COST)) {
                    return WRONG_CHOISE - 1;
                }
                Trooper teamPlayer = TACTIC.getTeamPlayerToHealNear(self);
                if (null == teamPlayer) {
                    return WRONG_CHOISE - 1;
                }
                context.setPlayerToHeal(teamPlayer);
                return TACTIC.calcAwardHealFieldMedic(self, teamPlayer);
            }
        },
        USE_MEDIKIT {
            @Override
            public void run(Move move, ActionContext.ActionContextBuilder c) {
                Trooper player = c.playerToHeal;
                if (null != player) {
                    move.setAction(ActionType.USE_MEDIKIT);
                    move.setX(player.getX());
                    move.setY(player.getY());
                }
            }

            @Override
            public double calc(ActionContext.ActionContextBuilder context) {
                Trooper self = context.getSelf();
                if (!self.isHoldingMedikit()
                        || (self.getActionPoints() < MEDIKIT_COST)
                        || !TACTIC.isNeedHeal(self)) {
                    return WRONG_CHOISE - 1;
                }
                Trooper teamPlayer = TACTIC.getTeamPlayerToHeal(self);
                if (null == teamPlayer) {
                    return WRONG_CHOISE - 1;
                }
                context.setPlayerToHeal(teamPlayer);
                return TACTIC.calcAwardUseMedikit(self, teamPlayer);
            }
        },
        DO_NOTHING {
            @Override
            public void run(Move move, ActionContext.ActionContextBuilder context) {
                move.setAction(ActionType.END_TURN);
            }

            @Override
            public double calc(ActionContext.ActionContextBuilder context) {
                Trooper self = context.getSelf();
                return TACTIC.calcAwardDoNothing(self);
            }
        },

REQUEST_ENEMY_DISPOSITION{
            @Override
            public void run(Move move, ActionContext.ActionContextBuilder context) {
                TACTIC.isDispositionRequested = true;
                move.setAction(ActionType.REQUEST_ENEMY_DISPOSITION);
            }

            @Override
            public double calc(ActionContext.ActionContextBuilder context) {
                Trooper self = context.getSelf();
                if(!self.getType().equals(TrooperType.COMMANDER)
                       ||
                        self.getActionPoints()<TACTIC.game.getCommanderRequestEnemyDispositionCost()
                        ||
                        !TACTIC.enemyPositions.isEmpty()
                        ){
                   return WRONG_CHOISE-1;
                }
                return TACTIC.calcAwardRequestDisposition(self);
            }
        },        MOVE_FIND{
            @Override
            public void run(Move move, ActionContext.ActionContextBuilder context) {
                Trooper self = context.getSelf();
                MovePath path = context.pathFind;
                TACTIC.enemyLastKnownPosition =TACTIC.pollEnemyLastKnownPosition();

                if (null != path) {
                    path.draw();
                    move.setAction(ActionType.MOVE);
                    move.setDirection(getDirection(self, path));
                }else{
                    throw new RuntimeException("omg!");
                }
            }

            @Override
            public double calc(ActionContext.ActionContextBuilder context) {
                Trooper self = context.getSelf();
                World world = context.getWorld();
                int ap = self.getActionPoints();
                int cost = 0;
                switch(self.getStance()){
                    case  KNEELING:
                        cost = MOVE_COST_KNEELING;
                        break;
                    case PRONE:
                        cost = MOVE_COST_PRONE;
                        break;
                    case STANDING:
                        cost = MOVE_COST_STANDING;
                        break;
                }
                if(ap<cost){
                    return WRONG_CHOISE-1;
                }
                int sx = self.getX();
                int sy = self.getY();

                Tactic.Point lastPosition = TACTIC.enemyLastKnownPosition;

                int tx = lastPosition.getX();
                int ty = lastPosition.getY();

               if(tx<0) {
                 lastPosition = TACTIC.peekEnemyLastKnownPosition();
                 tx = lastPosition.getX();
                 ty = lastPosition.getY();
               }


               MovePath path;
                if (tx < 0) {
                    double maxDanger = tileMap.dangerMap[sx][sy];
                    tx = sx;
                    ty = sy;
                    for (int x = 0; x < tileMap.width; x++) {
                        for (int y = 0; y < tileMap.height; y++) {
                            double danger = tileMap.dangerMap[x][y];
                            if (danger != 1000 && maxDanger < danger) {
                                maxDanger = danger;
                                tx = x;
                                ty = y;
                            }
                        }
                    }
                }
                  PathFinder pathFinder =
                        new MapPathFinder(tileMap,
                                new MapPathFinder.DangerCollisionHeuristic(world.getTroopers()));

                path = pathFinder.findPath(sx, sy, tx, ty);
                if(null==path){
                    return WRONG_CHOISE-1;
                }
                context.pathFind = path;
                return TACTIC.calcAwardMoveFind(self);
            }
        },
        MOVE_FIELD {
            @Override
            public double calc(ActionContext.ActionContextBuilder context) {
                World world = context.getWorld();
                Trooper self = context.getSelf();
                int ap = self.getActionPoints();
                int cost = 0;
                switch(self.getStance()){
                    case  KNEELING:
                        cost = MOVE_COST_KNEELING;
                        break;
                    case PRONE:
                        cost = MOVE_COST_PRONE;
                        break;
                    case STANDING:
                        cost = MOVE_COST_STANDING;
                        break;
                }
                if(ap<cost){
                    return WRONG_CHOISE-1;
                }
                PathFinder pathFinder = new MapPathFinder(tileMap,
                        new MapPathFinder.DangerCollisionHeuristic(world.getTroopers()));
                int sx = self.getX();
                int sy = self.getY();
                double minDanger = tileMap.dangerMap[sx][sy];
                int minX = sx;
                int minY = sy;
                for (int x = 0; x < tileMap.width; x++) {
                    for (int y = 0; y < tileMap.height; y++) {
                        double danger = tileMap.dangerMap[x][y];
                        if (minDanger > danger&&1000!=danger) {
                            minDanger = danger;
                            minX = x;
                            minY = y;
                        }
                    }
                }
                MovePath minPath = pathFinder.findPath(sx, sy, minX, minY);

                context.pathMoveField = minPath;
                if (null == minPath) {
                    return WRONG_CHOISE - 1;
                }
                return TACTIC.calcAwardMove(self, minPath);
            }

            @Override
            public void run(Move move, ActionContext.ActionContextBuilder c) {
                MovePath path = c.pathMoveField;
                if (null != path) {
                    path.draw();
                    Trooper self = c.getSelf();
                    move.setAction(ActionType.MOVE);
                    move.setDirection(getDirection(self, path));
                }
            }
        }, MOVE_BONUS {
            @Override
            public double calc(ActionContext.ActionContextBuilder context) {
                World world = context.getWorld();
                Trooper self = context.getSelf();
                int ap = self.getActionPoints();
                int cost = 0;
                switch(self.getStance()){
                    case  KNEELING:
                        cost = MOVE_COST_KNEELING;
                        break;
                    case PRONE:
                        cost = MOVE_COST_PRONE;
                        break;
                    case STANDING:
                        cost = MOVE_COST_STANDING;
                        break;
                }
                if(ap<cost){
                    return WRONG_CHOISE-1;
                }
                Bonus[] bonuses = world.getBonuses();
                List<MovePath> ways = new ArrayList<>();
                PathFinder pathFinder = new MapPathFinder(tileMap, new MapPathFinder.DangerCollisionHeuristic(world.getTroopers()));
                int sx = self.getX();
                int sy = self.getY();
                boolean isNeedMedikit = TACTIC.isNeedHeal(self) && !self.isHoldingMedikit();
                boolean isNeedGrenade = TACTIC.isNeedGrenade(self)&&!self.isHoldingGrenade();
                boolean isNeedFood = TACTIC.isNeedRation(self)&&!self.isHoldingFieldRation();
                for (Bonus bonus : bonuses) {
                    int bx = bonus.getX();
                    int by = bonus.getY();
                    switch (bonus.getType()) {
                        case GRENADE:
                            if (!isNeedGrenade) {
                                continue;
                            }
                            break;
                        case MEDIKIT:
                            if (!isNeedMedikit) {
                                continue;
                            }
                            break;
                        case FIELD_RATION:
                            if (!isNeedFood) {
                                continue;
                            }
                    }
                    MovePath path = pathFinder.findPath(sx, sy, bx, by);
                    if (null != path)
                        ways.add(path);
                }
                if (!ways.isEmpty()) {
                    MovePath minPath = ways.get(0);
                    for (MovePath path : ways) {
                        if (path.getLength() < minPath.getLength()) {
                            minPath = path;
                        }
                    }
                    context.pathMoveBonus = minPath;
                    return TACTIC.calcAwardMove(self, minPath);
                } else {
                    return WRONG_CHOISE - 1;
                }
            }

            @Override
            public void run(Move move, ActionContext.ActionContextBuilder c) {
                MovePath path = c.pathMoveBonus;
                if (null != path) {
                    path.draw();
                    Trooper self = c.getSelf();
                    move.setAction(ActionType.MOVE);
                    move.setDirection(getDirection(self, path));
                }
            }
        },

        THROW_GRENADE {
            @Override
            public void run(Move move, ActionContext.ActionContextBuilder context) {
                Trooper enemy = context.enemy;
                if (null != enemy) {
                    move.setAction(ActionType.THROW_GRENADE);
                    move.setX(enemy.getX());
                    move.setY(enemy.getY());
                }
            }

            @Override
            public double calc(ActionContext.ActionContextBuilder context) {
                World world = context.getWorld();
                Trooper self = context.getSelf();
                if (!self.isHoldingGrenade()
                        || self.getActionPoints() < THROW_COST) {
                    return WRONG_CHOISE;
                }
                Trooper myEnemy = null;
                for (Trooper enemy : TACTIC.enemies) {
                    if (world.isVisible(
                            TACTIC.game.getGrenadeThrowRange(),
                            self.getX(), self.getY(),
                            self.getStance(),
                            enemy.getX(), enemy.getY(),
                            enemy.getStance())
                            ) {
                        if (null == myEnemy) {
                            myEnemy = enemy;
                        } else {
                            if (self.getDistanceTo(myEnemy) >
                                    self.getDistanceTo(enemy)) {
                                myEnemy = enemy;
                            }
                        }
                    }
                }
                context.setEnemy(myEnemy);
                if (null == myEnemy) {
                    return WRONG_CHOISE - 1;
                }
                return TACTIC.calcAwardThrowGrenade(self, myEnemy);
            }
        },
         MOVE_TO_COMMANDER {
            @Override
            public void run(Move move, ActionContext.ActionContextBuilder c) {
                MovePath path = c.pathMoveCommander;
                if (null != path) {
                    path.draw();
                    Trooper self = c.getSelf();
                    move.setAction(ActionType.MOVE);
                    move.setDirection(getDirection(self, path));
                }
            }

             @Override
             public double calc(ActionContext.ActionContextBuilder context) {
                 World world = context.getWorld();
                 Trooper self = context.getSelf();
                 int ap = self.getActionPoints();
                int cost = 0;
                switch(self.getStance()){
                    case  KNEELING:
                        cost = MOVE_COST_KNEELING;
                        break;
                    case PRONE:
                        cost = MOVE_COST_PRONE;
                        break;
                    case STANDING:
                        cost = MOVE_COST_STANDING;
                        break;
                }
                if(ap<cost){
                    return WRONG_CHOISE-1;
                }
                 Trooper commander = TACTIC.getCommander();
                 if (self.equals(commander)) {
                     return WRONG_CHOISE - 1;
                 }else{
                     if(null==commander){
                         throw new RuntimeException("could not get Commander");
                     }
                 }

                 PathFinder pathFinder = new MapPathFinder(tileMap,
                         new MapPathFinder.CollisionHeuristic(world.getTroopers()));

                 int sx = self.getX();
                 int sy = self.getY();

                 MovePath minPath = pathFinder.findPath(sx, sy,
                         commander.getX(), commander.getY());

                 if (null == minPath) {
                     return WRONG_CHOISE - 1;
                 }
                 context.pathMoveCommander =minPath;
                 return TACTIC.calcAwardMoveCommander(self, commander, minPath);
             }
        },
        MOVE_ENEMY {
            @Override
            public void run(Move move, ActionContext.ActionContextBuilder c) {
                MovePath path = c.pathMoveEnemy;
                if (null != path) {
                    path.draw();
                    Trooper self = c.getSelf();
                    move.setAction(ActionType.MOVE);
                    move.setDirection(getDirection(self, path));
                }
            }

            @Override
            public double calc(ActionContext.ActionContextBuilder context) {

                World world = context.getWorld();
                Trooper self = context.getSelf();
                int ap = self.getActionPoints();
                int cost = 0;
                switch(self.getStance()){
                    case  KNEELING:
                        cost = MOVE_COST_KNEELING;
                        break;
                    case PRONE:
                        cost = MOVE_COST_PRONE;
                        break;
                    case STANDING:
                        cost = MOVE_COST_STANDING;
                        break;
                }
                if(ap<cost){
                    return WRONG_CHOISE-1;
                }
                Trooper myEnemy = null;
                for (Trooper enemy : TACTIC.enemies) {
                    if (null == myEnemy) {
                        myEnemy = enemy;
                    } else {
                        if (self.getDistanceTo(myEnemy) >
                                self.getDistanceTo(enemy)) {
                            myEnemy = enemy;
                        }
                    }
                }

                if (null == myEnemy) {
                    return WRONG_CHOISE - 1;
                }

                PathFinder pathFinder = new MapPathFinder(tileMap,
                        new MapPathFinder.CollisionHeuristic(world.getTroopers()));

                int sx = self.getX();
                int sy = self.getY();

                MovePath minPath = pathFinder.findPath(sx, sy, myEnemy.getX(), myEnemy.getY());

                if (null == minPath) {
                    return WRONG_CHOISE - 1;
                }
                context.pathMoveEnemy = minPath;
                return TACTIC.calcAwardMoveEnemy(self, myEnemy, minPath);
            }
        },
        LOW_STANCE {
            @Override
            public void run(Move move, ActionContext.ActionContextBuilder context) {
                move.setAction(ActionType.LOWER_STANCE);
            }

            @Override
            public double calc(ActionContext.ActionContextBuilder context) {
                Trooper self = context.getSelf();
                int ap =self.getActionPoints();
                if(ap<STANCE_CHANGE_COST){
                    return WRONG_CHOISE-1;
                }
                if (self.getStance().equals(TrooperStance.PRONE)) {
                    return WRONG_CHOISE - 1;
                }
                return TACTIC.calcAwardLowStance(self);
            }
        },
        HIGH_STANCE {
            @Override
            public void run(Move move, ActionContext.ActionContextBuilder context) {
                move.setAction(ActionType.RAISE_STANCE);
            }

            @Override
            public double calc(ActionContext.ActionContextBuilder context) {
                Trooper self = context.getSelf();
                int ap =self.getActionPoints();
                if(ap<STANCE_CHANGE_COST){
                    return WRONG_CHOISE-1;
                }
                if (self.getStance().equals(TrooperStance.STANDING)) {
                    return WRONG_CHOISE - 1;
                }
                return TACTIC.calcAwardHighStance(self);
            }
        },
        ATTACK_ENEMY {
            @Override
            public double calc(ActionContext.ActionContextBuilder context) {
                World world = context.getWorld();
                Trooper self = context.getSelf();
                int cost = self.getShootCost();
                int ap = self.getActionPoints();
                if(ap<cost){
                    return WRONG_CHOISE-1;
                }
                Trooper enemy = null;

                for (Trooper trooper : TACTIC.enemies) {
                    if (world.isVisible(self.getShootingRange(),
                            self.getX(), self.getY(),
                            self.getStance(),
                            trooper.getX(),
                            trooper.getY(),
                            trooper.getStance())) {
                        if (null == enemy) {
                            enemy = trooper;
                        }
                        if (enemy.getDistanceTo(self) > trooper.getDistanceTo(self)) {
                                enemy = trooper;
                        }
                    }
                }

                if (null == enemy) {
                    return WRONG_CHOISE - 1;
                }
                context.setEnemy(enemy);
                return TACTIC.calcAwardAttack(self, enemy);
            }

            @Override
            public void run(Move move, ActionContext.ActionContextBuilder context) {
                Trooper enemy = context.enemy;
                if (null != enemy) {
                    move.setAction(ActionType.SHOOT);
                    move.setX(enemy.getX());
                    move.setY(enemy.getY());
                }
            }
        };
        private static final int WRONG_CHOISE = 3-Integer.MAX_VALUE;
    }

    interface TacticCalculator {
        double calc(ActionContext.ActionContextBuilder context);
    }

    interface PathFinder {
        MovePath findPath(int sx, int sy, int tx, int ty);
    }

    interface AStarHeuristic {
        double getCost(int sx, int sy, int tx, int ty);

        boolean isValid(int xp, int yp);
    }

    interface ActionRunnable {
        void run(Move move, ActionContext.ActionContextBuilder context);
    }

    static class Tactic {
        List<Trooper> enemies = new ArrayList<>();
        Trooper self;
        World world;
        Game game;
        private int totalTeamHp;
        private TacticActions currentAction;
        private ActionContext.ActionContextBuilder aContextBuilder;
        private Point enemyLastKnownPosition = new Point(-1,-1);
        private LinkedList<Point> enemyPositions = new LinkedList<>();
        private Trooper commander;
        public boolean isDispositionRequested;


        public Trooper getTeamPlayerToHealNear(Trooper self) {

            int sx = self.getX();
            int sy = self.getY();
            int[][] xy = getSiblings(sx, sy);

            Trooper teamPlayer = null;
            for (Long id : TEAM.keySet()) {
                Me me = TEAM.get(id);
                if (TACTIC.isNeedHeal(me.trooper)) {
                    int mx = me.trooper.getX();
                    int my = me.trooper.getY();
                    for(int i = 0;i<4;i++){
                        if(mx==xy[i][0] && my==xy[i][1]
                                ||
                                mx==sx&&my==sy
                                ){
                            teamPlayer = me.trooper;
                        }
                    }
                }
            }
            return teamPlayer;
        }

        private int[][] getSiblings(int sx, int sy) {
            int[][] xy = new int[4][2];
            xy[0][0] = sx-1;
            xy[0][1] = sy;
            xy[1][0] = sx+1;
            xy[1][1] = sy;

            xy[2][0] = sx;
            xy[2][1] = sy-1;
            xy[3][0] = sx;
            xy[3][1] = sy+1;
            for(int i =0;i<4;i++){
                int x = xy[i][0];
                int y = xy[i][1];
                if(x<0){
                    xy[i][0] = 0;
                }else if(x>=tileMap.width){
                    xy[i][0] = tileMap.width-1;
                }
                if(y<0){
                    xy[i][1] = 0;
                }else if(y>=tileMap.height){
                    xy[i][1] = tileMap.height-1;
                }
            }
            return xy;
        }

        public Trooper getTeamPlayerToHeal(Trooper self) {
            Trooper teamPlayer = null;
            for (Long id : TEAM.keySet()) {
                Me me = TEAM.get(id);
                if (TACTIC.isNeedHeal(me.trooper)) {
                    if (null == teamPlayer) {
                        teamPlayer = me.trooper;
                    } else {
                        if (self.getDistanceTo(teamPlayer) >
                                self.getDistanceTo(me.trooper)) {
                            teamPlayer = me.trooper;
                        }
                    }
                }
            }
            return teamPlayer;
        }

        public void readWorld(World world, Trooper self, Game game) {
            aContextBuilder = new ActionContext.ActionContextBuilder(world, self);
            this.self = self;
            this.world = world;
            this.game = game;
            enemies.clear();
            Trooper[] allTroopers = world.getTroopers();
            commander = null;
            for(Long id:TEAM.keySet()){
                Me me = TEAM.get(id);
                me.trooper = null;
            }
            if(isDispositionRequested){
                isDispositionRequested = false;
                for(Player p: players){
                    int x = p.getApproximateX();
                    int y = p.getApproximateY();
                    enemyLastKnownPosition = new Point(x,y);
                    enemyPositions.push(enemyLastKnownPosition);
                }
            }
            for (Trooper trooper : allTroopers) {
                if (trooper.isTeammate()) {
                    long id = trooper.getId();
                    Me me = TEAM.get(id);
                    if(null==me){
                        continue;
                    }
                    me.trooper = trooper;
                    if (me.isCommander) {
                        commander = trooper;
                    }
                } else if (trooper.getHitpoints() > 0) {
                    enemies.add(trooper);
                    enemyLastKnownPosition = new Point(trooper.getX(), trooper.getY());
                    if (!enemyPositions.contains(enemyLastKnownPosition)) {
                        enemyPositions.push(enemyLastKnownPosition);
                    }
                }
            }
            List<Long>diedIds = new ArrayList<>();
            for(Long id:TEAM.keySet()){
                Me me = TEAM.get(id);
                if (null != me && null == me.trooper) {
                    diedIds.add(id);
                }
            }
            for(Long id:diedIds){
                TEAM.remove(id);
            }
            if(null==commander){
                //System.out.println("null commander");
                for(Long id:TEAM.keySet()){
                    Me me = TEAM.get(id);
                    if(!me.isDied()){
                        me.isCommander = true;
                        commander = me.trooper;
                        break;
                    }
                }
            }
            totalTeamHp = 0;
            for (Long lm : TEAM.keySet()) {
                Me m = TEAM.get(lm);
                int mHp = m.trooper.getHitpoints();
                totalTeamHp += mHp;
            }
        }

        public int getTotalTeamHp() {
            return totalTeamHp;
        }

        public void chooseAction() {
            TacticActions bestAction = TacticActions.DO_NOTHING;
            double kmax = bestAction.calc(aContextBuilder);
            int i = 0;
            for (TacticActions p : TacticActions.values()) {
                double k = p.calc(aContextBuilder);
                //System.out.println(p+" "+k);
               //DRAWER.setColorBlue();//Todo
                //DRAWER.drawString("k=" + String.valueOf(k) + "|" + p, 0, world.getHeight() - 0.5 * ++i);
                if (k > kmax) {
                    bestAction = p;
                }
            }
            //DRAWER.drawString("best|" + bestAction, 0, world.getHeight() - 0.5 * ++i);
            setCurrentAction(bestAction);
        }

        private void setCurrentAction(TacticActions action) {
            currentAction = action;
        }

        public void doAction(Move move) {
            //System.out.println(self.getType()+":"+currentAction);
            currentAction.run(move, aContextBuilder);
        }

        public double calcAwardAttack(Trooper self, Trooper enemy) {
            double shp = 2*self.getHitpoints()/self.getMaximalHitpoints();
            double ehp = 2*enemy.getHitpoints()/enemy.getMaximalHitpoints();
            final double et;
            switch(enemy.getType()){
                case COMMANDER:
                    et=W_ATTACK[10];
                    break;
                case FIELD_MEDIC:
                    et=W_ATTACK[11];
                    break;
                case SCOUT:
                    et=W_ATTACK[12];
                    break;
                case SNIPER:
                    et=W_ATTACK[13];
                    break;
                case SOLDIER:
                    et=W_ATTACK[14];
                    break;
                default:
                    throw new RuntimeException("unknown type "+enemy.getType());
            }
            double ts = TEAM.size()/5;
            double es = enemies.size()/20;
            double hg = enemy.isHoldingGrenade()?W_ATTACK[15]:W_ATTACK[16];
            return (shp * W_ATTACK[4]
                    + ehp * W_ATTACK[5]
                    + ts*W_ATTACK[17]
                    + es*W_ATTACK[18]
                    + hg*3
                    + et)/6;
        }

        public double calcAwardMove(Trooper self, MovePath minPath) {
            double ap = self.getActionPoints()/3;
            double pl = minPath.getLength()/6;
            double hp = self.getHitpoints()/self.getMaximalHitpoints();
            double pd = tileMap.dangerMap[self.getX()][self.getY()];
            return (ap * W_MOVE_TO[0]
                    + pl * W_MOVE_TO[1]
                    + hp * W_MOVE_TO[2]
                    + pd * W_MOVE_TO[3])/4;

        }

        public double calcAwardDoNothing(Trooper self) {
            double shp = self.getHitpoints()/self.getMaximalHitpoints();
            double sap = self.getActionPoints()/10;
            double ts = TEAM.size()/5;

            return (W_TACTIC[0] * shp
                    + W_TACTIC[1] * sap
                    + W_TACTIC[2] * ts)/3;
        }

        public double calcAwardHealFieldMedic(Trooper self, Trooper teamPlayer) {
            double nh = 3*calcNeedHealK(teamPlayer);
            double ap = self.getActionPoints()/3;
            double ts =  TEAM.size()/5;
            double d =  self.getDistanceTo(teamPlayer)/10;
            return (W_MEDIC[0] * nh
                    + W_MEDIC[1] * ap
                    + W_MEDIC[2] *ts
                    + W_MEDIC[3] *d)/4
                    ;
        }

        public double calcNeedHealK(Trooper t) {
            final double z;
            final double k;
            switch (t.getType()) {
                case FIELD_MEDIC:
                    z = W_HP[3]*3;
                    k = W_HP[4];
                    break;
                case COMMANDER:
                    z = W_HP[5];
                    k = W_HP[6]*3;
                    break;
                case SCOUT:
                    z = W_HP[7];
                    k = W_HP[8]*5;
                    break;
                case SNIPER:
                    z = W_HP[9];
                    k = W_HP[10];
                    break;
                case SOLDIER:
                    z = W_HP[11];
                    k = W_HP[12];
                    break;
                default:
                    throw new RuntimeException("wrong " + t.getType());
            }
            double cm = t.getHitpoints()/t.getMaximalHitpoints();
//todo enemy size
            return k* (z - cm);

        }

        public boolean isNeedHeal(Trooper t) {
            if(t.getHitpoints()>=t.getMaximalHitpoints()){
                return false;
            }
            double k = calcNeedHealK(t);
            return W_HP[2] > k;
        }


        public boolean isNeedGrenade(Trooper self) {
            if(self.isHoldingGrenade()){
                return false;
            }
               double ap   = self.getActionPoints()/3;
            double ts = TEAM.size()/5;
            double es = enemies.size()/10;
            double sHp = self.getHitpoints()/
                    self.getMaximalHitpoints();
            return
                    (W_TACTIC[9]*ap
                    +W_TACTIC[10]*ts
                    +W_TACTIC[11]*es
                    +W_TACTIC[12]*sHp
                    +W_TACTIC[13]*3)/5 > W_TACTIC[14];

        }

        public boolean isNeedRation(Trooper self) {
            if(self.isHoldingFieldRation()){
                return false;
            }
           double ap   = self.getActionPoints()/3;
            double ts = TEAM.size()/5;
            double es = enemies.size()/10;
            double sHp = self.getHitpoints()/
                    self.getMaximalHitpoints();
            return
                    (W_TACTIC[3]*ap
                    +W_TACTIC[4]*ts
                    +W_TACTIC[5]*es
                    +W_TACTIC[6]*sHp
                    +W_TACTIC[7])/5>W_TACTIC[8];
        }
        public boolean isTeamNeedHeal() {
            double k = 0;
            for (Long lme : TEAM.keySet()) {
                Me me = TEAM.get(lme);
                k += calcNeedHealK(me.trooper);
            }
            k += TEAM.size() * W_HP[1];
            return k/(TEAM.size()+1) < W_HP[0];
        }

        public double calcAwardThrowGrenade(Trooper self, Trooper enemy) {
            double shp = 2*self.getHitpoints()/self.getMaximalHitpoints();
            double ehp = 2*enemy.getHitpoints()/enemy.getMaximalHitpoints();
            double dist = 2*self.getDistanceTo(enemy)/self.getShootingRange();
            return (shp * W_ATTACK[0]
                    + ehp * W_ATTACK[1]
                    + dist * W_ATTACK[2])/3;
        }

        public double calcAwardUseMedikit(Trooper self, Trooper teamPlayer) {
            double ts = 3*TEAM.size()/5;
            double d = self.getDistanceTo(teamPlayer)/10;
            double es = enemies.size()/10;
            double nh = 5*calcNeedHealK(teamPlayer);
            return (W_HEAL[0] * ts
                    + W_HEAL[1] * d
                    + W_HEAL[2] * es
                    + W_HEAL[3] * nh)/4
                    ;
        }

        public double calcAwardHighStance(Trooper self) {
            double shp = self.getHitpoints()/self.getMaximalHitpoints();
            double danger = 3*tileMap.dangerMap[self.getX()][self.getY()];
            double ap  = self.getActionPoints()/10;
            double es =  enemies.size()/3;
            double ts = TEAM.size()/5;
            return (W_ATTACK[19] *es
                    + W_ATTACK[20] * shp
                    + W_ATTACK[21] * ts
                    + W_ATTACK[22] * danger
                    + W_ATTACK[23] * ap
                    + W_ATTACK[24])/6;
        }

        public double calcAwardLowStance(Trooper self) {
            double shp = 2*self.getHitpoints()/self.getMaximalHitpoints();
            double danger = tileMap.dangerMap[self.getX()][self.getY()];
            double es =  enemies.size()/5;
            double ts = TEAM.size()/5;
            return (W_ATTACK[6] *es
                    + W_ATTACK[7] * shp
                    + W_ATTACK[8] * ts
                    + W_ATTACK[8] * danger
                    + 2*W_ATTACK[9])/5;

        }

        public double calcAwardMoveEnemy(Trooper self, Trooper enemy, MovePath minPath) {
            double shp = 2*self.getHitpoints()/self.getMaximalHitpoints();
            double ehp = 2*enemy.getHitpoints()/enemy.getMaximalHitpoints();

            double pd = 0;
            int pl = 0;
            for (MovePath.MoveStep step : minPath.moveSteps) {
                pd += tileMap.dangerMap[step.x][step.y];
                pl++;
            }
            pd = 3*(pl>=1?pd/minPath.getLength():pd);

            return (shp * W_MOVE_ENEMY[0]
                    + ehp * W_MOVE_ENEMY[1]
                    + pd * W_MOVE_ENEMY[2]
                    + W_MOVE_ENEMY[3])/4;
        }

        public double calcAwardMoveFind(Trooper self) {
            double shp = self.getHitpoints()/self.getMaximalHitpoints();
            double danger = tileMap.dangerMap[self.getX()][self.getY()];
            double ts = TEAM.size()/5;
            double es = enemies.size()/10;
            double r=0;
            for(Me me :TEAM.values()){
               r+=self.getDistanceTo(me.trooper) ;
            }
            r = r/TEAM.size()/20;
            double sp = self.getActionPoints()/10;
            return (W_MOVE_FIND[0]*shp
                    + W_MOVE_FIND[1]*danger
                    + W_MOVE_FIND[2]*ts
                    + W_MOVE_FIND[3]*es
                    + W_MOVE_FIND[4]*r
                    + W_MOVE_FIND[5]*sp)/6;
        }

        public Point pollEnemyLastKnownPosition() {
            Point p = enemyPositions.pollLast();
            if(null==p){
                p = enemyLastKnownPosition;
                if(null==p){
                    p = new Point(-1,-1);
                }
            }
            return p;
        }


        public Point peekEnemyLastKnownPosition() {
            Point p  = enemyPositions.peekLast();
            if(null==p){
                p = enemyLastKnownPosition;
                if(null==p){
                    p = new Point(-1,-1);
                }
            }
            return p;
        }

        public double calcAwardMoveCommander(Trooper self, Trooper commander, MovePath minPath) {
            double sHp = self.getHitpoints()
                    /self.getMaximalHitpoints();
            double cHp = commander.getHitpoints()
                    /commander.getMaximalHitpoints();
            double dist = self.getDistanceTo(commander)/20;
            double ts = TEAM.size()/5;
            double es = enemies.size()/10;
            double ap = self.getActionPoints()/10;
            double edist = self.getDistanceTo(
                    enemyLastKnownPosition.x,
                    enemyLastKnownPosition.y
            )/20;
            double danger = 3*tileMap.dangerMap[self.getX()][self.getY()];
            return
                    (W_MOVE[3]*sHp
                    +W_MOVE[4]*cHp
                    +W_MOVE[5]*dist
                    +W_MOVE[6]*ts
                    +W_MOVE[7]*es
                    +W_MOVE[8]*ap
                    +W_MOVE[9]*edist
                    +W_MOVE[10]*danger
                    +W_MOVE[11])/9
                    ;
        }

        public Trooper getCommander() {
            return commander;
        }

        public double calcAwardUseFood(Trooper self) {
            return 0.5;
        }

        public double calcAwardMoveFieldMedic(Trooper self, Trooper teamPlayer, MovePath minPath) {
              double sHp = self.getHitpoints()
                    /self.getMaximalHitpoints();
            double cHp = teamPlayer.getHitpoints()
                    /teamPlayer.getMaximalHitpoints();
            double dist = self.getDistanceTo(teamPlayer)/20;
            double ts = TEAM.size()/5;
            double es = enemies.size()/10;
            double ap = self.getActionPoints()/10;
            double edist = self.getDistanceTo(
                    enemyLastKnownPosition.x,
                    enemyLastKnownPosition.y
            )/6;
            double danger = tileMap.dangerMap[self.getX()][self.getY()];
            double pl = minPath.getLength()/10;
            return
                    (
                    W_MOVE_TO[4]*cHp
                    +W_MOVE_TO[5]*dist
                    +W_MOVE_TO[6]*ts
                    +W_MOVE_TO[7]*es
                    +W_MOVE_TO[8]*ap
                    +W_MOVE_TO[9]*edist
                    +W_MOVE_TO[10]*danger
                    +W_MOVE_TO[11]
                    +W_MOVE_TO[12]*pl
                    +W_MOVE_TO[13]*sHp
                            )/10
                    ;
        }

        public double calcAwardRequestDisposition(Trooper self) {
            return W_MOVE_FIND[6];
        }



        public static class Point{
            private final int x;
            private final int y;

            public Point(int x, int y) {
                this.x = x;
                this.y = y;
            }
            public int getX(){
                return x;
            }
            public int getY(){
                return y;
            }
        }
    }

    static abstract class ActionContext {

        static class ActionContextBuilder {
            private final World world;
            private final Trooper self;
            private Trooper enemy;
            private Trooper playerToHeal;
            public MovePath pathMoveEnemy;
            public MovePath pathMoveCommander;
            public MovePath pathMoveBonus;
            public MovePath pathMoveField;
            public MovePath pathFind;
            public MovePath pathMoveMedic;

            ActionContextBuilder(World world, Trooper self) {
                this.world = world;
                this.self = self;
            }

            public void setPath(MovePath path) {
            }

            public void setEnemy(Trooper e) {
                enemy = e;
            }

            public World getWorld() {
                return world;
            }

            public Trooper getSelf() {
                return self;
            }

            public void setPlayerToHeal(Trooper playerToHeal) {
                this.playerToHeal = playerToHeal;
            }

        }

    }

    public static class TileBasedMap implements AStarHeuristic {
        private static final double BLOCKED_DANGER = 1000;
        private boolean[][] moveBlockedMap;
        private double[][] dangerMap;
        private int width, height;

        public TileBasedMap(boolean[][] blockedMap, int w, int h) {
            this.moveBlockedMap = blockedMap;
            dangerMap = new double[w][h];
            this.width = w;
            this.height = h;
        }

        public void drawDangerMap() {
            for (int x = 0; x < dangerMap.length; x++) {
                for (int y = 0; y < dangerMap[x].length; y++) {
                    //DRAWER.setColorRed();//Todo
                    //DRAWER.drawString( String.format("%.3f",dangerMap[x][y]), (double) x, (double) y + 0.4d);
                }
            }
        }

        public boolean isBlocked(int x, int y) {


            return (x < 0)
                    || (x >= width)
                    || (y < 0)
                    || (y >= height)
                    || moveBlockedMap[x][y];
        }

        public boolean isValid(int x, int y) {
            return !isBlocked(x, y);
        }

        public double getCost(int sx, int sy, int tx, int ty) {
            int dx = tx - sx;
            int dy = ty - sy;
            return Math.sqrt(dx * dx + dy * dy);
        }

        public void calcDangerMap(World world, Trooper self) {
            for (int x = 0; x < world.getWidth(); x++) {
                for (int y = 0; y < world.getHeight(); y++) {
                    if (!moveBlockedMap[x][y]) {
                        dangerMap[x][y] = getDanger(world, self, x, y, self.getStance());
                    } else {
                        dangerMap[x][y] = BLOCKED_DANGER;
                    }
                }
            }
        }

        private double getDanger(World world, Trooper self, int x, int y, TrooperStance stance) {
            double myPoints = self.getActionPoints()/10;
            double  myHp = 2*self.getHitpoints()/self.getMaximalHitpoints();
            double teamHp = TACTIC.getTotalTeamHp()/5/self.getMaximalHitpoints();
            double teamCount = TEAM.size()/5;
            double selfVisionRange = self.getVisionRange()/5;
            double selfShootRange = self.getShootingRange()/20;
            double selfDistance = self.getDistanceTo(x, y)/5;
            boolean isSelfFireVisible = world.isVisible(selfShootRange,
                    self.getX(), self.getY(), self.getStance(),
                    x, y, stance);
            boolean isSelfViewVisible = world.isVisible(selfVisionRange,
                    self.getX(), self.getY(), self.getStance(),
                    x, y, stance);
            double selfFireVisibleDanger = isSelfFireVisible ? 0.2 : 1.0;
            double selfViewVisibleDanger = isSelfViewVisible ? 0.2 : 1.0;
            int es = 0;
            double danger = 0;
            for (Trooper enemy : TACTIC.enemies) {
                double viewRange = enemy.getVisionRange()/20;
                double fireRange = enemy.getShootingRange()/20;
                double numPoints = enemy.getActionPoints()/5;
                double distance = enemy.getDistanceTo(x, y)/20;
                boolean isFireVisible = world.isVisible(fireRange,
                        enemy.getX(), enemy.getY(),
                        enemy.getStance(), x, y, stance);
                boolean isViewVisible = world.isVisible(viewRange,
                        enemy.getX(), enemy.getY(),
                        enemy.getStance(), x, y, stance);
                double fireVisibleDanger = isFireVisible ? 0.2 : 1.0;
                double viewVisibleDanger = isViewVisible ? 0.2 : 1.0;
                danger += (W_DANGER[0] * numPoints + W_DANGER[1] * fireVisibleDanger + W_DANGER[2] * viewVisibleDanger
                        + W_DANGER[3] * distance)/4
                ;
                es++;
            }
            danger = es>=1?danger/es:danger;
            danger +=( W_DANGER[4] * myPoints + W_DANGER[5] * myHp + W_DANGER[6] * teamHp
                    + W_DANGER[7] * teamCount + W_DANGER[8] * selfVisionRange + W_DANGER[9] * selfShootRange
                    + W_DANGER[10] * selfDistance + W_DANGER[11] * selfFireVisibleDanger
                    + W_DANGER[12] * selfViewVisibleDanger)/9;
            return danger/2;
        }


    }

    public static class MapPathFinder implements PathFinder {

        private final ArrayList<Node> closed = new ArrayList<>();
        private final SortedList<Node> open = new SortedList<>();
        private final TileBasedMap map;
        private final Node[][] nodes;
        private final AStarHeuristic heuristic;
        private final boolean[][] visited;

        public MapPathFinder(TileBasedMap map, AStarHeuristic heuristic) {
            this.map = map;
            this.visited = new boolean[map.width][map.height];
            this.heuristic = heuristic;
            nodes = new Node[map.width][map.height];
            for (int i = 0; i < map.width; i++) {
                for (int j = 0; j < map.height; j++) {
                    nodes[i][j] = new Node(i, j);
                }
            }
        }

        @Override
        public MovePath findPath(int sx, int sy, int tx, int ty) {
            if (map.isBlocked(sx, sy)) {
                return null;
            }
            nodes[sx][sy].cost = 0;
            nodes[sx][sy].depth = 0;
            closed.clear();
            open.clear();
            open.add(nodes[sx][sy]);

            nodes[tx][ty].parent = null;

            while (open.size() != 0) {
                Node current = open.getFirst();
                if (current.equals(nodes[tx][ty])) {
                    break;
                }
                closed.add(current);
                for (int x = -1; x < 2; x++) {
                    for (int y = -1; y < 2; y++) {
                        if ((x == 0 && y == 0) || (x != 0 && y != 0))
                            continue;

                        int xp = x + current.x;
                        int yp = y + current.y;

                        if (isValidLocation(sx, sy, xp, yp)) {
                            double nextStepCost = current.cost +
                                    getMovementCost(current.x, current.y, xp, yp);
                            Node neighbour = nodes[xp][yp];
                            visited[xp][yp] = true;
                            if (nextStepCost < neighbour.cost) {
                                if (open.contains(neighbour))
                                    open.remove(neighbour);
                                if (closed.contains(neighbour))
                                    closed.remove(neighbour);
                            }
                            if (!open.contains(neighbour) && !closed.contains(neighbour)) {
                                neighbour.cost = nextStepCost;
                                neighbour.heuristic = getHeuristicCost(xp, yp, tx, ty);
                                neighbour.parent = current;
                                open.add(neighbour);
                            }

                        }
                    }
                }

            }

            if (nodes[tx][ty].parent == null)
                return null;

            MovePath path = new MovePath();
            Node target = nodes[tx][ty];
            while (target != nodes[sx][sy]) {
                path.prependStep(target.x, target.y);
                target = target.parent;
            }
            return path;
        }

        private double getMovementCost(int x, int y, int xp, int yp) {
            return map.getCost(x, y, xp, yp);
        }

        private double getHeuristicCost(int xp, int yp, int tx, int ty) {
            return heuristic.getCost(xp, yp, tx, ty);
        }

        private boolean isValidLocation(int sx, int sy, int xp, int yp) {
            return !(sx == xp && sy == yp)
                    && map.isValid(xp, yp)
                    && heuristic.isValid(xp, yp);
        }

        public static class DangerCollisionHeuristic implements AStarHeuristic {

            private final Trooper[] troopers;

            public DangerCollisionHeuristic(Trooper[] troopers) {
                this.troopers = troopers;
            }

            @Override
            public double getCost(int sx, int sy, int tx, int ty) {
                double cost = tileMap.getCost(sx, sy, tx, ty)/6;
                double ds = tileMap.dangerMap[sx][sy];
                double dt = tileMap.dangerMap[tx][ty];
                return (W_MOVE[0] * cost
                        + W_MOVE[1] * ds
                        + W_MOVE[2] * dt)/3;
            }

            @Override
            public boolean isValid(int xp, int yp) {

              if(tileMap.dangerMap[xp][yp]==1000){
                    return false;
                }

                for (Trooper trooper : troopers) {
                    if (xp == trooper.getX() &&
                            yp == trooper.getY()) {
                        return false;
                    }
                }
                return tileMap.isValid(xp, yp);
            }
        }

        public static class CollisionHeuristic implements AStarHeuristic {

            private final Trooper[] troopers;

            public CollisionHeuristic(Trooper[] troopers) {
                this.troopers = troopers;
            }

            @Override
            public double getCost(int sx, int sy, int tx, int ty) {
                return tileMap.getCost(sx, sy, tx, ty);
            }

            @Override
            public boolean isValid(int xp, int yp) {
                if(tileMap.dangerMap[xp][yp]==1000){
                    return false;
                }

                for (Trooper trooper : troopers) {
                    if (xp == trooper.getX() &&
                            yp == trooper.getY()) {
                        return false;
                    }
                }
                return tileMap.isValid(xp, yp);
            }
        }

        static class Node implements Comparable {
            public int x;
            public int y;
            public double cost;
            public int depth;
            public Node parent;
            public double heuristic;

            Node(int x, int y) {
                this.x = x;
                this.y = y;
            }

            @Override
            public boolean equals(Object o) {
                return o instanceof Node && ((Node) o).x == x && ((Node) o).y == y;
            }

            @Override
            public int compareTo(Object o) {
                Node n = (Node) o;
                double f = heuristic + cost;
                double of = n.heuristic + cost;
                if (f < of) {
                    return -1;
                } else if (f > of) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }
    }

    public static class SortedList<E> extends AbstractList<E> {

        private ArrayList<E> internalList = new ArrayList<>();

        @Override
        public boolean remove(Object o) {
            return internalList.remove(o);
        }

        @Override
        public void clear() {
            internalList.clear();
        }

        @Override
        public boolean contains(Object o) {
            return internalList.contains(o);
        }

        @Override
        public void add(int position, E e) {
            internalList.add(e);
            Collections.sort(internalList, null);
        }

        @Override
        public E get(int i) {
            return internalList.get(i);
        }

        @Override
        public int size() {
            return internalList.size();
        }

        public E getFirst() {
            E result = internalList.get(0);
            internalList.remove(0);
            return result;
        }

    }

    public static class MovePath {
        private LinkedList<MoveStep> moveSteps = new LinkedList<>();

        public int getLength() {
            return moveSteps.size();
        }

        public void prependStep(int x, int y) {
            moveSteps.addFirst(new MoveStep(x, y));
        }

        public int getNextStepY() {
            return moveSteps.getFirst().y;
        }

        public int getNextStepX() {
            return moveSteps.getFirst().x;
        }

        public void draw() {
            for (MoveStep step : moveSteps) {
                step.draw();
            }
        }

        public static class MoveStep {
            public final int x;
            public final int y;

            MoveStep(int x, int y) {
                this.x = x;
                this.y = y;
            }

            public void draw() {
                //DRAWER.drawSquare(x + 0.3, y + 0.3, 0.3, 0.3);//Todo
            }
        }
    }

    public static class Me {
        public Trooper trooper;
        long myId;
        public boolean isCommander =false;

        public boolean isDied() {
            return null==trooper||trooper.getHitpoints() <= 0;
        }
    }
}
