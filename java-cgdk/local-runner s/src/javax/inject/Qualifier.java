package javax.inject;

import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({java.lang.annotation.ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Qualifier
{
}

/* Location:           /home/sss/Загрузки/aicup/local-runner (3)/local-runner.jar
 * Qualified Name:     javax.inject.Qualifier
 * JD-Core Version:    0.6.2
 */