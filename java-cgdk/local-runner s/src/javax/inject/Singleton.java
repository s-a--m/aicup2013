package javax.inject;

import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Scope
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface Singleton
{
}

/* Location:           /home/sss/Загрузки/aicup/local-runner (3)/local-runner.jar
 * Qualified Name:     javax.inject.Singleton
 * JD-Core Version:    0.6.2
 */