package javax.inject;

public abstract interface Provider
{
  public abstract Object get();
}

/* Location:           /home/sss/Загрузки/aicup/local-runner (3)/local-runner.jar
 * Qualified Name:     javax.inject.Provider
 * JD-Core Version:    0.6.2
 */