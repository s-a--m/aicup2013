package adapters;

import model.BonusType;

public class BonusAdapter extends UnitAdapter {

    private final BonusType type;


    public BonusAdapter(BonusType type) {
        this.type = type;
    }

    public BonusType getType() {
        return this.type;
    }

    public String toString() {
        return String.format("%s {id=%d, type=%s}", this.getClass().getSimpleName(), this.getId(), this.type);
    }
}
