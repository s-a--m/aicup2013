package adapters;

import core.MapBase;

import java.util.concurrent.atomic.AtomicLong;

public abstract class UnitAdapter {

    private static final AtomicLong IDS = new AtomicLong();
    private final long id;
    private MapBase map;


    protected UnitAdapter() {
        this.id = IDS.incrementAndGet();
    }

    public final long getId() {
        return this.id;
    }

    public void setMap(MapBase map) {
        this.map = map;
    }

    public int getX() {
        return this.map.getPosition(this).getFirst();
    }

    public int getY() {
        return this.map.getPosition(this).getSecond();
    }

    public final boolean equals(Object o) {
        return this == o || o != null && this.getClass() == o.getClass() && this.id == ((UnitAdapter) o).id;
    }

    public final int hashCode() {
        return Long.valueOf(this.id).hashCode();
    }

    public String toString() {
        return this.getClass().getSimpleName() + " {id=" + this.id + '}';
    }

}
