package adapters;

import model.TrooperStance;
import model.TrooperType;

public class TrooperAdapter extends UnitAdapter {

    private final PlayerAdapter player;
    private final int teammateIndex;
    private final TrooperType type;
    private TrooperStance stance;
    private int hitpoints;
    private final int maximalHitpoints;
    private int actionPoints;
    private final int initialActionPoints;
    private final double visionRange;
    private final double shootingRange;
    private final int shootCost;
    private final int standingDamage;
    private final int kneelingDamage;
    private final int proneDamage;
    private boolean holdingGrenade;
    private boolean holdingMedikit;
    private boolean holdingFieldRation;
    private double angle;
    private boolean requestEnemyDisposition;


    private TrooperAdapter(PlayerAdapter player, int teammateIndex, TrooperType type, int hitpoints, int maximalHitpoints, int initialActionPoints, double visionRange, double shootingRange, int shootCost, int standingDamage, int kneelingDamage, int proneDamage, boolean holdingGrenade, boolean holdingMedikit, boolean holdingFieldRation) {
        check(player, teammateIndex, type);
        this.player = player;
        this.teammateIndex = teammateIndex;
        this.type = type;
        this.stance = TrooperStance.STANDING;
        this.hitpoints = hitpoints;
        this.maximalHitpoints = maximalHitpoints;
        this.actionPoints = initialActionPoints;
        this.initialActionPoints = initialActionPoints;
        this.visionRange = visionRange;
        this.shootingRange = shootingRange;
        this.shootCost = shootCost;
        this.standingDamage = standingDamage;
        this.kneelingDamage = kneelingDamage;
        this.proneDamage = proneDamage;
        this.holdingGrenade = holdingGrenade;
        this.holdingMedikit = holdingMedikit;
        this.holdingFieldRation = holdingFieldRation;
    }

    private static void check(PlayerAdapter player, int teammateIndex, TrooperType type) {
        if (player == null) {
            throw new IllegalArgumentException("Argument \'player\' is \'null\'.");
        } else if (teammateIndex < 0) {
            throw new IllegalArgumentException("Argument \'teammateIndex\' is negative.");
        } else if (type == null) {
            throw new IllegalArgumentException("Argument \'type\' is \'null\'.");
        }
    }

    public PlayerAdapter getPlayer() {
        return this.player;
    }

    public int getTeammateIndex() {
        return this.teammateIndex;
    }

    public TrooperType getType() {
        return this.type;
    }

    public TrooperStance getStance() {
        return this.stance;
    }

    public void raiseStance() {
        switch (this.stance) {
            case PRONE:
                this.stance = TrooperStance.KNEELING;
                break;
            case KNEELING:
                this.stance = TrooperStance.STANDING;
                break;
            case STANDING:
                break;
            default:
                throw new IllegalStateException("Unsupported \'stance\': " + this.stance + '.');
        }
    }

    public void lowerStance() {
        switch (this.stance) {
            case PRONE:
                break;
            case KNEELING:
                this.stance = TrooperStance.PRONE;
                break;
            case STANDING:
                this.stance = TrooperStance.KNEELING;
                break;
            default:
                throw new IllegalStateException("Unsupported \'stance\': " + this.stance + '.');
        }
    }

    public int getHitpoints() {
        return this.hitpoints;
    }

    public void setHitpoints(int hitpoints) {
        this.hitpoints = hitpoints;
    }

    public int getMaximalHitpoints() {
        return this.maximalHitpoints;
    }

    public int getActionPoints() {
        return this.actionPoints;
    }

    public void setActionPoints(int actionPoints) {
        this.actionPoints = actionPoints;
    }

    public int getInitialActionPoints() {
        return this.initialActionPoints;
    }

    public double getVisionRange() {
        return this.visionRange;
    }

    public double getShootingRange() {
        if (this.type == TrooperType.SNIPER) {
            switch (this.stance) {
                case PRONE:
                    return this.shootingRange + 2.0D;
                case KNEELING:
                    return this.shootingRange + 1.0D;
                case STANDING:
                    return this.shootingRange + 0.0D;
                default:
                    throw new IllegalStateException("Unsupported trooper stance: " + this.stance + '.');
            }
        } else {
            return this.shootingRange;
        }
    }

    public int getShootCost() {
        return this.shootCost;
    }

    public int getStandingDamage() {
        return this.standingDamage;
    }

    public int getKneelingDamage() {
        return this.kneelingDamage;
    }

    public int getProneDamage() {
        return this.proneDamage;
    }

    public int getDamage() {
        switch (this.stance) {
            case PRONE:
                return this.proneDamage;
            case KNEELING:
                return this.kneelingDamage;
            case STANDING:
                return this.standingDamage;
            default:
                throw new IllegalStateException("Unsupported \'stance\': " + this.stance + '.');
        }
    }

    public boolean isHoldingGrenade() {
        return this.holdingGrenade;
    }

    public void setHoldingGrenade(boolean holdingGrenade) {
        this.holdingGrenade = holdingGrenade;
    }

    public boolean isHoldingMedikit() {
        return this.holdingMedikit;
    }

    public void setHoldingMedikit(boolean holdingMedikit) {
        this.holdingMedikit = holdingMedikit;
    }

    public boolean isHoldingFieldRation() {
        return this.holdingFieldRation;
    }

    public void setHoldingFieldRation(boolean holdingFieldRation) {
        this.holdingFieldRation = holdingFieldRation;
    }

    public double getAngle() {
        return this.angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public boolean isRequestEnemyDisposition() {
        return this.requestEnemyDisposition;
    }

    public void setRequestEnemyDisposition(boolean requestEnemyDisposition) {
        this.requestEnemyDisposition = requestEnemyDisposition;
    }

    public String toString() {
        return String.format("%s {id=%d, player.name=\'%s\', type=%s, x=%d, y=%d}",
                this.getClass().getSimpleName(),
                this.getId(),
                this.player == null ? null : this.player.getPlayerName(),
                this.type,
                this.getX(),
                this.getY());
    }

    public static TrooperAdapter create(PlayerAdapter player, int teammateIndex, TrooperType trooperType) {
        switch (trooperType) {
            case COMMANDER:
                return new TrooperAdapter(player, teammateIndex, trooperType, 100, 100, 10, 8.0D, 7.0D, 3, 15, 20, 25, false, false, false);
            case FIELD_MEDIC:
                return new TrooperAdapter(player, teammateIndex, trooperType, 100, 100, 10, 7.0D, 5.0D, 2, 9, 12, 15, false, true, false);
            case SOLDIER:
                return new TrooperAdapter(player, teammateIndex, trooperType, 120, 100, 10, 7.0D, 8.0D, 4, 25, 30, 35, true, false, false);
            case SNIPER:
                return new TrooperAdapter(player, teammateIndex, trooperType, 100, 100, 10, 7.0D, 10.0D, 9, 65, 80, 95, false, false, false);
            case SCOUT:
                return new TrooperAdapter(player, teammateIndex, trooperType, 100, 100, 12, 9.0D, 6.0D, 4, 20, 25, 30, false, false, true);
            default:
                throw new IllegalArgumentException("Unsupported trooper type: " + trooperType + '.');
        }
    }

}
