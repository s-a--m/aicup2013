package adapters;

import model.TrooperType;
import org.apache.commons.io.IOUtils;
import players.KeyboardPlayer;
import players.Player;

import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class PlayerAdapter {

    private static final AtomicLong IDs = new AtomicLong();
    private final long id;
    private final String playerName;
    private final Player controller;
    private boolean failed;
    private String errorMessage;
    private int score;
    private int enemyFrags;
    private int frags;
    private long remaininTime;
    private int approximateX;
    private int approximateY;
    private final Map<TrooperType, TrooperAdapter> troopers = new EnumMap<>(TrooperType.class);


    public PlayerAdapter(String playerName, Player controller) {
        this.id = IDs.incrementAndGet();
        this.playerName = playerName;
        this.controller = controller;
    }

    public final long getId() {
        return this.id;
    }

    public String getPlayerName() {
        return this.playerName;
    }

    public Player getController() {
        return this.controller;
    }

    public boolean isKeyboardPlayer() {
        return this.controller instanceof KeyboardPlayer;
    }

    public boolean isStrategyCrashed() {
        return this.failed;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void error(String message) {
        if (!this.isKeyboardPlayer()) {
            this.failed = true;
            this.errorMessage = message;
            IOUtils.closeQuietly(this.controller);
        }
    }

    public int getScore() {
        return this.score;
    }

    public void increaseScore(int value) {
        this.score += value;
    }

    public int getFrags() {
        return this.enemyFrags;
    }

    public void setFrags(int enemyFrags) {
        this.enemyFrags = enemyFrags;
    }

    public int getCasualities() {
        return this.frags;
    }

    public void setCasualties(int frags) {
        this.frags = frags;
    }

    public long getRemainingTime() {
        return this.remaininTime;
    }

    public void setRemainingTime(long remainingTime) {
        this.remaininTime = remainingTime;
    }

    public void updateTime(long delta) {
        this.remaininTime -= delta;
    }

    public int getApproximateX() {
        return this.approximateX;
    }

    public void setApproximateX(int approximateX) {
        this.approximateX = approximateX;
    }

    public int getApproximateY() {
        return this.approximateY;
    }

    public void setApproximateY(int approximateY) {
        this.approximateY = approximateY;
    }

    public Map<TrooperType, TrooperAdapter> getTroopers() {
        return Collections.unmodifiableMap(this.troopers);
    }

    public void addTrooper(TrooperAdapter newTrooper) {
        for (TrooperAdapter trooper : this.troopers.values()) {
            if (trooper.getTeammateIndex() == newTrooper.getTeammateIndex()) {
                throw new IllegalStateException(String.format("Trooper with teammate index %d is already added to the %s.", newTrooper.getTeammateIndex(), this));
            }
        }
        if (this.troopers.get(newTrooper.getType()) != null) {
            throw new IllegalStateException(String.format("Trooper with type \'%s\' is already added to the %s.", newTrooper.getType(), this));
        }
        this.troopers.put(newTrooper.getType(), newTrooper);
    }

    public final boolean equals(Object o) {
        return this == o || o != null && this.getClass() == o.getClass() && this.id == ((PlayerAdapter) o).id;
    }

    public final int hashCode() {
        return Long.valueOf(this.id).hashCode();
    }

    public String toString() {
        return String.format("%s {id=%d, name=\'%s\'}", this.getClass().getSimpleName(), this.id, this.playerName);
    }

}
