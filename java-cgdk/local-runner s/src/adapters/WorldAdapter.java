package adapters;

import core.Effect;
import core.FogOfWar;
import core.helpers.MoveHelper;
import core.helpers.World;
import model.Move;
import model.Trooper;

import java.util.Arrays;

public final class WorldAdapter extends World {

    public static final Effect[] EFFECTS = new Effect[0];
    public static final FogOfWar[] FOG_OF_WARS = new FogOfWar[0];
    private final Trooper trooper;
    private final Move move;
    private final Effect[] effects;
    private final FogOfWar[] fogOfWars;
    private final String mapName;


    public WorldAdapter(World world, String mapName, Trooper trooper, Move move, Effect[] effects, FogOfWar[] fogOfWars) {
        super(world.getMoveIndex(), world.getWidth(), world.getHeight(), world.getPlayers(), world.getTroopers(), world.getBonuses(), world.getCells(), world.getCellVisibilities1());
        this.mapName = mapName;
        this.trooper = trooper;
        this.move = MoveHelper.copy(move);
        this.effects = effects == null ? EFFECTS : Arrays.copyOf(effects, effects.length);
        this.fogOfWars = fogOfWars == null ? FOG_OF_WARS : Arrays.copyOf(fogOfWars, fogOfWars.length);
    }

    public Trooper getTrooper() {
        return this.trooper;
    }

    public Move getMove() {
        return MoveHelper.copy(this.move);
    }

    public Effect[] getEffects() {
        return Arrays.copyOf(this.effects, this.effects.length);
    }

    public FogOfWar[] getFogOfWars() {
        return Arrays.copyOf(this.fogOfWars, this.fogOfWars.length);
    }

}
