package gameserver;

import com.google.inject.AbstractModule;
import core.Engine;
import core.MapBase;

public final class GameModule extends AbstractModule {

    protected void configure() {
        this.bind(Engine.class).toInstance(new GameEngine());
        this.bind(MapBase.class).toInstance(new GameMap());
    }
}
