package gameserver;

import adapters.*;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import core.*;
import core.future.AbstractFutureValue;
import core.future.FutureValue;
import core.helpers.*;
import core.helpers.World;
import core.helpers.maps.CellMap;
import core.helpers.maps.VisibilityMap;
import core.tuple.Location;
import model.*;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import renderers.FileRenderer;
import renderers.RemoteStorageRenderer;
import renderers.Renderer;
import renderers.ScreenRenderer;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class GameEngine implements Engine {

    private static final Logger LOGGER = LoggerFactory.getLogger(GameEngine.class);
    @Inject
    private MapBase map;
    private Map<String, String> options = new HashMap<>();
    private final AtomicBoolean fail = new AtomicBoolean(false);
    private final AtomicReference<String> message = new AtomicReference<>();
    private int width;
    private int height;
    private int moveCount;
    private int moveIndex;
    private final List<Renderer> renderers = new ArrayList<>();
    private final List<PlayerAdapter> players = new ArrayList<>();
    private final List<PlayerAdapter> keyboardPlayers = new ArrayList<>();
    private final List<GameEvent> events = new ArrayList<>();
    private final Map<Integer, Map<TrooperType, Location>> startedLocations = new HashMap<>();
    private final List<TrooperAdapter> troopers = new ArrayList<>();
    private boolean[][][][][] cellVisibilities;
    private ExecutorService threadPool;
    private long start;

    public static boolean ensureProtocolVersion(Integer version) {
        return version != null && Arrays.asList(new Integer[]{1, 2}).contains(version);
    }


    public void init(Map<String, String> properties, List<String> players) {
        LOGGER.info("Game has been started.");
        this.start = System.currentTimeMillis();
        this.options = Collections.unmodifiableMap(new HashMap<>(properties));
        this.moveCount = OptionsHelper.getMoveCount(this.options);
        this.loadMap();
        this.addRenderers();
        this.addPlayers(players);
        if (!PlayerHelper.allCrashed(this.players)) {
            this.addPlayerUnits();
            this.addTroopers();
            this.addBonuses();
            this.writeGameContext();
            this.addCollisionListeners();
            this.addEvents();
        }

        LOGGER.info("Game has been initialized.");
    }

    public void run() {
        for (this.moveIndex = 0;
             this.moveIndex < this.moveCount && !this.fail.get() && !PlayerHelper.allCrashed(this.players);
             ++this.moveIndex) {
            if (PlayerHelper.oneAlive(this.players)) {
                for (PlayerAdapter player : this.players) {
                    if (PlayerHelper.isAlive(player)) {
                        player.increaseScore(100);
                        return;
                    }
                }

                this.last();
                return;
            }

            this.step();
            this.notifyEvents();
        }
    }

    public void finish() {
        LOGGER.info("Game has been finished in " + (System.currentTimeMillis() - this.start) + " ms.");
        this.closeControllers();
        this.closeRenderers();
        this.writeStrategyDescription();
        this.writeAttributes();
        this.writeResults();
    }

    private void closeControllers() {
        for (PlayerAdapter player : this.players) {
            IOUtils.closeQuietly(player.getController());
        }
    }

    private void closeRenderers() {

        for (Renderer renderer : this.renderers) {
            try {
                renderer.close();
            } catch (IOException e) {
                LOGGER.error(String.format("Can\'t close renderer \'%s\'.", renderer.getClass().getSimpleName()), e);
                this.error(String.format("Can\'t close renderer \'%s\': %s", renderer.getClass().getSimpleName(), ExceptionUtils.getStackTrace(e)));
            }
        }

    }

    private void writeStrategyDescription() {
        String file = this.options.get("strategy-description-file");
        if (!StringUtils.isBlank(file)) {
            try {
                StringBuilder sb = new StringBuilder();

                for (PlayerAdapter ignored : this.players)
                    sb.append("-\n");

                FileUtils.writeByteArrayToFile(new File(file), sb.toString().getBytes("UTF-8"));
            } catch (IOException e) {
                LOGGER.error(String.format("Can\'t write strategy descriptions to file \'%s\'.", file), e);
            }
        }

    }

    private void writeAttributes() {
        String file = this.options.get("attributes-file");
        if (!StringUtils.isBlank(file)) {
            try {
                HashMap<String, String> map = new HashMap<>();
                map.put("map", this.options.get("map"));
                byte[] buffer = (new GsonBuilder()).create().toJson(map).getBytes("UTF-8");
                FileUtils.writeByteArrayToFile(new File(file), buffer);
            } catch (IOException e) {
                LOGGER.error(String.format("Can\'t write attributes to file \'%s\'.", new Object[]{file}), e);
            }
        }

    }

    private void writeResults() {
        String file = this.options.get("results-file");
        if (!StringUtils.isBlank(file)) {
            try {
                StringBuilder results = new StringBuilder();
                if (this.fail.get()) {
                    LOGGER.error("Game has failed with the message: " + this.message.get());
                    results.append("FAILED\n").append(this.message.get()).append('\n');
                } else {
                    results.append("OK\nSEED ").append(this.options.get("seed")).append('\n');
                    Map<Integer, Integer> placesByScore = this.getPlacesByScore();

                    for (PlayerAdapter player : this.players) {
                        results.append(placesByScore.get(player.getScore())).append(' ')
                                .append(player.getScore())
                                .append(player.isStrategyCrashed() ? " CRASHED" : " OK");
                        if (!Boolean.parseBoolean(this.options.get("local-test"))) {
                            results.append(' ').append(Base64.encodeBase64URLSafeString(ArchiveHelper.compress(this.getPlayerOutput(player).getBytes("UTF-8"), 9)));
                        }
                        results.append('\n');
                    }
                }

                FileUtils.writeByteArrayToFile(new File(file), results.toString().getBytes("UTF-8"));
            } catch (IOException e) {
                LOGGER.error(String.format("Can\'t write results to file \'%s\'.", new Object[]{file}), e);
            }
        }

    }

    private String getPlayerOutput(PlayerAdapter player) {
        StringBuilder sb = new StringBuilder();
        if (!StringHelper.isEmpty(player.getErrorMessage())) {
            sb.append(player.getErrorMessage());
        }


        return sb.toString();
    }

    private static void appendFile(StringBuilder sb, String s, File parent, String child) {
        String content = readFile(new File(parent, child));
        if (!StringHelper.isEmptyLine(content)) {
            if (sb.length() > 0) {
                sb.append("\r\n\r\n");
            }

            sb.append(s).append("\r\n").append(content);
        }

    }

    private static String readFile(File file) {
        if (!file.isFile()) {
            return "";
        } else {
            List<String> lines;
            try {
                lines = FileUtils.readLines(file, Charsets.UTF_8);

                while (!lines.isEmpty() && StringHelper.isEmptyLine(lines.get(0))) {
                    lines.remove(0); // удаляем пустые строки в начале файла
                }

                while (!lines.isEmpty() && StringHelper.isEmptyLine(lines.get(lines.size() - 1))) {
                    lines.remove(lines.size() - 1); // удаляем пустые строки в конце файла
                }

                if (lines.isEmpty()) {
                    return "";
                }
            } catch (IOException e) {
                LOGGER.error("Can\'t read file \'" + file.getAbsolutePath() + "\'.", e);
                return "";
            }

            return StringUtils.join(StringHelper.fitLines(lines, 100, 17), "\r\n");
        }
    }

    private Map<Integer, Integer> getPlacesByScore() {
        HashMap<Integer, Integer> scoreMap = new HashMap<>();
        ArrayList<PlayerAdapter> playersByScore = new ArrayList<>(this.players);
        Collections.sort(playersByScore, new Comparator<PlayerAdapter>() {
            @Override
            public int compare(PlayerAdapter left, PlayerAdapter right) {
                return Integer.valueOf(right.getScore()).compareTo(left.getScore());
            }
        });

        for (int i = playersByScore.size() - 1; i >= 0; --i) {
            scoreMap.put(playersByScore.get(i).getScore(), i + 1);
        }

        return scoreMap;
    }

    private void last() {
        this.drawWorld(WorldHelper.createWorld(
                this.moveIndex,
                this.width, this.height,
                Collections.unmodifiableList(this.players),
                this.map.getUnits(),
                this.map.getCells(),
                this.cellVisibilities,
                null,
                false), null, null, null);
    }

    private void drawWorld(World world, Trooper trooper, Move move, Effect[] effects) {
        WorldAdapter worldAdapter = WorldHelper.createAdapter(
                world,
                this.options.get("map"),
                trooper, move, effects, this.getFogOfWars(world));

        for (Renderer renderer : this.renderers) {
            try {
                renderer.render(worldAdapter);
            } catch (IOException e) {
                LOGGER.error(String.format("Can\'t render world using renderer \'%s\' (moveIndex=%d).", renderer.getClass().getSimpleName(), this.moveIndex), e);
                this.error(String.format("Can\'t render world using renderer \'%s\' (moveIndex=%d): %s", renderer.getClass().getSimpleName(), this.moveIndex, ExceptionUtils.getStackTrace(e)));
            }
        }

    }

    private FogOfWar[] getFogOfWars(World world) {
        ArrayList<FogOfWar> fogOfWars = new ArrayList<>();
        model.Player[] players = world.getPlayers();

        for (model.Player player : players) {
            fogOfWars.add(new FogOfWar(player.getId(), Base64.encodeBase64URLSafeString(this.getVisibilityBitSet(world, player))));
        }

        return fogOfWars.toArray(new FogOfWar[fogOfWars.size()]);
    }

    private byte[] getVisibilityBitSet(World world, model.Player player) {
        ArrayList<Trooper> visibleTroopers = new ArrayList<>();
        Trooper[] troopers = world.getTroopers();

        for (Trooper trooper : troopers) {
            if (trooper.getPlayerId() == player.getId()) {
                visibleTroopers.add(trooper);
            }
        }

        BitSet mask = new BitSet(2 * this.width * this.height);

        for (int y = 0; y < this.height; ++y) {
            for (int x = 0; x < this.width; ++x) {
                for (Trooper trooper : visibleTroopers) {
                    byte value = this.getVisibilityValue(trooper, x, y);
                    if (value == 0) continue;
                    byte b = 0;
                    if (mask.get(2 * (y * this.width + x))) b = (byte) (b + 2);
                    if (mask.get(2 * (y * this.width + x) + 1)) ++b;
                    if (value <= b) continue;
                    mask.set(2 * (y * this.width + x), (value & 2) != 0);
                    mask.set(2 * (y * this.width + x) + 1, (value & 1) != 0);
                }
            }
        }

        return mask.toByteArray();
    }

    private byte getVisibilityValue(Trooper trooper, int x, int y) {
        TrooperStance viewerStance = trooper.getStance();
        byte result = 0;
        if (WorldHelper.isVisible(this.cellVisibilities, trooper.getVisionRange(), trooper.getX(), trooper.getY(), viewerStance, x, y, TrooperStance.STANDING)) {
            ++result;
            if (WorldHelper.isVisible(this.cellVisibilities, trooper.getVisionRange(), trooper.getX(), trooper.getY(), viewerStance, x, y, TrooperStance.KNEELING)) {
                ++result;
                if (WorldHelper.isVisible(this.cellVisibilities, trooper.getVisionRange(), trooper.getX(), trooper.getY(), viewerStance, x, y, TrooperStance.PRONE)) {
                    ++result;
                }
            }
        }

        return result;
    }

    private void step() {

        for (TrooperAdapter trooper : this.troopers) {
            final PlayerAdapter player = trooper.getPlayer();
            if (TrooperHelper.isAlive(trooper) && !player.isStrategyCrashed()) {
                TrooperAdapter commander = player.getTroopers().get(TrooperType.COMMANDER);
                if (TrooperHelper.isAlive(commander) && trooper.getType() != TrooperType.COMMANDER
                        && StrictMath.hypot((double) (commander.getX() - trooper.getX()), (double) (commander.getY() - trooper.getY())) <= 5.0D
                        && trooper.getType() != TrooperType.SCOUT) {
                    trooper.setActionPoints(trooper.getInitialActionPoints() + 2);
                } else trooper.setActionPoints(trooper.getInitialActionPoints());

                for (; trooper.getActionPoints() > 0 && TrooperHelper.isAlive(trooper) && !player.isStrategyCrashed() && !PlayerHelper.oneAlive(this.players); this.notifyEvents()) {
                    List<UnitAdapter> units = this.map.getUnits();
                    CellType[][] cells = this.map.getCells();
                    final Trooper trooper1 = TrooperHelper.createTrooper(trooper, Arrays.asList(player));
                    final core.helpers.World w = WorldHelper.createWorld(
                            this.moveIndex,
                            this.width, this.height,
                            Collections.unmodifiableList(this.players),
                            units, cells, this.cellVisibilities,
                            Arrays.asList(player),
                            trooper.getType() == TrooperType.COMMANDER && trooper.isRequestEnemyDisposition());
                    trooper.setRequestEnemyDisposition(false);
                    Trooper trooper2 = TrooperHelper.createTrooper(trooper, null);
                    World world = WorldHelper.createWorld(
                            this.moveIndex,
                            this.width, this.height,
                            Collections.unmodifiableList(this.players),
                            units, cells, this.cellVisibilities,
                            null,
                            false);
                    if (player.isKeyboardPlayer()) {
                        this.drawWorld(w, trooper1, null, null);
                    }

                    Future<Move> future = this.threadPool.submit(new Callable<Move>() {
                        public Move call() {
                            return player.getController().move(trooper1, w);
                        }
                    });
                    FutureValue<Move> futureMove = new FutureValue<>();
                    if (!this.requestStrategy(player, future, futureMove)) break;

                    Move move = futureMove.get();
                    if (move == null || move.getAction() == null || move.getAction() == ActionType.END_TURN) break;

                    List<Effect> effects = this.action(trooper, move);
                    if (trooper.getPlayer().isStrategyCrashed()) move.setStrategyCrashed(true);

                    if (player.isKeyboardPlayer())
                        this.drawWorld(w, trooper1, move, effects.toArray(new Effect[effects.size()]));
                    else if (!this.keyboardPlayers.isEmpty() && !PlayerHelper.allDead(this.keyboardPlayers)) {
                        if (WorldHelper.isVisibleByPlayers(this.cellVisibilities, this.keyboardPlayers, trooper))
                            this.drawWorld(WorldHelper.createWorld(
                                    this.moveIndex,
                                    this.width, this.height,
                                    Collections.unmodifiableList(this.players),
                                    this.map.getUnits(),
                                    cells,
                                    this.cellVisibilities,
                                    this.keyboardPlayers,
                                    false),
                                    TrooperHelper.createTrooper(trooper, this.keyboardPlayers), move, effects.toArray(new Effect[effects.size()]));
                    } else this.drawWorld(world, trooper2, move, effects.toArray(new Effect[effects.size()]));
                }
            }
        }

    }

    private <T> boolean requestStrategy(PlayerAdapter player, Future<T> future, AbstractFutureValue<T> value) {
        boolean debug = Boolean.parseBoolean(this.options.get("debug"));
        long start = System.currentTimeMillis();

        try {
            if (debug) value.set(future.get(10L, TimeUnit.MINUTES));
            else value.set(future.get(2000L, TimeUnit.MILLISECONDS));
        } catch (InterruptedException e) {
            LOGGER.error(String.format("Strategy adapter \'%s\' of %s has been interrupted at a move %d.", player.getController().getClass().getSimpleName(), player, this.moveIndex), e);
            future.cancel(true);
            player.error("Ожидание отклика от стратегии было прервано.");
            return false;
        } catch (ExecutionException e1) {
            LOGGER.warn(String.format("Strategy adapter \'%s\' of %s has failed at a move %d.", player.getController().getClass().getSimpleName(), player, this.moveIndex), e1);
            future.cancel(true);
            player.error("Процесс стратегии непредвиденно завершился на ходе " + this.moveIndex + '.');
            return false;
        } catch (TimeoutException e2) {
            LOGGER.warn(String.format("Strategy adapter \'%s\' of %s has timed out at a move %d.", player.getController().getClass().getSimpleName(), player, this.moveIndex), e2);
            future.cancel(true);
            player.error("Процесс стратегии превысил ограничение по времени на ход.");
            return false;
        }

        if (!debug) {
            long delta = System.currentTimeMillis() - start;
            long regaining = player.getRemainingTime();
            if (regaining < delta) {
                LOGGER.warn(String.format("Strategy adapter \'%s\' of %s has consumed all available game time at a move %d.", player.getController().getClass().getSimpleName(), player, this.moveIndex));
                player.error("Процесс стратегии превысил ограничение по времени на игру.");
                return false;
            }

            player.updateTime(delta);
        }

        return true;
    }

    private void notifyEvents() {
        for (GameEvent event : this.events) {
            event.handle(this.map, this.moveIndex);
        }
    }

    private List<Effect> action(TrooperAdapter trooper, Move move) {
        switch (move.getAction()) {
            case MOVE:
                return this.move(trooper, move);
            case SHOOT:
                return this.shoot(trooper, move);
            case RAISE_STANCE:
                return raiseStance(trooper, move);
            case LOWER_STANCE:
                return lowerStance(trooper, move);
            case THROW_GRENADE:
                return this.throwGrenade(trooper, move);
            case USE_MEDIKIT:
                return this.useMedikit(trooper, move);
            case EAT_FIELD_RATION:
                return eatFieldRation(trooper, move);
            case HEAL:
                return this.heal(trooper, move);
            case REQUEST_ENEMY_DISPOSITION:
                return requestEnemyDisposition(trooper, move);
            default:
                throw new IllegalArgumentException("Unsupported action: " + move.getAction() + '.');
        }
    }

    private static void errorOutOfMap(TrooperAdapter trooper, Move move) {
        trooper.getPlayer().error(trooper + " не может выполнить " + move + ": " + "в качестве цели указана клетка за пределами игрового поля.");
    }

    private static void errorNotEnoughActionPoints(TrooperAdapter trooper, Move move) {
        trooper.getPlayer().error(trooper + " не может выполнить " + move + ": недостаточно очков действия.");
    }

    private List<Effect> move(TrooperAdapter trooper, Move move) {
        ArrayList<Effect> effects = new ArrayList<>(1);
        int moveCost = getMoveCost(trooper);
        if (trooper.getActionPoints() < moveCost) errorNotEnoughActionPoints(trooper, move);
        else {
            int x = trooper.getX();
            int y = trooper.getY();
            boolean canMove = move.getDirection() == null
                    ? this.map.moveTo(trooper, move.getX(), move.getY())
                    : this.map.moveTo(trooper, move.getDirection());
            trooper.setActionPoints(trooper.getActionPoints() - moveCost);
            effects.add(new Effect(x, y, EffectType.ACTION_POINTS_CHANGE, -moveCost));
            if (canMove) {
                if (x != trooper.getX() || y != trooper.getY())
                    trooper.setAngle(AngleHelper.getAngle((double) x, (double) y, (double) trooper.getX(), (double) trooper.getY()));
            } else move.setStrategyCrashed(true);
        }

        return effects;
    }

    private List<Effect> shoot(TrooperAdapter trooper, Move move) {
        ArrayList<Effect> effects = new ArrayList<>(2);
        if (trooper.getActionPoints() < trooper.getShootCost()) errorNotEnoughActionPoints(trooper, move);
        else {
            int trooperX = trooper.getX();
            int trooperY = trooper.getY();
            int x;
            int y;
            if (move.getDirection() != null) {
                x = trooperX + move.getDirection().getOffsetX();
                y = trooperY + move.getDirection().getOffsetY();
            } else {
                x = move.getX();
                y = move.getY();
            }

            if (x < 0 || x >= this.width || y < 0 || y >= this.height) errorOutOfMap(trooper, move);
            else {
                if (x == trooperX && y == trooperY)
                    trooper.getPlayer().error(trooper + " не может выполнить " + move + ": " + "в качестве цели указана собственная клетка бойца.");
                else if (StrictMath.hypot((double) (x - trooperX), (double) (y - trooperY)) > trooper.getShootingRange())
                    trooper.getPlayer().error(trooper + " не может выполнить " + move + ": " + "в качестве цели указана клетка за пределами радиуса стрельбы бойца.");
                else {

                    for (UnitAdapter unit : this.map.getUnitsAtPoint(new Location(x, y))) {
                        if (unit.getClass() == TrooperAdapter.class) {
                            TrooperAdapter targetTrooper = (TrooperAdapter) unit;
                            if (WorldHelper.isVisible(
                                    this.cellVisibilities,
                                    trooper.getShootingRange(),
                                    trooperX, trooperY,
                                    TrooperHelper.getStance(trooper),
                                    unit.getX(), unit.getY(),
                                    TrooperHelper.getStance(targetTrooper))) {
                                int damage = TrooperHelper.hit(targetTrooper, trooper, this.map, this.players);
                                effects.add(new Effect(x, y, EffectType.HITPOINTS_CHANGE, -damage));
                            }
                        }
                    }

                    trooper.setActionPoints(trooper.getActionPoints() - trooper.getShootCost());
                    effects.add(new Effect(trooperX, trooperY, EffectType.ACTION_POINTS_CHANGE, -trooper.getShootCost()));
                    trooper.setAngle(AngleHelper.getAngle((double) trooperX, (double) trooperY, (double) x, (double) y));
                }
            }
        }

        return effects;
    }

    private static List<Effect> raiseStance(TrooperAdapter trooper, Move move) {
        ArrayList<Effect> effects = new ArrayList<>(1);
        if (trooper.getActionPoints() < 2) errorNotEnoughActionPoints(trooper, move);
        else {
            trooper.raiseStance();
            trooper.setActionPoints(trooper.getActionPoints() - 2);
            effects.add(new Effect(trooper.getX(), trooper.getY(), EffectType.ACTION_POINTS_CHANGE, -2));
        }

        return effects;
    }

    private static List<Effect> lowerStance(TrooperAdapter trooper, Move move) {
        ArrayList<Effect> effects = new ArrayList<>(1);
        if (trooper.getActionPoints() < 2) errorNotEnoughActionPoints(trooper, move);
        else {
            trooper.lowerStance();
            trooper.setActionPoints(trooper.getActionPoints() - 2);
            effects.add(new Effect(trooper.getX(), trooper.getY(), EffectType.ACTION_POINTS_CHANGE, -2));
        }

        return effects;
    }

    private List<Effect> throwGrenade(TrooperAdapter trooper, Move move) {
        ArrayList<Effect> effects = new ArrayList<>(2);
        if (trooper.getActionPoints() < 8) errorNotEnoughActionPoints(trooper, move);
        else if (!trooper.isHoldingGrenade())
            trooper.getPlayer().error(trooper + " не может выполнить " + move + ": у бойца нет гранаты.");
        else {
            int x = trooper.getX();
            int y = trooper.getY();
            int targetX;
            int targetY;
            if (move.getDirection() != null) {
                targetX = x + move.getDirection().getOffsetX();
                targetY = y + move.getDirection().getOffsetY();
            } else {
                targetX = move.getX();
                targetY = move.getY();
            }

            if (targetX < 0 || targetX >= this.width || targetY < 0 || targetY >= this.height)
                errorOutOfMap(trooper, move);
            else {
                if (StrictMath.hypot((double) (targetX - x), (double) (targetY - y)) > 5.0D)
                    trooper.getPlayer().error(trooper + " не может выполнить " + move + ": " + "в качестве цели указана клетка за пределами дальности броска гранаты.");
                else {
                    for (Direction direction : Direction.values()) {
                        Location location = new Location(targetX + direction.getOffsetX(), targetY + direction.getOffsetY());

                        for (UnitAdapter unit : this.map.getUnitsAtPoint(location)) {
                            if (unit.getClass() == TrooperAdapter.class) {
                                TrooperAdapter target = (TrooperAdapter) unit;
                                int direct = direction == Direction.CURRENT_POINT ? 80 : 60;
                                direct = TrooperHelper.hit(target, trooper.getPlayer(), this.map, this.players, direct);
                                effects.add(new Effect(location.getFirst(), location.getSecond(), EffectType.HITPOINTS_CHANGE, -direct));
                            }
                        }
                    }

                    trooper.setHoldingGrenade(false);
                    trooper.setActionPoints(trooper.getActionPoints() - 8);
                    effects.add(new Effect(x, y, EffectType.ACTION_POINTS_CHANGE, -8));
                    if (x != targetX || y != targetY)
                        trooper.setAngle(AngleHelper.getAngle((double) x, (double) y, (double) targetX, (double) targetY));
                }
            }
        }

        return effects;
    }

    private List<Effect> useMedikit(TrooperAdapter trooper, Move move) {
        ArrayList<Effect> effects = new ArrayList<>(2);
        if (trooper.getActionPoints() < 2)
            errorNotEnoughActionPoints(trooper, move);
        else if (!trooper.isHoldingMedikit())
            trooper.getPlayer().error(trooper + " не может выполнить " + move + ": у бойца нет аптечки.");
        else {
            int x;
            int y;
            if (move.getDirection() != null) {
                x = trooper.getX() + move.getDirection().getOffsetX();
                y = trooper.getY() + move.getDirection().getOffsetY();
            } else {
                x = move.getX();
                y = move.getY();
            }

            if (x < 0 || x >= this.width || y < 0 || y >= this.height) errorOutOfMap(trooper, move);
            else {
                if (StrictMath.abs(x - trooper.getX()) + StrictMath.abs(y - trooper.getY()) > 1)
                    trooper.getPlayer().error(trooper + " не может выполнить " + move + ": " + "в качестве цели указана клетка за пределами дальности использования аптечки.");
                else {

                    for (UnitAdapter unit : this.map.getUnitsAtPoint(new Location(x, y))) {
                        if (unit.getClass() == TrooperAdapter.class) {
                            TrooperAdapter target = (TrooperAdapter) unit;
                            int healpoints = x == trooper.getX() && y == trooper.getY() ? 30 : 50;
                            int hitpoints = target.getHitpoints();
                            int newHitpoints = StrictMath.min(hitpoints + healpoints, target.getMaximalHitpoints());
                            if (newHitpoints != hitpoints) {
                                target.setHitpoints(newHitpoints);
                                effects.add(new Effect(x, y, EffectType.HITPOINTS_CHANGE, newHitpoints - hitpoints));
                            }

                            trooper.setHoldingMedikit(false);
                            break;
                        }
                    }

                    trooper.setActionPoints(trooper.getActionPoints() - 2);
                    effects.add(new Effect(trooper.getX(), trooper.getY(), EffectType.ACTION_POINTS_CHANGE, -2));
                    if (trooper.getX() != x || trooper.getY() != y) {
                        trooper.setAngle(AngleHelper.getAngle((double) trooper.getX(), (double) trooper.getY(), (double) x, (double) y));
                    }
                }
            }
        }

        return effects;
    }

    private static List<Effect> eatFieldRation(TrooperAdapter trooper, Move move) {
        ArrayList<Effect> effects = new ArrayList<>(1);
        if (trooper.getActionPoints() < 2) errorNotEnoughActionPoints(trooper, move);
        else if (!trooper.isHoldingFieldRation())
            trooper.getPlayer().error(trooper + " не может выполнить " + move + ": у бойца нет сухого пайка.");
        else {
            int actionPoints = trooper.getActionPoints();
            int newActionPoints = actionPoints - 2 + 5;
            newActionPoints = StrictMath.min(newActionPoints, trooper.getInitialActionPoints());
            trooper.setHoldingFieldRation(false);
            if (newActionPoints != actionPoints) {
                trooper.setActionPoints(newActionPoints);
                effects.add(new Effect(trooper.getX(), trooper.getY(), EffectType.ACTION_POINTS_CHANGE, newActionPoints - actionPoints));
            }
        }

        return effects;
    }

    private List<Effect> heal(TrooperAdapter trooper, Move move) {
        ArrayList<Effect> effects = new ArrayList<>(2);
        if (trooper.getActionPoints() < 1)
            errorNotEnoughActionPoints(trooper, move);
        else if (trooper.getType() != TrooperType.FIELD_MEDIC)
            trooper.getPlayer().error(trooper + " не может выполнить " + move + ": боец не является медиком.");
        else {
            int x;
            int y;
            if (move.getDirection() != null) {
                x = trooper.getX() + move.getDirection().getOffsetX();
                y = trooper.getY() + move.getDirection().getOffsetY();
            } else {
                x = move.getX();
                y = move.getY();
            }

            if (x < 0 || x >= this.width || y < 0 || y >= this.height) errorOutOfMap(trooper, move);
            else {
                if (StrictMath.abs(x - trooper.getX()) + StrictMath.abs(y - trooper.getY()) > 1) {
                    trooper.getPlayer().error(trooper + " не может выполнить " + move + ": " + "в качестве цели указана клетка за пределами дальности лечения.");
                } else {

                    for (UnitAdapter unit : this.map.getUnitsAtPoint(new Location(x, y))) {
                        if (unit.getClass() == TrooperAdapter.class) {
                            TrooperAdapter target = (TrooperAdapter) unit;
                            int healed = x == trooper.getX() && y == trooper.getY() ? 3 : 5;
                            int hitpoints = target.getHitpoints();
                            int newHitpoints = StrictMath.min(hitpoints + healed, target.getMaximalHitpoints());
                            if (newHitpoints != hitpoints) {
                                target.setHitpoints(newHitpoints);
                                effects.add(new Effect(x, y, EffectType.HITPOINTS_CHANGE, newHitpoints - hitpoints));
                            }
                            break;
                        }
                    }

                    trooper.setActionPoints(trooper.getActionPoints() - 1);
                    effects.add(new Effect(trooper.getX(), trooper.getY(), EffectType.ACTION_POINTS_CHANGE, -1));
                    if (trooper.getX() != x || trooper.getY() != y) {
                        trooper.setAngle(AngleHelper.getAngle((double) trooper.getX(), (double) trooper.getY(), (double) x, (double) y));
                    }
                }
            }
        }

        return effects;
    }

    private static List<Effect> requestEnemyDisposition(TrooperAdapter trooper, Move move) {
        ArrayList<Effect> effects = new ArrayList<>(1);
        if (trooper.getActionPoints() < 10)
            errorNotEnoughActionPoints(trooper, move);
        else if (trooper.getType() != TrooperType.COMMANDER) {
            trooper.getPlayer().error(trooper + " не может выполнить " + move + ": боец не является командиром.");
        } else {
            trooper.setRequestEnemyDisposition(true);
            trooper.setActionPoints(trooper.getActionPoints() - 10);
            effects.add(new Effect(trooper.getX(), trooper.getY(), EffectType.ACTION_POINTS_CHANGE, -10));
        }

        return effects;
    }

    private static int getMoveCost(TrooperAdapter trooper) {
        switch (trooper.getStance()) {
            case PRONE:
                return 6;
            case KNEELING:
                return 4;
            case STANDING:
                return 2;
            default:
                throw new IllegalArgumentException("Unsupported trooper stance: " + trooper.getStance() + '.');
        }
    }

    private void loadMap() {
        LOGGER.debug("Started to load map.");
        String mapFile = this.options.get("map");
        CellMap map = MapHelper.loadCellMap(this.startedLocations, mapFile);
        CellType[][] cells = map.getCells();
        if (cells.length > 0) {
            this.width = cells.length;
            this.height = cells[0].length;
            this.map.init(cells);
            VisibilityMap visibilityMap = MapHelper.loadVisibilityMap(map);
            this.cellVisibilities = visibilityMap.getCellVisibilities();
            LOGGER.debug("Finished to load map.");
        } else {
            throw new ResourceException("Can\'t load map: cell array is empty.");
        }
    }

    private void addRenderers() {
        LOGGER.debug("Started to add renderers.");
        if (Boolean.parseBoolean(this.options.get("render-to-screen"))) {
            LOGGER.debug("Adding " + ScreenRenderer.class.getSimpleName() + '.');
            this.renderers.add(new ScreenRenderer(this.width, this.height, this.options));
        }

        String textFile = this.options.get("write-to-text-file");
        if (!StringUtils.isBlank(textFile)) {
            try {
                LOGGER.debug("Adding " + FileRenderer.class.getSimpleName() + '.');
                this.renderers.add(new FileRenderer(new File(textFile)));
            } catch (IOException e) {
                LOGGER.error(String.format("Can\'t create renderer \'%s\'.", FileRenderer.class.getSimpleName()), e);
                this.error(String.format("Can\'t create renderer \'%s\': %s", FileRenderer.class.getSimpleName(), ExceptionUtils.getStackTrace(e)));
            }
        }

        String remoteStorage = this.options.get("write-to-remote-storage");
        if (!StringUtils.isBlank(remoteStorage)) {
            try {
                LOGGER.debug("Adding " + RemoteStorageRenderer.class.getSimpleName() + '.');
                this.renderers.add(new RemoteStorageRenderer(remoteStorage));
            } catch (IOException e) {
                LOGGER.error(String.format("Can\'t create renderer \'%s\'.", RemoteStorageRenderer.class.getSimpleName()), e);
                this.error(String.format("Can\'t create renderer \'%s\': %s", RemoteStorageRenderer.class.getSimpleName(), ExceptionUtils.getStackTrace(e)));
            }
        }

        LOGGER.debug("Finished to add renderers.");
    }

    private void addPlayers(List<String> players) {
        LOGGER.debug("Started to add players.");
        int count = players.size();
        if (count != 2 && count != 4) {
            throw new IllegalArgumentException("Unexpected player count: " + count + '.');
        } else {
            if (this.threadPool != null) {
                this.threadPool.shutdown();
            }

            this.threadPool = Executors.newFixedThreadPool(count, new ThreadFactory() {
                private final AtomicInteger count = new AtomicInteger();

                @Override
                public Thread newThread(Runnable r) {
                    Thread thread = new Thread(r);
                    thread.setDaemon(true);
                    thread.setName(GameEngine.class.getSimpleName() + "#StrategyThread-" + count.incrementAndGet());
                    return thread;
                }
            });

            for (int i = 0; i < count; ++i) {
                String playerString = players.get(i);
                String playerName = this.options.get("p" + (i + 1) + "-name");
                if (StringUtils.isBlank(playerName)) playerName = "Player #" + (i + 1);

                int teamSize = OptionsHelper.parseTeamSize(this.options, i);
                PlayerAdapter player = PlayerHelper.initPlayer(this.options, i, playerName, playerString, teamSize, this.renderers);
                this.players.add(player);
                if (player.isKeyboardPlayer()) {
                    if (this.keyboardPlayers.size() >= 1)
                        throw new IllegalArgumentException(String.format("Can only add %d keyboard player(s).", 1));

                    this.keyboardPlayers.add(player);
                }

                player.setRemainingTime((long) (teamSize * (this.moveCount + 1)) * 500L + 2000L);
            }

            LOGGER.debug("Finished to add players.");
        }
    }

    private void addPlayerUnits() {
        LOGGER.debug("Adding player units.");
        TrooperType[] trooperTypes = TrooperType.values();

        int index = 0;
        for (PlayerAdapter player : this.players) {
            int teamSize = OptionsHelper.parseTeamSize(this.options, index);
            Map<TrooperType, Location> troopersLocations =
                    MapHelper.getTroopersLocations(this.startedLocations, index);

            for (int teammateIndex = 0; teammateIndex < teamSize; ++teammateIndex) {
                TrooperAdapter trooper = TrooperAdapter.create(player, teammateIndex, trooperTypes[teammateIndex]);
                player.addTrooper(trooper);
                Location location = troopersLocations.get(trooper.getType());
                this.map.addUnit(trooper, location.getFirst(), location.getSecond());
            }

            ++index;
        }
    }

    private void addTroopers() {
        ArrayList<PlayerAdapter> players = new ArrayList<>(this.players);
        Collections.shuffle(players, RND.instance());
        TrooperType[] trooperTypes = TrooperType.values();
        ArrayHelper.shuffle(trooperTypes, RND.instance());

        for (TrooperType trooperType : trooperTypes) {
            for (PlayerAdapter player : players) {
                TrooperAdapter trooper = player.getTroopers().get(trooperType);
                if (trooper != null) this.troopers.add(trooper);
            }
        }
    }

    private void addBonuses() {
        LOGGER.debug("Adding bonuses.");
        CellType[][] cells = this.map.getCells();
        HashMap<Location, BonusType> bonusPositions = new HashMap<>();
        int bonusCount = Convert.doubleToInt(0.0125D * (double) this.width * (double) this.height);

        for (int i = 0; i < bonusCount; ++i) {
            int x = RND.nextInt(this.width);
            int y = RND.nextInt(this.height);
            Location location = new Location(x, y);
            if (cells[x][y] != CellType.FREE || bonusPositions.containsKey(location) || !this.map.getUnitsAtPoint(location).isEmpty()) {
                x = RND.nextInt(this.width);
                y = RND.nextInt(this.height);
                location = new Location(x, y);
                if (cells[x][y] != CellType.FREE || bonusPositions.containsKey(location) || !this.map.getUnitsAtPoint(location).isEmpty())
                    continue;
            }

            BonusType bonusType = BonusHelper.nextBonus();
            bonusPositions.put(location, bonusType);
            bonusPositions.put(new Location(this.width - 1 - x, this.height - 1 - y), bonusType);
            bonusPositions.put(new Location(this.width - 1 - x, y), bonusType);
            bonusPositions.put(new Location(x, this.height - 1 - y), bonusType);
        }

        for (Entry<Location, BonusType> bonuses : bonusPositions.entrySet()) {
            Location location = bonuses.getKey();
            this.map.addUnit(new BonusAdapter(bonuses.getValue()), location.getFirst(), location.getSecond());
        }

    }

    private void writeGameContext() {
        LOGGER.debug("Sending game contexts.");
        final model.Game game = GameHelper.createGame(this.moveCount);

        for (final PlayerAdapter player : this.players) {
            Future<Integer> future = this.threadPool.submit(new Callable<Integer>() {
                public Integer call() {
                    players.Player controller = player.getController();
                    int protocolVersion = controller.getProtocolVersion();
                    controller.init(game);
                    return protocolVersion;
                }
            });
            FutureValue<Integer> futureProtocolVersion = new FutureValue<>();
            if (this.requestStrategy(player, future, futureProtocolVersion)) {
                Integer protocolVersion = futureProtocolVersion.get();
                if (!player.isStrategyCrashed() && !ensureProtocolVersion(protocolVersion)) {
                    LOGGER.warn(String.format("Strategy adapter \'%s\' returned unsupported protocol version %d.", player.getController().getClass().getSimpleName(), protocolVersion));
                    player.error("Процесс стратегии использует устаревшую версию протокола.");
                }
            }
        }

    }

    private void addCollisionListeners() {
        LOGGER.debug("Adding collision listeners.");
        this.map.addCollisionListener(TrooperAdapter.class, BonusAdapter.class, new Collider() {
            public void collide(Collision collision) {
                TrooperHelper.collideTrooperAndBonus(collision.getMap(), (TrooperAdapter) collision.getFrom(), (BonusAdapter) collision.getTo());
            }
        });
    }

    private void addEvents() {
        LOGGER.debug("Adding game events.");
        this.events.add(new GameEvent());
    }

    private void error(String message) {
        if (!this.fail.getAndSet(true)) this.message.set(message);
    }
}
