package gameserver;

import adapters.BonusAdapter;
import adapters.TrooperAdapter;
import adapters.UnitAdapter;
import core.Collider;
import core.Collision;
import core.MapBase;
import core.helpers.DirectionHelper;
import core.tuple.Location;
import model.CellType;
import model.Direction;

import java.util.*;

public class GameMap implements MapBase {

    private final Map<Long, UnitAdapter> units = new HashMap<>();
    private final Map<UnitAdapter, Location> unitLocations = new HashMap<>();
    private final Map<Location, List<UnitAdapter>> locations = new HashMap<>();
    private final List<CollisionListener> collisionListeners = new ArrayList<>();
    private CellType[][] cells;


    public void init(CellType[][] cells) {
        if (this.cells != null) {
            throw new IllegalStateException("Map is already initialized.");
        } else if (cells == null) {
            throw new IllegalArgumentException("Can\'t initialize map with \'null\'.");
        } else {
            this.cells = cells;
        }
    }

    public CellType[][] getCells() {
        CellType[][] copiedCells = new CellType[this.cells.length][];

        for (int x = 0; x < this.cells.length; ++x) {
            copiedCells[x] = Arrays.copyOf(this.cells[x], this.cells[x].length);
        }

        return copiedCells;
    }

    public UnitAdapter addUnit(UnitAdapter unit, int x, int y) {
        this.ensureUnitPosition(unit, x, y);
        Location location = new Location(x, y);
        List<UnitAdapter> units = this.locations.get(location);
        if (units == null) {
            units = new ArrayList<>(1);
            this.locations.put(location, units);
        } else {

            for (UnitAdapter u : units) {
                if (u.getClass() == unit.getClass()) {
                    throw new IllegalStateException(String.format("Can\'t add unit: location (%d, %d) is already occupied.", x, y));
                }
            }
        }

        this.units.put(unit.getId(), unit);
        this.unitLocations.put(unit, location);
        units.add(unit);
        unit.setMap(this);
        return unit;
    }

    public void removeUnit(UnitAdapter unit) {
        if (this.units.remove(unit.getId()) != null) {
            Location location = this.unitLocations.remove(unit);
            List<UnitAdapter> units = this.locations.get(location);
            units.remove(unit);
            if (units.isEmpty()) {
                this.locations.remove(location);
            }
        }

    }

    public boolean moveTo(UnitAdapter unit, int x, int y) {
        this.ensureUnit(unit);
        Location location = this.unitLocations.get(unit);
        Direction direction = DirectionHelper.directionFromTo(location.getFirst(), location.getSecond(), x, y);
        return direction != null && this.moveTo(unit, direction);
    }

    public boolean moveTo(UnitAdapter unit, Direction direction) {
        this.ensureUnit(unit);
        Location from = this.unitLocations.get(unit);
        if (direction == Direction.CURRENT_POINT) return false;
        int x = from.getFirst() + direction.getOffsetX();
        int y = from.getSecond() + direction.getOffsetY();

        if (x < 0 || x >= this.cells.length
         || y < 0 || y >= this.cells[0].length
         || this.cells[x][y] != CellType.FREE) return false;
        Location to = new Location(x, y);
        List<UnitAdapter> units = this.locations.get(to);
        if (units == null) {
            units = new ArrayList<>(1);
            this.locations.put(to, units);
        } else {
            for (UnitAdapter u : units) {
                if (u.getClass() == unit.getClass()) return false;
            }
        }

        ArrayList<UnitAdapter> bonuses = new ArrayList<>(units);
        this.unitLocations.put(unit, to);
        units.add(unit);
        this.locations.get(from).remove(unit);

        for (UnitAdapter bonus : bonuses)
            this.collide(new Collision(this, unit, bonus));

        return true;
    }

    public List<UnitAdapter> getUnits() {
        return Collections.unmodifiableList(new ArrayList<>(this.units.values()));
    }

    public List<UnitAdapter> getUnitsAtPoint(Location location) {
        List<UnitAdapter> units = this.locations.get(location);
        return units == null
                ? Collections.<UnitAdapter>emptyList()
                : Collections.unmodifiableList(new ArrayList<>(units));
    }

    public Location getPosition(UnitAdapter unit) {
        return this.unitLocations.get(unit);
    }

    public void addCollisionListener(Class<? extends TrooperAdapter> fromClass, Class<? extends BonusAdapter> toClass, Collider collider) {
        this.collisionListeners.add(new CollisionListener(fromClass, toClass, collider));
    }

    private void collide(Collision collision) {

        for (CollisionListener listener : this.collisionListeners) {
            UnitAdapter first = collision.getFrom();
            UnitAdapter second = collision.getTo();
            if (listener.getFromClass().isInstance(first) && listener.getToClass().isInstance(second)) {
                listener.getCollider().collide(new Collision(this, first, second));
            } else if (listener.getFromClass().isInstance(second) && listener.getToClass().isInstance(first)) {
                listener.getCollider().collide(new Collision(this, second, first));
            }
        }

    }

    private void ensureUnitPosition(UnitAdapter unit, int x, int y) {
        if (unit.getClass() != TrooperAdapter.class && unit.getClass() != BonusAdapter.class) {
            throw new IllegalArgumentException("Unsupported unit class: " + unit.getClass() + '.');
        } else if (this.units.get(unit.getId()) != null) {
            throw new IllegalStateException("Unit {id=" + unit.getId() + "} is already added.");
        } else if (this.cells == null) {
            throw new IllegalStateException("Can\'t add unit: map is not initialized.");
        } else if (this.cells[x][y] != CellType.FREE) {
            throw new IllegalStateException("Can\'t place unit on cell with type " + this.cells[x][y] + '.');
        }
    }

    private void ensureUnit(UnitAdapter unit) {
        if (unit.getClass() != TrooperAdapter.class) {
            throw new IllegalArgumentException("Unsupported unit class: " + unit.getClass() + '.');
        } else if (this.units.get(unit.getId()) == null) {
            throw new IllegalStateException("Unit {id=" + unit.getId() + "} is not added.");
        } else if (this.cells == null) {
            throw new IllegalStateException("Can\'t move unit: map is not initialized.");
        }
    }

    static final class CollisionListener {

        private final Class<?> fromClass;
        private final Class<?> toClass;
        private final Collider collider;


        private CollisionListener(Class fromClass, Class toClass, Collider collider) {
            this.fromClass = fromClass;
            this.toClass = toClass;
            this.collider = collider;
        }

        private Class<?> getFromClass() {
            return this.fromClass;
        }

        private Class<?> getToClass() {
            return this.toClass;
        }

        private Collider getCollider() {
            return this.collider;
        }

    }

}
