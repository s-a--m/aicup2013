package gameserver;

import adapters.BonusAdapter;
import adapters.TrooperAdapter;
import adapters.UnitAdapter;
import core.Event;
import core.MapBase;
import core.helpers.TrooperHelper;

public class GameEvent implements Event {

    public void handle(MapBase map, int moveIndex) {

        for (UnitAdapter unit : map.getUnits()) {
            if (unit instanceof TrooperAdapter) {
                TrooperAdapter trooper = (TrooperAdapter) unit;
                if (TrooperHelper.isAlive(trooper)) {
                    for (UnitAdapter target : map.getUnitsAtPoint(map.getPosition(unit))) {
                        if (target instanceof BonusAdapter)
                            TrooperHelper.collideTrooperAndBonus(map, trooper, (BonusAdapter) target);
                    }
                }
            }
        }

    }
}
