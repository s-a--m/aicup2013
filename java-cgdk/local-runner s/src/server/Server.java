package server;

import model.Game;
import model.Move;
import model.PlayerContext;

public interface Server {

    void listen(int port);

    void accept();

    String readToken();

    void writeTeamSize(int teamSize);

    int readProtocolVersion();

    void writeGameContext(Game game);

    void writePlayerContext(PlayerContext playerContext, boolean first);

    Move readMove();

    void endGame();

    void close();
}
