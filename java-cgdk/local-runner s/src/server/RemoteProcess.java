package server;

import core.helpers.ArchiveHelper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;

public class RemoteProcess {

    private static final Logger LOGGER = LoggerFactory.getLogger(RemoteProcess.class);
    private final String path;
    private final Process process;
    private final File workDir;
    private final AtomicBoolean closed = new AtomicBoolean();


    private RemoteProcess(String path, Process process, File workDir) {
        this.path = path;
        this.process = process;
        this.workDir = workDir;
    }

    public File getWorkDir() {
        return this.workDir;
    }

    public void join(long timeout) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    process.waitFor();
                } catch (InterruptedException ignored) {
                }

            }
        });
        thread.start();

        try {
            thread.join(timeout);
        } catch (InterruptedException e) {
            thread.interrupt();
        }

    }

    public void close() {
        if (this.closed.compareAndSet(false, true)) {
            this.process.destroy();

            try {
                this.process.waitFor();
                LOGGER.info("Process finished with exit code \'" + this.process.exitValue() + "\'.");
            } catch (InterruptedException ignored) {
            }

        }
    }

    protected void finalize() throws Throwable {
        if (!this.closed.get()) {
            LOGGER.error(String.format("Process \'%s\' in directory \'%s\' has not been destroyed.", this.path, this.workDir.getAbsolutePath()));
        }

        this.close();
        super.finalize();
    }

    public static RemoteProcess create(String playerDefinition, Map<String, String> environment, String... args) throws IOException {
        File file = new File(playerDefinition);
        File tempDir = mkTempDir(file.getParentFile());
        String extension = FilenameUtils.getExtension(file.getName());
        RemoteProcessProperties properties;
        if ("zip".equalsIgnoreCase(extension)) {
            ArchiveHelper.extract(file, tempDir);
            properties = RemoteProcessProperties.getPropertiesFor(file.getName().substring(0, file.getName().length() - ".zip".length()));
        } else {
            File file1 = new File(tempDir, file.getName());
            FileUtils.copyFile(file, file1);
            properties = RemoteProcessProperties.getPropertiesFor(playerDefinition);
        }

        copyResources(properties, tempDir, environment);
        String commandLine = properties.expand(FilenameUtils.getName(playerDefinition), environment);
        ArrayList<String> command = new ArrayList<>(Arrays.asList(parseCommandLine(tempDir, commandLine)));
        Collections.addAll(command, args);
        if (!(new File(command.get(0))).isAbsolute()) {
            command.set(0, (new File(tempDir, command.get(0))).getAbsolutePath());
        }

        Process process = (new ProcessBuilder(command)).directory(tempDir).start();
        write(process, process.getInputStream(), new File(tempDir, "runexe.output"));
        write(process, process.getErrorStream(), new File(tempDir, "runexe.error"));
        logCommands(command, tempDir);
        return new RemoteProcess(commandLine, process, tempDir);
    }

    private static void write(final Process process, final InputStream input, final File file) {
        (new Thread(new Runnable() {

            public void run() {
                try {
                    FileUtils.copyInputStreamToFile(input, file);
                } catch (IOException ignored) {
                }

                LOGGER.debug("Completed to write stream from " + process + " to " + file + '.');
            }
        })).start();
    }

    private static void logCommands(List<String> commands, File workDir) {
        StringBuilder sb = new StringBuilder();

        for (String command : commands) {
            if (sb.length() > 0) {
                sb.append(' ');
            }
            sb.append('\"').append(command).append('\"');
        }

        LOGGER.info("Running \'" + sb + "\' in the \'" + workDir + "\'.");
    }

    private static void copyResources(RemoteProcessProperties properties, File dir, Map<String, String> environment) throws IOException {

        for (String resources : properties.getResources()) {
            String resourceName = FilenameUtils.getName(resources);
            BufferedOutputStream outputStream = null;
            InputStream inputStream = null;

            try {
                File file = new File(dir, resourceName);
                inputStream = RemoteProcess.class.getResourceAsStream(resources);
                outputStream = new BufferedOutputStream(new FileOutputStream(file));
                if (environment != null && properties.shouldFilter(resources)) {
                    String string = new String(IOUtils.toByteArray(inputStream), "UTF-8");

                    for (Entry<String, String> variable : environment.entrySet()) {
                        string = string.replace("${" + variable.getKey() + '}', variable.getValue());
                    }

                    IOUtils.write(string.getBytes("UTF-8"), outputStream);
                } else {
                    IOUtils.copy(inputStream, outputStream);
                }
            } finally {
                IOUtils.closeQuietly(inputStream);
                IOUtils.closeQuietly(outputStream);
            }
        }

    }

    private static File mkTempDir(File base) throws IOException {
        File temp = new File(base, RandomStringUtils.randomAlphanumeric(10));
        if (!temp.mkdirs()) {
            throw new IOException("Can\'t create temporary directory \'" + temp + "\'.");
        } else {
            return temp;
        }
    }

    private static String[] parseCommandLine(File workDir, String commandLine) {
        if ((new File(workDir, commandLine)).exists()) {
            return new String[]{commandLine};
        } else {
            commandLine = commandLine + " ";
            boolean var2 = false;
            boolean var3 = false;
            StringBuilder sb = new StringBuilder();
            ArrayList<String> commands = new ArrayList<>();

            for (int i = 0; i < commandLine.length(); ++i) {
                char c = commandLine.charAt(i);
                if (c == 92) {
                    var2 ^= true;
                    if (!var2) {
                        sb.append('\\');
                    }
                } else {
                    if (c == 34) {
                        if (!var2) {
                            var3 = !var3;
                        } else {
                            sb.append('\"');
                        }
                    } else {
                        if (var2) {
                            sb.append('\\');
                        }

                        if (c <= 32 && !var3) {
                            if (sb.length() > 0) {
                                commands.add(sb.toString());
                                sb.setLength(0);
                            }
                        } else {
                            sb.append(c);
                        }
                    }

                    var2 = false;
                }
            }

            return commands.toArray(new String[commands.size()]);
        }
    }
}
