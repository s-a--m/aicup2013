package server;

import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RemoteProcessProperties {

    private static final Pattern VARIABLE = Pattern.compile("(%[a-zA-Z_1-90]+%)");
    private static final Pattern SEMICOLON = Pattern.compile(";");
    private static final ConcurrentMap<String, RemoteProcessProperties> postfixProperties = new ConcurrentHashMap<>();
    private final String commandLine;
    private final List<String> resources;
    private final Set<String> filters;


    public RemoteProcessProperties(String commandLine, List<String> resources, Set<String> filters) {
        this.commandLine = commandLine;
        this.resources = new ArrayList<>(resources);
        this.filters = new HashSet<>(filters);
    }

    public String expand(String path, Map<String, String> options) {
        String command = this.commandLine;
        Matcher matcher = VARIABLE.matcher(this.commandLine);
        if (matcher.find()) {
            int size = matcher.groupCount();
            for (int i = 0; i < size; ++i) {
                String group = matcher.group(i);
                String env = System.getenv(group.substring(1, group.length() - 1));
                if (env != null) command = command.replace(group, env);
            }
        }

        if (options != null) {
            for (Entry<String, String> option : options.entrySet())
                command = command.replace("${" + option.getKey() + '}', option.getValue());
        }

        return String.format(command, path);
    }

    public List<String> getResources() {
        return Collections.unmodifiableList(this.resources);
    }

    public boolean shouldFilter(String resource) {
        return this.filters.contains(resource);
    }

    public static RemoteProcessProperties getPropertiesFor(String path) {
        if (path != null) {
            if (path.endsWith(".zip")) {
                path = path.substring(0, path.length() - ".zip".length());
            }

            for (Entry<String, RemoteProcessProperties> o : postfixProperties.entrySet()) {
                if (path.endsWith('.' + o.getKey())) {
                    return o.getValue();
                }
            }
        }

        throw new IllegalArgumentException(String.format("Can\'t find run script for \'%s\'.", path));
    }

    private static void readProperties() {
        String path = "/remote-process.properties";
        Properties properties = new Properties();

        try {
            properties.load(RemoteProcessProperties.class.getResourceAsStream(path));
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Can\'t read property file \'%s\'.", path), e);
        }

        for (String postfix : SEMICOLON.split(properties.getProperty("remote-process.supported-postfixes"))) {
            String property = "remote-process.postfix-command-line." + postfix;
            String commandLine = properties.getProperty(property);
            if (StringUtils.isBlank(commandLine)) {
                throw new IllegalArgumentException(String.format("Expected property \'%s\' in \'%s\'.", property, path));
            }

            String resourcesToCopy = properties.getProperty("remote-process.resources-to-copy." + postfix);
            ArrayList<String> resources = new ArrayList<>();
            if (!StringUtils.isBlank(resourcesToCopy)) {

                for (String resource : SEMICOLON.split(resourcesToCopy)) {
                    resources.add(resource.trim());
                }
            }

            String resourcesToFilter = properties.getProperty("remote-process.resources-to-filter." + postfix);
            HashSet<String> filters = new HashSet<>();
            if (!StringUtils.isBlank(resourcesToFilter)) {
                for (String filter : SEMICOLON.split(resourcesToFilter)) {
                    filters.add(filter.trim());
                }
            }

            postfixProperties.put(postfix, new RemoteProcessProperties(commandLine.trim(), resources, filters));
        }

    }

    static {
        readProperties();
    }
}
