package players;

import model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import strategy.DummyGuy;

import java.lang.reflect.Constructor;

public class InternalPlayer implements Player {

    private static final Logger LOGGER = LoggerFactory.getLogger(InternalPlayer.class);
    private static final String player_package = DummyGuy.class.getPackage().getName();
    private final Strategy[] strategies;
    private Game game;


    public InternalPlayer(String className, int teamSize) {
        if (!className.endsWith(".class")) {
            throw new IllegalArgumentException(String.format("Illegal player definition: \'%s\'.", new Object[]{className}));
        } else {
            className = className.substring(0, className.length() - ".class".length());
            if(className.contains("MyStrategy")){
                className = className;
            }else if (className.indexOf(46) == -1) {
                className = player_package + '.' + className;
            }

            Constructor<? extends Strategy> constructor;
            try {
                Class<? extends Strategy> clazz = Class.forName(className).asSubclass(Strategy.class);
                constructor = clazz.getConstructor(new Class[0]);
            } catch (ClassNotFoundException e) {
                String message = String.format("Class \'%s\' does not exist.", className);
                LOGGER.error(message, e);
                throw new IllegalArgumentException(message, e);
            } catch (NoSuchMethodException e1) {
                String message = String.format("Class \'%s\' hasn\'t default constructor.", className);
                LOGGER.error(message, e1);
                throw new IllegalArgumentException(message, e1);
            }

            this.strategies = new Strategy[teamSize];

            for (int i = 0; i < teamSize; ++i) {
                Strategy strategy;
                try {
                    Strategy o = constructor.newInstance();
                    if (o != null) {
                        strategy = o;
                    } else {
                        LOGGER.error(String.format("Instance of class \'%s\' is not a strategy.", className));
                        strategy = new DummyGuy();
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Can\'t create instance of class \'%s\'.", className), e);
                    strategy = new DummyGuy();
                }

                this.strategies[i] = strategy;
            }

        }
    }

    public int getProtocolVersion() {
        return 2;
    }

    public void init(Game game) {
        this.game = game;
    }

    public Move move(Trooper trooper, World world) {
        Move move = new Move();
        this.strategies[trooper.getTeammateIndex()].move(trooper, world, this.game, move);
        return move;
    }

    public void close() {
    }

}
