package players;

import model.Move;

public interface Movable {

    Move move();
}
