package players;

import model.Game;
import model.Move;
import model.Trooper;
import model.World;

import java.io.Closeable;

public interface Player extends Closeable {

    int getProtocolVersion();

    void init(Game game);

    Move move(Trooper trooper, World world);
}
