package players;

import model.Game;
import model.Move;
import model.Trooper;
import model.World;

public class FallbackPlayer implements Player {

    public int getProtocolVersion() {
        return 2;
    }

    public void init(Game game) {
    }

    public Move move(Trooper trooper, World world) {
        return new Move();
    }

    public void close() {
    }
}
