package players;

import model.Game;
import model.Move;
import model.Trooper;
import model.World;

public class KeyboardPlayer implements Player {

    private final Movable movable;


    public KeyboardPlayer(Movable movable) {
        this.movable = movable;
    }

    public int getProtocolVersion() {
        return 2;
    }

    public void init(Game game) {
    }

    public Move move(Trooper trooper, World world) {
        return this.movable.move();
    }

    public void close() {
    }
}
