import core.Runner;
import model.Strategy;
import strategy.SmartGuy;

import java.io.*;

public final class LocalTestRunner {

    public static void main(String[] args) {
        boolean renderToScreen =false;
        boolean sync =false;
        int teamSize = 3;
        Class<? extends Strategy> smart = SmartGuy.class;
        Class<? extends Strategy> my= MyStrategy.class;
        Long seed = null;

        String results = args.length>0&&args[0]!=null?args[0]:"myResults";
        String filename = args.length>1&&args[1]!=null?args[1]:"myDebug";

        Runner runner = new Runner(
                "-move-count=50",
                "-render-to-screen=" + renderToScreen,
                "-render-to-screen-scale=1.0",
                "-render-to-screen-sync=" + sync,
                "-results-file=" + results,
                "-debug=true",
                "-base-adapter-port=32001",
                "-seed=" + (seed != null ? seed.toString() : ""),
                "-p1-name=1" ,
                "-p2-name=2",
                "-p3-name=3",
                "-p4-name=4",
                "-p1-team-size=" + teamSize,
                "-p2-team-size=" + teamSize,
                "-p3-team-size=" + teamSize,
                "-p4-team-size=" + teamSize,
                my.getSimpleName()+".class",
                smart.getSimpleName()+".class",
                smart.getSimpleName()+".class",
                smart.getSimpleName()+".class"
        );

        readFile(new File(filename));

        runner.run();
    }

    private static void readFile(File file) {
 //       System.out.println(file.getAbsolutePath());
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
            }
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String s = reader.readLine();
            String[] ss = null;
            if (null != s)
                ss = s.split(",");
            if (null != ss && ss.length > 1) {
                double[] dd = new double[ss.length];
                for (int i = 0; i < ss.length; i++) {
                    dd[i] = Double.parseDouble(ss[i]);
                }
                readArrays(dd);
            } else {
                writeArrays(file);
            }

        } catch (IOException ignore) {
            System.out.println(ignore.getMessage());
        }
    }

    private static void writeArrays(File file) {
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
            }
        } else {
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {

                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        try (FileWriter writer = new FileWriter(file)) {

            writeArray(writer, MyStrategy.W_DANGER);
            writeArray(writer, MyStrategy.W_ATTACK);
            writeArray(writer, MyStrategy.W_HEAL);
            writeArray(writer, MyStrategy.W_HP);
            writeArray(writer, MyStrategy.W_MEDIC);
            writeArray(writer, MyStrategy.W_MOVE);
            writeArray(writer, MyStrategy.W_MOVE_ENEMY);
            writeArray(writer, MyStrategy.W_MOVE_TO);
            writeArray(writer, MyStrategy.W_TACTIC);
            writeArray(writer, MyStrategy.W_MOVE_FIND);

            writer.flush();
        } catch (Exception ignore) {

            System.out.println(ignore.getMessage());
        }
    }

    private static void writeArray(FileWriter writer, double[] arr) throws IOException {
        for (double d : arr) {
            writer.write(String.valueOf(d) + ",");
        }
    }

    private static void readArrays(double[] dd) {
        int offset = 0;

        offset = readArray(dd, offset, MyStrategy.W_DANGER);
        offset = readArray(dd, offset, MyStrategy.W_ATTACK);
        offset = readArray(dd, offset, MyStrategy.W_HEAL);
        offset = readArray(dd, offset, MyStrategy.W_HP);
        offset = readArray(dd, offset, MyStrategy.W_MEDIC);
        offset = readArray(dd, offset, MyStrategy.W_MOVE);
        offset = readArray(dd, offset, MyStrategy.W_MOVE_ENEMY);
        offset = readArray(dd, offset, MyStrategy.W_MOVE_TO);
        offset = readArray(dd, offset, MyStrategy.W_TACTIC);
        offset = readArray(dd, offset, MyStrategy.W_MOVE_FIND);
    }

    private static int readArray(double[] dd, int i, double[] arr) {
        System.arraycopy(dd, i, arr, 0, arr.length);
        return i + arr.length;
    }
}
