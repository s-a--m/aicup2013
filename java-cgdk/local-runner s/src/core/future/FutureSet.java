package core.future;


public interface FutureSet<T> {

    T set(T value);
}
