package core.future;

public abstract class AbstractFutureValue<T> implements FutureGet<T>, FutureSet<T> {

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (this.getClass() != o.getClass()) return false;

        AbstractFutureValue<?> futureValue = (AbstractFutureValue<?>) o;
        if (this.get() == null) {
            if (futureValue.get() != null)
                return false;
        } else if (!this.get().equals(futureValue.get()))
            return false;

        return true;
    }

    public int hashCode() {
        return this.get() == null ? 0 : this.get().hashCode();
    }

    public String toString() {
        return String.valueOf(this.get());
    }
}
