package core.future;


public interface FutureGet<T> {

    T get();
}
