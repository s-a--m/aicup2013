package core.future;

public class FutureValue<T> extends AbstractFutureValue<T> {

    private T value;

    public T get() {
        return this.value;
    }

    public T set(T value) {
        return this.value = value;
    }
}
