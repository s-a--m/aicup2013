package core;


public enum EffectType {

    HITPOINTS_CHANGE,
    ACTION_POINTS_CHANGE
}
