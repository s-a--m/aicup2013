package core;

public final class Effect {

    private final int x;
    private final int y;
    private final EffectType type;
    private final int amount;


    public Effect(int x, int y, EffectType type, int amount) {
        this.x = x;
        this.y = y;
        this.type = type;
        this.amount = amount;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public EffectType getType() {
        return this.type;
    }

    public int getAmount() {
        return this.amount;
    }
}
