package core;

import adapters.BonusAdapter;
import adapters.TrooperAdapter;
import adapters.UnitAdapter;
import core.tuple.Location;
import model.CellType;
import model.Direction;

import java.util.List;

public interface MapBase {

    void init(CellType[][] cells);

    CellType[][] getCells();

    UnitAdapter addUnit(UnitAdapter unit, int x, int y);

    void removeUnit(UnitAdapter unit);

    boolean moveTo(UnitAdapter unit, int x, int y);

    boolean moveTo(UnitAdapter unit, Direction direction);

    List<UnitAdapter> getUnits();

    List<UnitAdapter> getUnitsAtPoint(Location location);

    Location getPosition(UnitAdapter unit);

    void addCollisionListener(Class<? extends TrooperAdapter> fromClass, Class<? extends BonusAdapter> toClass, Collider collider);
}
