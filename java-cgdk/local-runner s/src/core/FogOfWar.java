package core;


public final class FogOfWar {

    private final long playerId;
    private final String fogOfWar;


    public FogOfWar(long playerId, String fogOfWar) {
        this.playerId = playerId;
        this.fogOfWar = fogOfWar;
    }

    public long getPlayerId() {
        return this.playerId;
    }
}
