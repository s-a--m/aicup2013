package core;

import adapters.UnitAdapter;

public final class Collision {

    private final MapBase map;
    private final UnitAdapter from;
    private final UnitAdapter to;


    public Collision(MapBase map, UnitAdapter from, UnitAdapter to) {
        this.map = map;
        this.from = from;
        this.to = to;
    }

    public MapBase getMap() {
        return this.map;
    }

    public UnitAdapter getFrom() {
        return this.from;
    }

    public UnitAdapter getTo() {
        return this.to;
    }
}
