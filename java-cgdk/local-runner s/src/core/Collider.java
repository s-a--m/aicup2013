package core;

public interface Collider {

    void collide(Collision collision);
}
