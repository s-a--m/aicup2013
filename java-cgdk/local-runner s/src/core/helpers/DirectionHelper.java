package core.helpers;

import model.Direction;

public class DirectionHelper {

    public static Direction directionFromTo(int fromX, int fromY, int toX, int toY) {
        return toX == fromX && toY == fromY - 1
                ? Direction.NORTH
                : (toX == fromX + 1 && toY == fromY
                ? Direction.EAST
                : (toX == fromX && toY == fromY + 1
                ? Direction.SOUTH
                : (toX == fromX - 1 && toY == fromY
                ? Direction.WEST
                : (toX == fromX && toY == fromY
                ? Direction.CURRENT_POINT : null))));
    }
}
