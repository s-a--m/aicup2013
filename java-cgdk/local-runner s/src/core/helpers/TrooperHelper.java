package core.helpers;

import adapters.BonusAdapter;
import adapters.PlayerAdapter;
import adapters.TrooperAdapter;
import com.google.common.primitives.Chars;
import com.google.common.primitives.Ints;
import core.MapBase;
import model.Trooper;
import model.TrooperStance;
import model.TrooperType;

import java.util.List;

public class TrooperHelper {

    public static TrooperType fromChar(char c) {
        c = Character.toUpperCase(c);
        char last = Chars.checkedCast((long) ('A' + TrooperType.values().length - 1));
        return c >= 'A' && c <= last ? TrooperType.values()[c - 'A'] : null;
    }

    public static boolean isAlive(TrooperAdapter trooper) {
        return trooper.getHitpoints() > 0;
    }

    public static int hit(TrooperAdapter target, TrooperAdapter shooter, MapBase map, List<PlayerAdapter> players) {
        int damage;

        switch (shooter.getStance()) {
            case PRONE:
                damage = shooter.getProneDamage();
                break;
            case KNEELING:
                damage = shooter.getKneelingDamage();
                break;
            case STANDING:
                damage = shooter.getStandingDamage();
                break;
            default:
                throw new IllegalArgumentException("Unsupported damager \'stance\': " + shooter.getStance() + '.');
        }

        return hit(target, shooter.getPlayer(), map, players, damage);
    }

    public static int hit(TrooperAdapter target, PlayerAdapter shooter, MapBase map, List<PlayerAdapter> players, int damage) {
        if (!isAlive(target)) {
            return 0;
        } else {
            boolean teammate = shooter.getId() == target.getPlayer().getId();
            damage = StrictMath.min(target.getHitpoints(), damage);
            int score = Ints.checkedCast(Double.valueOf((double) damage * 1.0D).longValue());
            target.setHitpoints(target.getHitpoints() - damage);
            if (!PlayerHelper.oneAlive(players)) {
                if (!PlayerHelper.isAlive(target.getPlayer())) {
                    score += 50;
                } else if (!isAlive(target)) {
                    score += 25;
                }
            }

            if (!isAlive(target)) {
                target.getPlayer().setCasualties(target.getPlayer().getCasualities() + 1);
                if (!teammate) {
                    shooter.setFrags(shooter.getFrags() + 1);
                }

                map.removeUnit(target);
            }

            if (!teammate) {
                shooter.increaseScore(score);
            }

            return damage;
        }
    }

    public static void collideTrooperAndBonus(MapBase map, TrooperAdapter trooper, BonusAdapter bonus) {
        switch (bonus.getType()) {
            case GRENADE:
                if (!trooper.isHoldingGrenade()) {
                    trooper.setHoldingGrenade(true);
                    map.removeUnit(bonus);
                }
                break;
            case MEDIKIT:
                if (!trooper.isHoldingMedikit()) {
                    trooper.setHoldingMedikit(true);
                    map.removeUnit(bonus);
                }
                break;
            case FIELD_RATION:
                if (!trooper.isHoldingFieldRation()) {
                    trooper.setHoldingFieldRation(true);
                    map.removeUnit(bonus);
                }
                break;
            default:
                throw new IllegalArgumentException("Unsupported bonus type: " + bonus.getType() + '.');
        }
    }

    public static TrooperStance getStance(TrooperAdapter trooper) {
        return trooper.getStance();
    }

    public static double getStealthBonus(TrooperAdapter trooper) {
        switch (trooper.getType()) {
            case SNIPER:
                switch (trooper.getStance()) {
                    case PRONE:
                        return 1;
                    case KNEELING:
                        return 0.5;
                    case STANDING:
                        return 0;
                    default:
                        throw new IllegalArgumentException("Unsupported trooper stance: " + trooper.getStance() + '.');
                }
            case FIELD_MEDIC:
            case SOLDIER:
            case COMMANDER:
            case SCOUT:
                return 0;
            default:
                throw new IllegalArgumentException("Unsupported trooper type: " + trooper.getType() + '.');
        }
    }

    public static double getStealthBonusNegation(TrooperAdapter trooper) {
        switch (trooper.getType()) {
            case SCOUT:
                return 1.0D;
            case COMMANDER:
            case FIELD_MEDIC:
            case SOLDIER:
            case SNIPER:
                return 0;
            default:
                throw new IllegalArgumentException("Unsupported trooper type: " + trooper.getType() + '.');
        }
    }

    public static Trooper createTrooper(TrooperAdapter trooper, List<PlayerAdapter> players) {
        boolean teammate = true;
        if (players == null) teammate = false;
        else {
            for (PlayerAdapter player : players) {
                if (!trooper.getPlayer().equals(player)) {
                    teammate = false;
                    break;
                }
            }
        }

        return new Trooper(
                trooper.getId(),
                trooper.getX(), trooper.getY(),
                trooper.getPlayer().getId(), trooper.getTeammateIndex(), teammate,
                trooper.getType(),
                trooper.getStance(),
                trooper.getHitpoints(), trooper.getMaximalHitpoints(),
                trooper.getActionPoints(), trooper.getInitialActionPoints(),
                trooper.getVisionRange(), trooper.getShootingRange(),
                trooper.getShootCost(),
                trooper.getStandingDamage(), trooper.getKneelingDamage(), trooper.getProneDamage(), trooper.getDamage(),
                trooper.isHoldingGrenade(), trooper.isHoldingMedikit(), trooper.isHoldingFieldRation());
    }
}
