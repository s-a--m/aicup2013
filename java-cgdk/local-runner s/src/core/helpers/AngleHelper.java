package core.helpers;


public final class AngleHelper {

    public static double getAngle(double fromX, double fromY, double x, double y) {
        return StrictMath.atan2(y - fromY, x - fromX);
    }
}
