package core.helpers;

import java.util.Random;

public class ArrayHelper {

    public static void shuffle(Object[] objects, Random random) {
        if (objects != null && objects.length > 1) {
            for (int i = objects.length - 1; i > 0; --i) {
                int r = random.nextInt(i + 1);
                Object temp = objects[i];
                objects[i] = objects[r];
                objects[r] = temp;
            }
        }

    }
}
