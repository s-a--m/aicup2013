package core.helpers;

import adapters.PlayerAdapter;
import adapters.TrooperAdapter;
import model.Player;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import players.FallbackPlayer;
import players.InternalPlayer;
import players.KeyboardPlayer;
import renderers.Renderer;
import renderers.ScreenRenderer;
import server.RemoteProcessProperties;

import java.util.List;
import java.util.Map;

public final class PlayerHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerHelper.class);


    private PlayerHelper() {
        throw new UnsupportedOperationException();
    }

    public static PlayerAdapter initPlayer(Map<String, String> optiotns, int playerID, String playerName, String playerDefinition, int teamSize, List<Renderer> renderers) {
        ensure(playerName, playerDefinition);

        try {
            if (playerDefinition.endsWith(".class")) {
                return new PlayerAdapter(playerName, new InternalPlayer(playerDefinition, teamSize));
            } else {
                if ("#KeyboardPlayer".equals(playerDefinition)) {

                    for (Renderer renderer : renderers) {
                        if (renderer instanceof ScreenRenderer) {
                            return new PlayerAdapter(playerName, new KeyboardPlayer(((ScreenRenderer) renderer).getMovable()));
                        }
                    }
                } else {

                }

                String message = String.format("Unsupported player definition: \'%s\'.", playerDefinition);
                LOGGER.error(message);
                throw new IllegalArgumentException(message);
            }
        } catch (RuntimeException e) {
            LOGGER.error(String.format("Can\'t load player defined by \'%s\'.", playerDefinition), e);
            PlayerAdapter player = new PlayerAdapter(playerName, new FallbackPlayer());
            player.error("При инициализации игрока возникло непредвиденное исключение.");
            return player;
        }
    }



    private static void ensure(String name, String playerDefinition) {
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("Argument \'name\' is \'null\' or blank.");
        } else if (StringUtils.isBlank(playerDefinition)) {
            throw new IllegalArgumentException("Argument \'playerDefinition\' is \'null\' or blank.");
        }
    }

    public static Player createPlayer(PlayerAdapter player) {
        return new Player(
                player.getId(),
                player.getPlayerName(),
                player.getScore(),
                player.isStrategyCrashed(),
                player.getApproximateX(), player.getApproximateY());
    }

    public static Player[] createPlayers(List<PlayerAdapter> players) {
        int size = players.size();
        Player[] player = new Player[size];

        for (int i = 0; i < size; ++i) {
            player[i] = createPlayer(players.get(i));
        }

        return player;
    }

    public static boolean oneAlive(List<PlayerAdapter> players) {
        int count = 0;

        for (PlayerAdapter player : players) {
            if (isAlive(player)) {
                ++count;
            }
        }

        return count <= 1;
    }

    public static boolean isAlive(PlayerAdapter player) {
        for (TrooperAdapter trooper : player.getTroopers().values()) {
            if (TrooperHelper.isAlive(trooper))
                return true;
        }
        return false;
    }

    public static boolean allDead(List<PlayerAdapter> players) {
        for (PlayerAdapter player : players) {
            if (isAlive(player))
                return false;
        }

        return true;
    }

    public static boolean allCrashed(List<PlayerAdapter> players) {
        for (PlayerAdapter player : players) {
            if (!player.isStrategyCrashed())
                return false;
        }
        return true;
    }

}
