package core.helpers;

import model.*;

import java.util.Arrays;

public class World extends model.World {

    private final int moveIndex;
    private final int width;
    private final int height;
    private final Player[] players;
    private final Trooper[] troopers;
    private final Bonus[] bonuses;
    private final CellType[][] cells;
    private final boolean[][][][][] cellVisibilities;


    public World(int moveIndex, int width, int height, Player[] players, Trooper[] troopers, Bonus[] bonuses, CellType[][] cellls, boolean[][][][][] cellVisibilities) {
        this.moveIndex = moveIndex;
        this.width = width;
        this.height = height;
        this.players = Arrays.copyOf(players, players.length);
        this.troopers = Arrays.copyOf(troopers, troopers.length);
        this.bonuses = Arrays.copyOf(bonuses, bonuses.length);
        if (cellls == null) {
            this.cells = null;
        } else {
            this.cells = new CellType[cellls.length][];

            for (int column = 0; column < cellls.length; ++column) {
                this.cells[column] = Arrays.copyOf(cellls[column], cellls[column].length);
            }
        }

        this.cellVisibilities = cellVisibilities;
    }

    public int getMoveIndex() {
        return this.moveIndex;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public Player[] getPlayers() {
        return Arrays.copyOf(this.players, this.players.length);
    }

    public Trooper[] getTroopers() {
        return Arrays.copyOf(this.troopers, this.troopers.length);
    }

    public Bonus[] getBonuses() {
        return Arrays.copyOf(this.bonuses, this.bonuses.length);
    }

    public CellType[][] getCells() {
        if (this.cells == null) {
            return null;
        } else {
            CellType[][] cells = new CellType[this.cells.length][];

            for (int i = 0; i < this.cells.length; ++i) {
                cells[i] = Arrays.copyOf(this.cells[i], this.cells[i].length);
            }

            return cells;
        }
    }

    public boolean[][][][][] getCellVisibilities1() {
        return this.cellVisibilities;
    }

    public boolean isVisible(
            double maxRange,
            int viewerX, int viewerY, TrooperStance viewerStance,
            int objectX, int objectY, TrooperStance objectStance) {
        int minStanceIndex = StrictMath.min(viewerStance.ordinal(), objectStance.ordinal());
        return StrictMath.hypot((double) (objectX - viewerX), (double) (objectY - viewerY)) <= maxRange
                && this.cellVisibilities[viewerX][viewerY][objectX][objectY][minStanceIndex];
    }
}
