package core.helpers.maps;


public final class VisibilityMap {

    private final String mapName;
    private final String mapHashCode;
    private final boolean[][][][][] cellVisibilities;

    public VisibilityMap(String mapName, String mapHashCode, boolean[][][][][] cellVisibilities) {
        this.mapName = mapName;
        this.mapHashCode = mapHashCode;
        this.cellVisibilities = cellVisibilities;
    }


    public String getMapName() {
        return this.mapName;
    }

    public String getMapHashCode() {
        return this.mapHashCode;
    }

    public boolean[][][][][] getCellVisibilities() {
        return this.cellVisibilities;
    }
}
