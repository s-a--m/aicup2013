package core.helpers.maps;

import model.CellType;

public final class CellMap {

    private final String name;
    private final String hash;
    private final CellType[][] cells;


    public CellMap(String name, String hash, CellType[][] cells) {
        this.name = name;
        this.hash = hash;
        this.cells = cells;
    }

    public String getName() {
        return this.name;
    }

    public String getHash() {
        return this.hash;
    }

    public CellType[][] getCells() {
        return this.cells;
    }
}
