package core.helpers;

import adapters.*;
import com.google.gson.GsonBuilder;
import core.Effect;
import core.FogOfWar;
import model.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class WorldHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(WorldHelper.class);
    private static final ConcurrentMap<Long, Pattern> unitPatterns = new ConcurrentHashMap<>();
    private static final ConcurrentMap<Long, Pattern> playerNamePatterns = new ConcurrentHashMap<>();
    private static final ConcurrentMap<Long, Pattern> playerFogOfWarPattern = new ConcurrentHashMap<>();


    private WorldHelper() {
        throw new UnsupportedOperationException();
    }

    public static core.helpers.World createWorld(
            int moveIndex,
            int width, int height,
            List<PlayerAdapter> players,
            List<UnitAdapter> units,
            CellType[][] cells,
            boolean[][][][][] cellVisibilities,
            List<PlayerAdapter> keyboardPlayers,
            boolean requestEnemyDisposition) {
        ArrayList<Trooper> troopers = new ArrayList<>();
        ArrayList<Bonus> bonuses = new ArrayList<>();
        HashMap<Long, List<Trooper>> troopersMap = requestEnemyDisposition
                ? new HashMap<Long, List<Trooper>>() : null;

        for (UnitAdapter unit : units) {
            Trooper trooper = null;
            if (unit.getClass() == TrooperAdapter.class) {
                trooper = TrooperHelper.createTrooper((TrooperAdapter) unit, keyboardPlayers);
            }

            if (requestEnemyDisposition && trooper != null) {
                List<Trooper> troopers1 = troopersMap.get(Long.valueOf(trooper.getPlayerId()));
                if (troopers1 == null) {
                    troopers1 = new ArrayList<>();
                    troopersMap.put(trooper.getPlayerId(), troopers1);
                }

                troopers1.add(trooper);
            }

            if (isVisibleByPlayers(cellVisibilities, keyboardPlayers, unit)) {
                if (unit.getClass() == TrooperAdapter.class) troopers.add(trooper);
                else {
                    if (unit.getClass() == BonusAdapter.class)
                        bonuses.add(BonusHelper.createBonus((BonusAdapter) unit));
                    else throw new IllegalArgumentException("Unsupported unit class: " + unit.getClass() + '.');
                }
            }
        }

        Collections.shuffle(troopers, RND.instance());
        Collections.shuffle(bonuses, RND.instance());
        ArrayList<PlayerAdapter> randomPlayers = new ArrayList<>(players);
        Collections.shuffle(randomPlayers, RND.instance());
        if (requestEnemyDisposition) {
            for (PlayerAdapter player : randomPlayers) {
                List<Trooper> playerTroopers = troopersMap.get(Long.valueOf(player.getId()));
                int size = playerTroopers == null ? 0 : playerTroopers.size();
                if (size == 0) {
                    player.setApproximateX(-1);
                    player.setApproximateY(-1);
                } else {
                    long approximateX = 0L;
                    long approximateY = 0L;

                    for (Trooper playerTrooper : playerTroopers) {
                        approximateX += (long) playerTrooper.getX();
                        approximateY += (long) playerTrooper.getY();
                    }

                    approximateX /= (long) size;
                    approximateY /= (long) size;
                    int errorFromX = StrictMath.max(0, Convert.intToLong(approximateX) - 5);
                    int errorToX = StrictMath.min(width - 1, Convert.intToLong(approximateX) + 5);
                    int errorFromY = StrictMath.max(0, Convert.intToLong(approximateY) - 5);
                    int errorToY = StrictMath.min(height - 1, Convert.intToLong(approximateY) + 5);
                    player.setApproximateX(RND.nextRange(errorFromX, errorToX));
                    player.setApproximateY(RND.nextRange(errorFromY, errorToY));
                }
            }
        } else {
            for (PlayerAdapter player : randomPlayers) {
                player.setApproximateX(-1);
                player.setApproximateY(-1);
            }
        }

        return new core.helpers.World(moveIndex,
                width, height,
                PlayerHelper.createPlayers(randomPlayers),
                troopers.toArray(new Trooper[troopers.size()]),
                bonuses.toArray(new Bonus[bonuses.size()]),
                cells,
                cellVisibilities);
    }

    private static boolean[] toOneDimensionArray(boolean[][][][][] c,int width,
                                                 int height,int stanceCount) {
       //todo



        boolean[] result = new boolean[width* height* width* height* stanceCount];
       for(int i = 0;i<c.length;i++){
           for(int j = 0;j<c[0].length;j++){
               for(int k = 0;k<c[0][0].length;k++){
                   for(int h = 0;h<c[0][0][0].length;h++){
                       for(int n = 0;n<c[0][0][0][0].length;n++){
                           result[i+j+k+h+n] = c[i][j][k][h][n];
                       }
                   }
               }
           }
       }
        return result;
    }

    public static WorldAdapter createAdapter(
            World world,
            String mapName,
            Trooper trooper,
            Move move,
            Effect[] effects,
            FogOfWar[] fogOfWars) {
        return new WorldAdapter(world, mapName, trooper, move, effects, fogOfWars);
    }

    public static String serializeWorld(World world, WorldAdapter worldAdapter) {
        try {
            if (world instanceof WorldAdapter && worldAdapter != null) {
                WorldAdapter adapter = (WorldAdapter) world;
                if (world.getMoveIndex() == 0) {
                    world = new WorldAdapter(
                            new World(world.getMoveIndex(),
                                    world.getWidth(), world.getHeight(),
                                    world.getPlayers(), world.getTroopers(), world.getBonuses(), null, null),
                            null,
                            adapter.getTrooper(),
                            adapter.getMove(),
                            adapter.getEffects(),
                            adapter.getFogOfWars());
                }
            }

            String str = (new GsonBuilder()).setVersion((double) world.getMoveIndex()).create().toJson(world);
            if (worldAdapter != null) {
                StringBuilder sb = new StringBuilder(str);
                serializeUnits(world, worldAdapter, sb);
                serializePlayers(world, worldAdapter, sb);
                if (world instanceof WorldAdapter) {
                    serializeFogOfWar((WorldAdapter) world, worldAdapter, sb);
                }

                str = sb.toString();
            }

            return str;
        } catch (RuntimeException e) {
            String result = null;

            try {
                result = (new GsonBuilder()).setVersion((double) world.getMoveIndex()).serializeSpecialFloatingPointValues().serializeNulls().create().toJson(world);
            } catch (RuntimeException ignored) {
            }

            LOGGER.error(String.format("Can\'t serialize world with default settings. Result of serialization with safe settings: %s.", result));
            throw e;
        }
    }

    private static void serializeUnits(World world, WorldAdapter worldAdapter, StringBuilder stringBuilder) {
        HashMap<Long, Unit> unitIdMap = new HashMap<>();
        Trooper[] troopers = worldAdapter.getTroopers();

        for (Trooper trooper : troopers) {
            unitIdMap.put(trooper.getId(), trooper);
        }

        Bonus[] bonuses = worldAdapter.getBonuses();

        for (Bonus bonuse : bonuses) {
            unitIdMap.put(bonuse.getId(), bonuse);
        }

        ArrayList<Unit> units = new ArrayList<>();
        units.addAll(Arrays.asList(world.getTroopers()));
        units.addAll(Arrays.asList(world.getBonuses()));

        for (Unit unit : units) {
            if (EqualsBuilder.reflectionEquals(unit, unitIdMap.get(Long.valueOf(unit.getId())))) {
                Pattern unitPattern = getUnitPattern(unit.getId());

                while (true) {
                    Matcher matcher = unitPattern.matcher(stringBuilder);
                    if (!matcher.find()) break;

                    stringBuilder.replace(
                            stringBuilder.lastIndexOf("{", matcher.start()) + 1,
                            stringBuilder.indexOf("}", matcher.end()),
                            "\"id\":" + unit.getId());
                }
            }
        }

    }

    private static void serializePlayers(World world, WorldAdapter worldAdapter, StringBuilder sb) {
        HashMap<Long, Player> playerIdMap = new HashMap<>();

        for (Player player : worldAdapter.getPlayers()) {
            playerIdMap.put(player.getId(), player);
        }

        for (Player player : world.getPlayers()) {
            if (EqualsBuilder.reflectionEquals(player, playerIdMap.get(Long.valueOf(player.getId())))) {
                Pattern pattern = getPlayerNamePattern(player.getId());

                while (true) {
                    Matcher matcher = pattern.matcher(sb);
                    if (!matcher.find()) {
                        break;
                    }

                    sb.replace(sb.lastIndexOf("{", matcher.start()) + 1, sb.indexOf("}", matcher.end()), "\"id\":" + player.getId());
                }
            }
        }

    }

    private static void serializeFogOfWar(WorldAdapter world, WorldAdapter worldAdapter, StringBuilder sb) {
        HashMap<Long, FogOfWar> fogOfWarMap = new HashMap<>();


        for (FogOfWar fogOfWar : worldAdapter.getFogOfWars()) {
            fogOfWarMap.put(fogOfWar.getPlayerId(), fogOfWar);
        }

        for (FogOfWar fogOfWar : world.getFogOfWars()) {
            FogOfWar fogOfWar1 = fogOfWarMap.get(Long.valueOf(fogOfWar.getPlayerId()));
            if (EqualsBuilder.reflectionEquals(fogOfWar, fogOfWar1)) {
                Pattern pattern = getFogOfWarPattern(fogOfWar.getPlayerId());

                while (true) {
                    Matcher matcher = pattern.matcher(sb);
                    if (!matcher.find()) {
                        break;
                    }

                    sb.replace(sb.lastIndexOf("{", matcher.start()) + 1, sb.indexOf("}", matcher.end()), "\"id\":" + fogOfWar.getPlayerId());
                }
            }
        }

    }

    private static Pattern getUnitPattern(long id) {
        Pattern pattern = unitPatterns.get(id);
        if (pattern == null) {
            pattern = Pattern.compile("\"id\"\\s*:\\s*" + id + "\\s*,\\s*\"x\"\\s*:", Pattern.MULTILINE);
            unitPatterns.putIfAbsent(id, pattern);
        }

        return pattern;
    }

    private static Pattern getPlayerNamePattern(long id) {
        Pattern pattern = playerNamePatterns.get(id);
        if (pattern == null) {
            pattern = Pattern.compile("\"id\"\\s*:\\s*" + id + "\\s*,\\s*\"name\"\\s*:", Pattern.MULTILINE);
            playerNamePatterns.putIfAbsent(id, pattern);
        }

        return pattern;
    }

    private static Pattern getFogOfWarPattern(long id) {
        Pattern pattern = playerFogOfWarPattern.get(id);
        if (pattern == null) {
            pattern = Pattern.compile("\"id\"\\s*:\\s*" + id + "\\s*,\\s*\"fogOfWar\"\\s*:", Pattern.MULTILINE);
            playerFogOfWarPattern.putIfAbsent(id, pattern);
        }

        return pattern;
    }

    public static boolean isVisibleByPlayers(boolean[][][][][] cellVisibilities, List<PlayerAdapter> players, UnitAdapter unit) {
        if (players == null) {
            return true;
        } else {
            if (unit.getClass() == TrooperAdapter.class) {
                for (PlayerAdapter player : players) {
                    if (((TrooperAdapter) unit).getPlayer().equals(player)) {
                        return true;
                    }
                }
            }

            int x = unit.getX();
            int y = unit.getY();
            TrooperStance stance = UnitHelper.getStance(unit);

            for (PlayerAdapter player : players) {
                for (TrooperAdapter trooper : player.getTroopers().values()) {
                    double visionRange = trooper.getVisionRange();
                    double stealthBonus = UnitHelper.getStealthBonus(unit);
                    double stealthBonusNegation = TrooperHelper.getStealthBonusNegation(trooper);
                    visionRange -= StrictMath.max(stealthBonus - stealthBonusNegation, 0.0D);
                    if (TrooperHelper.isAlive(trooper) && isVisible(
                            cellVisibilities,
                            visionRange,
                            trooper.getX(), trooper.getY(), TrooperHelper.getStance(trooper),
                            x, y, stance)) {
                        return true;
                    }
                }
            }

            return false;
        }
    }

    public static boolean isVisible(
            boolean[][][][][] cellVisibilities,
            double viewerRange,
            int viewerX, int viewerY, TrooperStance viewerStance,
            int tartgetX, int targetY, TrooperStance targetStance) {
        int rangeX = tartgetX - viewerX;
        int rangeY = targetY - viewerY;
        return (double) (rangeX * rangeX + rangeY * rangeY) <= viewerRange * viewerRange
                && cellVisibilities[viewerX][viewerY]
                [tartgetX][targetY]
                [StrictMath.min(viewerStance.ordinal(), targetStance.ordinal())];
    }

}
