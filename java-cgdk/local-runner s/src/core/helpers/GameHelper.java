package core.helpers;

import model.Game;

public class GameHelper {

    public static Game createGame(int moveCount) {
        return new Game(moveCount, 100, 50, 25, 1.0D, 2, 2, 4, 6, 2, 5.0D, 10, 5, 1, 5, 3, 0.0D, 0.5D, 1.0D, 0.0D, 1.0D, 2.0D, 1.0D, 8, 5.0D, 80, 60, 2, 50, 30, 2, 5);
    }
}
