package core.helpers;

import java.util.Random;

public class RND {

    private static final Random RANDOM = new Random(randomSeed());


    public static long randomSeed() {
        return System.nanoTime() ^ Thread.currentThread().getId() + Runtime.getRuntime().maxMemory() * Runtime.getRuntime().freeMemory() & Runtime.getRuntime().totalMemory();
    }

    public static Random instance() {
        return RANDOM;
    }

    public static void setSeed(long seed) {
        RANDOM.setSeed(seed);
    }

    public static int nextInt(int v) {
        return RANDOM.nextInt(v);
    }

    public static int nextRange(int from, int to) {
        return from + RANDOM.nextInt(to - from + 1);
    }

    public static boolean nextBoolean() {
        return RANDOM.nextBoolean();
    }

}
