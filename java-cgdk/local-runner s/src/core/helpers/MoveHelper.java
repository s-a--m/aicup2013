package core.helpers;

import model.Move;

public class MoveHelper {

    public static Move copy(Move move) {
        if (move == null) {
            return null;
        } else {
            Move copy = new Move();
            copy.setAction(move.getAction());
            copy.setDirection(move.getDirection());
            copy.setX(move.getX());
            copy.setY(move.getY());
            copy.setStrategyCrashed(move.isStrategyCrashed());
            return copy;
        }
    }
}
