package core.helpers;

import adapters.BonusAdapter;
import model.Bonus;
import model.BonusType;

public final class BonusHelper {

    public static Bonus createBonus(BonusAdapter bonus) {
        return new Bonus(bonus.getId(), bonus.getX(), bonus.getY(), bonus.getType());
    }

    public static BonusType nextBonus() {
        int i = RND.nextInt(3);
        if (i < 1) {
            return BonusType.GRENADE;
        } else {
            --i;
            if (i < 1) {
                return BonusType.MEDIKIT;
            } else {
                --i;
                return BonusType.FIELD_RATION;
            }
        }
    }
}
