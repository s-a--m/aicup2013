package core.helpers;

import adapters.BonusAdapter;
import adapters.TrooperAdapter;
import adapters.UnitAdapter;
import model.TrooperStance;

public class UnitHelper {

    public static TrooperStance getStance(UnitAdapter unit) {
        if (unit.getClass() == TrooperAdapter.class) {
            return TrooperHelper.getStance((TrooperAdapter) unit);
        } else if (unit.getClass() == BonusAdapter.class) {
            return TrooperStance.PRONE;
        } else {
            throw new IllegalArgumentException("Unsupported unit class: " + unit.getClass() + '.');
        }
    }

    public static double getStealthBonus(UnitAdapter unit) {
        if (unit.getClass() == TrooperAdapter.class) {
            return TrooperHelper.getStealthBonus((TrooperAdapter) unit);
        } else if (unit.getClass() == BonusAdapter.class) {
            return 0.0D;
        } else {
            throw new IllegalArgumentException("Unsupported unit class: " + unit.getClass() + '.');
        }
    }
}
