package core.helpers;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;
import org.apache.commons.io.IOUtils;
import repeater.RepeatedOperation;
import repeater.Repeater;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Iterator;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class ArchiveHelper {

    private static final int BUFFER_LENGTH = 0x100000;


    public static byte[] compress(byte[] buffer, int level) {
        if (buffer.length == 0) {
            return buffer;
        } else {
            Deflater deflater = new Deflater();
            deflater.setLevel(level);
            deflater.setInput(buffer);
            deflater.finish();
            ByteArrayOutputStream stream = new ByteArrayOutputStream(buffer.length);
            byte[] compressed = new byte[BUFFER_LENGTH];

            while (!deflater.finished()) {
                stream.write(compressed, 0, deflater.deflate(compressed));
            }

            IOUtils.closeQuietly(stream);
            return stream.toByteArray();
        }
    }

    public static byte[] decompress(byte[] buffer) throws DataFormatException {
        if (buffer.length == 0) {
            return buffer;
        } else {
            Inflater inflater = new Inflater();
            inflater.setInput(buffer);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            byte[] decompressed = new byte[BUFFER_LENGTH];

            while (!inflater.finished()) {
                stream.write(decompressed, 0, inflater.inflate(decompressed));
            }

            inflater.end();
            return stream.toByteArray();
        }
    }

    public static void extract(File file, File dest) throws IOException {
        extract(file, dest, null);
    }

    public static void extract(File file, File dest, FileFilter filter) throws IOException {
        try {
            createDirectory(dest);
            ZipFile zipFile = new ZipFile(file);
            int count = 0;
            Iterator iterator = zipFile.getFileHeaders().iterator();

            while (true) {
                if (iterator.hasNext()) {
                    Object zipHeader = iterator.next();
                    if ((long) count < 50000L) {
                        FileHeader entry = (FileHeader) zipHeader;
                        File destFile = new File(dest, entry.getFileName());
                        if (filter != null && filter.accept(destFile)) {
                            continue;
                        }

                        if (entry.isDirectory()) {
                            createDirectory(destFile);
                        } else {
                            if (entry.getUncompressedSize() > 536870912L || entry.getCompressedSize() > 536870912L) {
                                long size = StrictMath.max(entry.getUncompressedSize(), entry.getCompressedSize());
                                throw new IOException("Entry \'" + entry.getFileName() + "\' is larger than " + size + " B.");
                            }

                            createDirectory(destFile.getParentFile());
                            zipFile.extractFile(entry, dest.getAbsolutePath());
                        }

                        ++count;
                        continue;
                    }
                }

                return;
            }
        } catch (ZipException e) {
            throw new IOException("Can\'t extract ZIP-file to directory.", e);
        }
    }

    public static File createDirectory(final File file) throws IOException {
        return Repeater.run(new RepeatedOperation<File>() {
            public File run() throws IOException {
                if (!file.isDirectory() && !file.mkdirs() && !file.isDirectory()) {
                    throw new IOException("Can\'t create directory \'" + file + "\'.");
                } else {
                    return file;
                }
            }
        });
    }
}
