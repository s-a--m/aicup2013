package core.helpers;

import com.google.common.primitives.Chars;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import core.ResourceException;
import core.helpers.maps.CellMap;
import core.helpers.maps.VisibilityMap;
import core.tuple.Location;
import model.CellType;
import model.TrooperStance;
import model.TrooperType;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.mutable.MutableInt;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.zip.DataFormatException;

public final class MapHelper {

    private static final Pattern TWO_DIGIT_PATTERN = Pattern.compile("[1-9][0-9]?");


    public static CellMap loadCellMap(Map<Integer, Map<TrooperType, Location>> startingLocations, String mapName) {
        byte[] buffer = readMap((mapName = FilenameUtils.getBaseName(mapName)) + ".map");
        String[] lines = StringHelper.END_LINE.split(StringUtils.newStringUtf8(buffer));
        MutableInt i = new MutableInt(0);
        CellType[][] cells = parseMapSize(lines, i);
        if (cells.length > 0) {
            parseCells(startingLocations, cells, lines, i, cells.length, cells[0].length);
        }

        return new CellMap(mapName, DigestUtils.sha512Hex(buffer), cells);
    }

    public static VisibilityMap loadVisibilityMap(CellMap map) {
        byte[] buffer = readMap(FilenameUtils.getBaseName(map.getName()) + ".vmap");

        try {
            buffer = ArchiveHelper.decompress(buffer);
        } catch (DataFormatException e) {
            throw new ResourceException("Can\'t decompress visibility map for map " + map.getName() + '.', e);
        }

        VisibilityMap visibilityMap;
        try {
            visibilityMap = (new GsonBuilder()).create().fromJson(StringUtils.newStringUtf8(buffer), VisibilityMap.class);
        } catch (JsonSyntaxException e) {
            throw new ResourceException("Can\'t parse visibility map for map \'" + map.getName() + "\'.", e);
        }

        if (!enusreVisibilityMap(map, visibilityMap)) {
            throw new ResourceException("Map visibilities are incorrect for map \'" + map.getName() + "\'.");
        } else {
            return visibilityMap;
        }
    }

    private static boolean enusreVisibilityMap(CellMap cellMap, VisibilityMap visibilityMap) {
        if (!StringHelper.equal(cellMap.getName(), visibilityMap.getMapName())) {
            return false;
        } else if (!StringHelper.equal(cellMap.getHash(), visibilityMap.getMapHashCode())) {
            return false;
        } else {
            CellType[][] cells = cellMap.getCells();
            boolean[][][][][] cellVisibilities = visibilityMap.getCellVisibilities();
            return cellVisibilities != null && cellVisibilities.length == cells.length && cellVisibilities[0].length == cells[0].length && cellVisibilities[0][0].length == cells.length && cellVisibilities[0][0][0].length == cells[0].length && cellVisibilities[0][0][0][0].length == TrooperStance.values().length;
        }
    }

    private static byte[] readMap(String fileName) {
        byte[] buffer = readResource(WorldHelper.class, "/maps/" + fileName);
        if (buffer == null) {
            try {
                File file = new File("maps/"+fileName);

                if (!file.isFile()) {
                    throw new ResourceException("Map file \'" + fileName + "\' is not found in current directory.");
                }

                if (file.length() > 8388608L) {
                    throw new ResourceException(String.format("Size of the map file \'%s\' is greater than %d B.", fileName, 8388608L));
                }

                buffer = FileUtils.readFileToByteArray(file);
            } catch (IOException e) {
                throw new ResourceException("Can\'t read map file \'" + fileName + "\' from current directory.", e);
            }
        }

        return buffer;
    }

    private static CellType[][] parseMapSize(String[] source, MutableInt i) {
        int size = source.length;

        CellType[][] cells;
        for (cells = null; i.intValue() < size && cells == null; i.increment()) {
            String symbol = source[i.intValue()];
            if (!StringHelper.isEmptyLine(symbol) && symbol.indexOf('#') != 0) {
                String[] row = StringHelper.STRING.split(symbol.trim());
                if (row.length < 2) {
                    throw new ResourceException("Can\'t read width and height of the map.");
                }

                String width = row[0];
                if (!TWO_DIGIT_PATTERN.matcher(width).matches()) {
                    throw new IllegalArgumentException(String.format("Map width \'%s\' does not match pattern \'%s\'.", new Object[]{width, TWO_DIGIT_PATTERN.pattern()}));
                }

                int w = Integer.parseInt(width);
                String height = row[1];
                if (!TWO_DIGIT_PATTERN.matcher(height).matches()) {
                    throw new IllegalArgumentException(String.format("Map height \'%s\' does not match pattern \'%s\'.", new Object[]{height, TWO_DIGIT_PATTERN.pattern()}));
                }

                int h = Integer.parseInt(height);
                cells = new CellType[w][h];
            }
        }

        return cells;
    }

    private static void parseCells(
            Map<Integer, Map<TrooperType, Location>> startingLocations,
            CellType[][] cells,
            String[] lines,
            MutableInt i,
            int width, int height) {
        if (startingLocations == null) startingLocations = new HashMap<>();

        int size = lines.length;
        int w = width / 2 + width % 2;
        int h = height / 2 + height % 2;
        int y = 0;

        for (; i.intValue() < size && y < h; i.increment()) {
            String line = lines[i.intValue()];
            if (!StringHelper.isEmptyLine(line) && line.indexOf(35) != 0) {
                line = StringHelper.STRING.matcher(line).replaceAll("");
                if (line.length() != w) {
                    throw new IllegalArgumentException("Length of the map line is not " + w + '.');
                }

                for (int x = 0; x < w; ++x) {
                    char c = line.charAt(x);
                    CellType cell = parseCell(c);
                    if (cell == null) {
                        throw new IllegalArgumentException("Found unexpected character \'" + c + "\'.");
                    }

                    Location[] locationses = new Location[]{
                            new Location(x, y),
                            new Location(width - 1 - x, height - 1 - y),
                            new Location(width - 1 - x, y),
                            new Location(x, height - 1 - y)};
                    int positionCount = locationses.length;

                    for (Location location : locationses) {
                        cells[location.getFirst()][location.getSecond()] = cell;
                    }

                    TrooperType trooperType = TrooperHelper.fromChar(c);
                    if (trooperType != null) {
                        ensureStartLocation(x, y, width, height);

                        for (int p = 0; p < positionCount; ++p) {
                            setTrooperStartLocation(startingLocations, p, trooperType, locationses[p]);
                        }
                    }
                }

                ++y;
            }
        }

        if (getTroopersLocations(startingLocations, 0).size() != TrooperType.values().length) {
            throw new IllegalArgumentException("Unexpected number of troopers\' starting locations.");
        }
    }

    private static void ensureStartLocation(int x, int y, int width, int height) {
        if (width % 2 != 0 && x == width / 2 + 1 || height % 2 != 0 && y == height / 2 + 1) {
            throw new IllegalArgumentException("Starting location of the trooper can\'t lay on the central line.");
        }
    }

    private static void setTrooperStartLocation(
            Map<Integer, Map<TrooperType, Location>> startinLocation,
            int playerIndex, TrooperType trooperType, Location location) {
        Map<TrooperType, Location> troopersLocations = getTroopersLocations(startinLocation, playerIndex);
        if (troopersLocations.put(trooperType, location) != null) {
            throw new IllegalArgumentException(String.format("Starting location for trooper %s is already registered.",
                    trooperType));
        }
    }

    public static Map<TrooperType, Location> getTroopersLocations(
            Map<Integer, Map<TrooperType, Location>> startingLocations,
            int playerIndex) {
        Map<TrooperType, Location> troopersLocations = startingLocations.get(playerIndex);
        if (troopersLocations == null) {
            troopersLocations = new EnumMap<>(TrooperType.class);
            startingLocations.put(playerIndex, troopersLocations);
        }

        return troopersLocations;
    }

    public static byte[] readResource(Class<WorldHelper> clazz, String resource) {
        try {
            InputStream stream = clazz.getResourceAsStream(resource);
            if (stream == null) {
                throw new ResourceException("Can\'t find resource \'" + resource + "\' for " + clazz + '.');
            } else {
                try {
                    return IOUtils.toByteArray(stream);
                } catch (IOException e) {
                    throw new ResourceException("Can\'t read resource \'" + resource + "\' for " + clazz + '.', e);
                }
            }
        } catch (ResourceException e) {
            return null;
        }
    }

    public static CellType parseCell(char c) {
        c = Character.toUpperCase(c);
        switch (c) {
            case '0':
                return CellType.FREE;
            case '1':
                return CellType.LOW_COVER;
            case '2':
                return CellType.MEDIUM_COVER;
            case '3':
                return CellType.HIGH_COVER;
            default:
                char to = Chars.checkedCast((long) ('A' + TrooperType.values().length - 1));
                return c >= 'A' && c <= to ? CellType.FREE : null;
        }
    }
}
