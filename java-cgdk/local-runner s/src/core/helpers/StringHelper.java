package core.helpers;

import core.tuple.Tuple;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Pattern;

public final class StringHelper {

    public static final Pattern END_LINE = Pattern.compile("\\r\\n|\\r|\\n");
    public static final Pattern STRING = Pattern.compile("\\s+");
    public static final Pattern DOT = Pattern.compile("\\.");
    private static final ConcurrentMap<Class, Map<Object, List<Field>>> CLASS_FIELD_MAP = new ConcurrentHashMap<>();


    public static boolean isEmpty(String s) {
        return s == null || s.isEmpty();
    }

    public static boolean isEmptyLine(String line) {
        if (line != null && !line.isEmpty()) {
            for (int i = line.length() - 1; i >= 0; --i) {
                if (!Character.isWhitespace(line.charAt(i))) {
                    return false;
                }
            }

            return true;
        } else {
            return true;
        }
    }

    public static boolean equal(String left, String right) {
        return left == null ? right == null : left.equals(right);
    }

    public static String stringify(Class<? extends Tuple> clazz, Object o, boolean nullable, String... fields) {
        return o == null ? clazz.getSimpleName() + " {null}" : stringifyObject(o, nullable, fields);
    }

    public static String stringifyObject(Object o, boolean nullable, String... fields) {
        Class clazz = o.getClass();
        Map<Object, List<Field>> fieldMap = getFieldMap(clazz);
        StringBuilder sb = (new StringBuilder(clazz.getSimpleName())).append(" {");
        boolean first = true;

        for (String name : fields) {
            if (isEmptyLine(name)) {
                throw new IllegalArgumentException("Field name can not be neither \'null\' nor blank.");
            }

            Object fieldValue = getValue(o, fieldMap, name);
            String str;
            if (fieldValue == null) {
                if (nullable) continue;

                str = name + "=null";
            } else str = stringifyValue(fieldValue, name);

            if (first) first = false;
            else sb.append(", ");

            sb.append(str);
        }

        return sb.append('}').toString();
    }

    private static Object getValue(Object o, Map<Object, List<Field>> fieldMap, String field) {
        Object result = null;
        Object currentObject = o;
        Map<Object, List<Field>> currentFieldMap = fieldMap;
        String[] strings = DOT.split(field);

        for (String name : strings) {
            if (isEmptyLine(name)) {
                throw new IllegalArgumentException("Field name can not be neither \'null\' nor blank.");
            }

            List<Field> fields = currentFieldMap.get(name);
            if (fields == null || fields.isEmpty()) {
                throw new IllegalArgumentException(String.format("There is no field \'%s\' in %s.", name, currentObject.getClass()));
            }

            result = getValue(fields.get(0), currentObject);
            if (result == null) {
                return result;
            }
            currentObject = result;
            currentFieldMap = getFieldMap(result.getClass());
        }
        return result;
    }

    private static String stringifyValue(Object o, String name) {
        StringBuilder sb = new StringBuilder(name);
        if (o.getClass() != Boolean.TYPE && o.getClass() != Boolean.class) {
            sb.append('=').append(stringify(o));
        } else if (!(Boolean) o) {
            sb.insert(0, '!');
        }

        return sb.toString();
    }

    private static Object getValue(Field field, Object o) {
        try {
            return field.get(o);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException("Can\'t get value of inaccessible field \'" + field.getName() + "\'.", e);
        }
    }

    private static String stringify(Object o) {
        if (o == null) {
            return null;
        } else {
            Class clazz = o.getClass();
            if (clazz.isArray()) {
                return stringifyArray(o);
            } else if (o instanceof Collection) {
                return stringifyCollection((Collection) o);
            } else if (o instanceof Map) {
                return stringifyMap((Map) o);
            } else if (o instanceof Entry) {
                Entry entry = (Entry) o;
                return stringify(entry.getKey()) + '=' + stringify(entry.getValue());
            } else {
                return clazz == Character.class
                        ? "\'" + o + '\''
                        : (clazz != Boolean.class
                        && clazz != Byte.class
                        && clazz != Short.class
                        && clazz != Integer.class
                        && clazz != Long.class
                        && clazz != Float.class
                        && clazz != Double.class
                        ? (clazz.isEnum()
                        ? ((Enum) o).name()
                        : (clazz == String.class
                        ? '\'' + (String) o + '\''
                        : '\'' + String.valueOf(o) + '\''))
                        : o.toString());
            }
        }
    }

    private static String stringifyArray(Object arr) {
        StringBuilder sb = new StringBuilder("[");
        int size = Array.getLength(arr);

        for (int i = 0; i < size; ++i) {
            if (i > 0) sb.append(", ");
            sb.append(stringify(Array.get(arr, i)));
        }

        return sb.append(']').toString();
    }

    private static String stringifyCollection(Collection collection) {
        StringBuilder sb = new StringBuilder("[");
        boolean first = true;
        for (Object o : collection) {
            if (first) first = false;
            else sb.append(", ");
            sb.append(stringify(o));
        }
        return sb.append(']').toString();
    }

    private static String stringifyMap(Map map) {
        StringBuilder sb = new StringBuilder("{");
        boolean first = true;

        for (Object o : map.entrySet()) {
            if (first) first = false;
            else sb.append(", ");
            sb.append(stringify(o));
        }

        return sb.append('}').toString();
    }

    private static Map<Object, List<Field>> getFieldMap(Class clazz) {
        Map<Object, List<Field>> fieldMap = CLASS_FIELD_MAP.get(clazz);
        if (fieldMap != null) {
            return fieldMap;
        } else {
            HashMap<Object, List<Field>> map = new HashMap<>();

            for (Class<?> c = clazz; c != null; c = c.getSuperclass()) {
                Field[] fields = c.getDeclaredFields();

                for (Field field : fields) {
                    if (!field.isEnumConstant() && !Modifier.isStatic(field.getModifiers()) && !field.isSynthetic()) {
                        List<Field> fieldList = map.get(field.getName());
                        ArrayList<Field> list;
                        if (fieldList == null) {
                            list = new ArrayList<>(1);
                        } else {
                            list = new ArrayList<>(fieldList.size() + 1);
                            list.addAll(fieldList);
                        }

                        field.setAccessible(true);
                        list.add(field);
                        map.put(field.getName(), Collections.unmodifiableList(list));
                    }
                }
            }

            assert clazz != null;
            CLASS_FIELD_MAP.putIfAbsent(clazz, Collections.unmodifiableMap(map));
            return CLASS_FIELD_MAP.get(clazz);
        }
    }

    public static String fitLine(String str, int maxLength) {
        if (maxLength < 8) {
            throw new IllegalArgumentException("Argument maxLength is expected to be at least 8.");
        } else if (str != null && str.length() > maxLength) {
            int halfLength = maxLength / 2;
            int pos = maxLength - halfLength - 3;
            return str.substring(0, halfLength) + "..." + str.substring(str.length() - pos);
        } else {
            return str;
        }
    }

    public static List<String> fitLines(List<String> lines, int maxLineLength, int maxLineCount) {
        if (maxLineCount < 8) {
            throw new IllegalArgumentException("Argument maxLineCount is expected to be at least 8.");
        } else if (lines == null) {
            return null;
        } else {
            ArrayList<String> result = new ArrayList<>(maxLineCount);
            if (lines.size() <= maxLineCount) {

                for (String line : lines) {
                    result.add(fitLine(line, maxLineLength));
                }
            } else {
                int head = maxLineCount / 2;
                int tail = maxLineCount - head - 1;

                for (int i = 0; i < head; ++i) {
                    result.add(fitLine(lines.get(i), maxLineLength));
                }

                result.add("...");

                for (int i = lines.size() - tail; i < lines.size(); ++i) {
                    result.add(fitLine(lines.get(i), maxLineLength));
                }
            }

            return result;
        }
    }

}
