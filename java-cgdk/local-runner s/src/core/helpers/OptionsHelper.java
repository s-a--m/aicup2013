package core.helpers;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.AbstractMap.SimpleEntry;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

public class OptionsHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(OptionsHelper.class);
    private static final Pattern MOVE_COUNT_PATTERN = Pattern.compile("[1-9][0-9]{0,3}");
    private static final Pattern TEAM_SIZE_PATTERN = Pattern.compile("[3-5]");
    private static final Map<Integer, Integer> playerMap = new HashMap<>();


    private OptionsHelper() {
        throw new UnsupportedOperationException();
    }

    public static int getMoveCount(Map<String, String> properties) {
        LOGGER.debug("Parsing move count.");
        String moveCount = properties.get("move-count");
        if (StringUtils.isBlank(moveCount)) {
            moveCount = "50";
        }

        if (!MOVE_COUNT_PATTERN.matcher(moveCount).matches()) {
            throw new IllegalArgumentException("Illegal move count value: \'" + moveCount + "\'.");
        } else {
            return Integer.parseInt(moveCount);
        }
    }

    public static int parseTeamSize(Map<String, String> properties, int playerID) {
        Integer teamSize = playerMap.get(playerID);
        if (teamSize == null) {
            LOGGER.debug("Parsing team size for player #" + (playerID + 1) + '.');
            String str = properties.get("p" + (playerID + 1) + "-team-size");
            if (StringUtils.isBlank(str)) {
                str = "3";
            }

            if (!TEAM_SIZE_PATTERN.matcher(str).matches()) {
                throw new IllegalArgumentException("Illegal team size value: \'" + str + "\'.");
            }

            teamSize = Integer.parseInt(str);
            playerMap.put(playerID, teamSize);
        }

        return teamSize;
    }

    public static void parsePlayers(Map<String, String> options, List<String> players) {

        for (String player : players) {
            if ("#KeyboardPlayer".equals(player)) {
                options.put("debug", "true");
                options.put("keyboard-player", "true");
                options.put("render-to-screen", "true");
            } else if ("#LocalTestPlayer".equals(player)) {
                options.put("debug", "true");
                options.put("local-test", "true");
            }
        }

        if (StringUtils.isBlank(options.get("map"))) {
            options.put("map", "default.map");
        }

    }

    public static void init(String[] args, Map<String, String> options, List<String> players) {
        for (String arg : args) {
            if (arg.startsWith("-")) {
                Entry<String, String> entry = parserProperty(arg.substring("-".length()));
                options.put(entry.getKey(), entry.getValue());
            } else {
                players.add(arg);
            }
        }

    }

    private static Entry<String, String> parserProperty(String property) {
        int delim = property.indexOf('=');
        if (delim <= 0) {
            throw new IllegalArgumentException("Illegal property string: \'" + property + "\'.");
        } else {
            return new SimpleEntry<>(property.substring(0, delim), property.substring(delim + 1));
        }
    }

    public static void parseSeed(Map<String, String> options) {
        String str = options.get("seed");
        if (StringUtils.isBlank(str)) {
            long seed = RND.randomSeed();
            RND.setSeed(seed);
            options.put("seed", String.valueOf(seed));
        } else {
            RND.setSeed(Long.parseLong(str));
        }

        LOGGER.info("Starting game with seed \'" + options.get("seed") + "\'.");
    }

}
