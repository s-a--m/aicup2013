package core.helpers;

import javax.vecmath.Vector2d;

public class VectorHelper {

    public static Vector2d rotate(Vector2d vec, double angle) {
        return new Vector2d(
                vec.x * StrictMath.cos(angle) - vec.y * StrictMath.sin(angle),
                vec.x * StrictMath.sin(angle) + vec.y * StrictMath.cos(angle));
    }
}
