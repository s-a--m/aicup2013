package core.helpers;

import com.google.common.primitives.Ints;

public class Convert {

    public static byte intToByte(int v) {
        if (v <= 127 && v >= -128) {
            return (byte) v;
        } else {
            throw new IllegalArgumentException("Can\'t convert " + v + " to byte.");
        }
    }

    public static int doubleToInt(double v) {
        return intToLong(Double.valueOf(v).longValue());
    }

    public static int intToLong(long v) {
        return Ints.checkedCast(v);
    }
}
