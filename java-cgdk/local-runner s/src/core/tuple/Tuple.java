package core.tuple;

import core.helpers.StringHelper;

public class Tuple<F, S> {

    private final F first;
    private final S second;


    public Tuple(F first, S second) {
        this.first = first;
        this.second = second;
    }

    public F getFirst() {
        return this.first;
    }

    public S getSecond() {
        return this.second;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        Tuple<?, ?> tuple = (Tuple<?, ?>) obj;
        if (this.first == null) {
            if (tuple.first != null) {
                return false;
            }
        } else if (!this.first.equals(tuple.first)) {
            return false;
        }

        if (this.second == null) {
            if (tuple.second != null) {
                return false;
            }
        } else if (!this.second.equals(tuple.second)) {
            return false;
        }

        return true;
    }

    public int hashCode() {
        int seed = this.first == null ? 0 : this.first.hashCode();
        seed = 31 * seed + (this.second == null ? 0 : this.second.hashCode());
        return seed;
    }

    public String toString() {
        return toString(Tuple.class, this);
    }

    public static String toString(Class<? extends Tuple> clazz, Tuple tuple) {
        return StringHelper.stringify(clazz, tuple, false, "first", "second");
    }
}
