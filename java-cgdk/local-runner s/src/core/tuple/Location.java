package core.tuple;

public class Location extends IntegerTuple {

    public Location(Integer x, Integer y) {
        super(x, y);
    }
}
