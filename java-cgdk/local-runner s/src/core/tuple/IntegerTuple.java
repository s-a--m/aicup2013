package core.tuple;

public class IntegerTuple extends Tuple<Integer, Integer>
        implements Comparable<Tuple<Integer, Integer>> {

    IntegerTuple(Integer first,
                 Integer second) {
        super(first, second);
    }

    public String toString() {
        return toString(IntegerTuple.class, this);
    }

    @Override
    public int compareTo(Tuple<Integer, Integer> o) {
        int result;
        if (!this.getFirst().equals(o.getFirst())) {
            if (this.getFirst() == null) return -1;
            if (o.getFirst() == null) return 1;

            result = this.getFirst().compareTo(o.getFirst());
            if (result != 0) return result;
        }

        if (!this.getSecond().equals(o.getSecond())) {
            if (this.getSecond() == null) return -1;
            if (o.getSecond() == null) return 1;

            result = this.getSecond().compareTo(o.getSecond());
            if (result != 0) return result;
        }
        return 0;
    }
}
