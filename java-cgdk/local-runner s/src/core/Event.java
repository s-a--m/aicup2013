package core;

public interface Event {

    void handle(MapBase map, int moveIndex);
}
