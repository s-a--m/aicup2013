package core;

import com.google.inject.Guice;
import com.google.inject.Module;
import core.helpers.OptionsHelper;
import gameserver.GameModule;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public final class Runner implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(Runner.class);
    private final String[] args;


    public Runner(String... args) {
        this.args = args;
    }

    public void run() {
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                LOGGER.error("Got unexpected exception in thread \'" + t + "\'.", e);
                e.printStackTrace();
            }
        });
        HashMap<String, String> properties = new HashMap<>();
        ArrayList<String> players = new ArrayList<>();
        OptionsHelper.init(this.args, properties, players);

        try {
            OptionsHelper.parsePlayers(properties, players);
            OptionsHelper.parseSeed(properties);
            Engine engine = Guice.createInjector(new Module[]{new GameModule()}).getInstance(Engine.class);

            try {
                engine.init(Collections.unmodifiableMap(properties), Collections.unmodifiableList(players));
                engine.run();
            } finally {
                engine.finish();
            }
        } catch (RuntimeException e) {
            LOGGER.error("Got unexpected game exception.", e);
            e.printStackTrace();
            String resultsFile = properties.get("results-file");
            if (!StringUtils.isBlank(resultsFile)) {
                try {
                    String message = "FAILED\n" + ExceptionUtils.getStackTrace(e) + '\n';
                    FileUtils.writeByteArrayToFile(new File(resultsFile), message.getBytes("UTF-8"));
                } catch (IOException e1) {
                    LOGGER.error(String.format("Can\'t write results to file \'%s\'.", resultsFile), e1);
                }
            }
        }

    }
}
