package core;

import java.util.List;
import java.util.Map;

public interface Engine {

    void init(Map<String, String> properties, List<String> players);

    void run();

    void finish();
}
