package org.aopalliance.intercept;

public abstract interface MethodInterceptor
{
  public abstract Object invoke(MethodInvocation paramMethodInvocation)
    throws Throwable;
}

/* Location:           /home/sss/Загрузки/aicup/local-runner (3)/local-runner.jar
 * Qualified Name:     org.aopalliance.intercept.MethodInterceptor
 * JD-Core Version:    0.6.2
 */