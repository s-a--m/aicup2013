package strategy;

import core.helpers.RND;
import model.*;

public class QuickStartGuy implements Strategy {

    public void move(Trooper self, World world, Game game, Move move) {
        if (self.getActionPoints() >= self.getShootCost()) {
            Trooper[] troopers = world.getTroopers();

            for (Trooper trooper : troopers) {
                boolean visible = world.isVisible(
                        self.getShootingRange(),
                        self.getX(), self.getY(), self.getStance(),
                        trooper.getX(), trooper.getY(), trooper.getStance());
                if (visible && !trooper.isTeammate()) {
                    move.setAction(ActionType.SHOOT);
                    move.setX(trooper.getX());
                    move.setY(trooper.getY());
                    return;
                }
            }
        }

        if (self.getActionPoints() >= game.getStandingMoveCost()) {
            CellType[][] cells = world.getCells();
            int halfWidth = world.getWidth() / 2;
            int halfHeight = world.getHeight() / 2;
            int offsetX = self.getX() > halfWidth ? -1 : (self.getX() < halfWidth ? 1 : 0);
            int offsetY = self.getY() > halfHeight ? -1 : (self.getY() < halfHeight ? 1 : 0);
            boolean freeX = offsetX != 0 && cells[self.getX() + offsetX][self.getY()] == CellType.FREE;
            boolean freeY = offsetY != 0 && cells[self.getX()][self.getY() + offsetY] == CellType.FREE;
            if (freeX || freeY) {
                move.setAction(ActionType.MOVE);
                if (freeX && freeY) {
                    if (RND.nextBoolean()) {
                        move.setX(self.getX() + offsetX);
                        move.setY(self.getY());
                    } else {
                        move.setX(self.getX());
                        move.setY(self.getY() + offsetY);
                    }
                } else if (freeX) {
                    move.setX(self.getX() + offsetX);
                    move.setY(self.getY());
                } else {
                    move.setX(self.getX());
                    move.setY(self.getY() + offsetY);
                }

            }
        }

    }
}
