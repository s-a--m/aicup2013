package strategy;

import core.helpers.RND;
import model.*;
import org.apache.commons.lang.ArrayUtils;

public class SmartGuy implements Strategy {

    private boolean wasInCenter;


    public void move(Trooper self, World world, Game game, Move move) {
        CellType[][] cells = world.getCells();
        if (!tryUseMedikit(self, world, game, move)) {
            if (!tryHeal(self, world, game, move)) {
                if (!tryShoot(self, world, game, move)) {
                    if (self.getActionPoints() >= game.getStandingMoveCost()) {
                        if (!findBonus(self, world, move, cells)) {
                            this.move(self, world, move);
                        }
                    }
                }
            }
        }
    }

    private static boolean tryUseMedikit(Trooper self, World world, Game game, Move move) {
        Direction[] directions = Direction.values();
        ArrayUtils.reverse(directions);
        if (self.getActionPoints() >= game.getMedikitUseCost() && self.isHoldingMedikit()) {

            for (Direction direction : directions) {
                int hitpoints = direction == Direction.CURRENT_POINT
                        ? game.getMedikitHealSelfBonusHitpoints()
                        : game.getMedikitBonusHitpoints();

                for (Trooper trooper : world.getTroopers()) {
                    if (trooper.getHitpoints() <= trooper.getMaximalHitpoints() - hitpoints && trooper.isTeammate() && trooper.getX() == self.getX() + direction.getOffsetX() && trooper.getY() == self.getY() + direction.getOffsetY()) {
                        move.setAction(ActionType.USE_MEDIKIT);
                        move.setDirection(direction);
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private static boolean tryHeal(Trooper self, World world, Game game, Move move) {
        Direction[] directions = Direction.values();
        ArrayUtils.reverse(directions);
        if (self.getType() == TrooperType.FIELD_MEDIC && self.getActionPoints() >= game.getFieldMedicHealCost()) {

            for (Direction direction : directions) {
                int bonusHitpoints = direction == Direction.CURRENT_POINT ? game.getFieldMedicHealSelfBonusHitpoints() : game.getFieldMedicHealBonusHitpoints();

                for (Trooper trooper : world.getTroopers()) {
                    if (trooper.getHitpoints() <= trooper.getMaximalHitpoints() - bonusHitpoints && trooper.isTeammate() && trooper.getX() == self.getX() + direction.getOffsetX() && trooper.getY() == self.getY() + direction.getOffsetY()) {
                        move.setAction(ActionType.HEAL);
                        move.setDirection(direction);
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private static boolean tryShoot(Trooper self, World world, Game game, Move move) {
        if (self.getActionPoints() >= self.getShootCost()) {
            for (Trooper trooper : world.getTroopers()) {
                if (!trooper.isTeammate()) {
                    boolean visible = world.isVisible(self.getShootingRange(), self.getX(), self.getY(), self.getStance(), trooper.getX(), trooper.getY(), trooper.getStance());
                    if (self.getActionPoints() <= self.getInitialActionPoints() - game.getFieldRationBonusActionPoints() && self.getActionPoints() >= game.getFieldRationEatCost() && self.isHoldingFieldRation() && visible) {
                        move.setAction(ActionType.EAT_FIELD_RATION);
                        return true;
                    }

                    if (self.getActionPoints() >= game.getGrenadeThrowCost()
                            && self.isHoldingGrenade()
                            && StrictMath.hypot((double) (trooper.getX() - self.getX()), (double) (trooper.getY() - self.getY())) <= game.getGrenadeThrowRange()
                            && hasNeighborhood(world, trooper.getX(), trooper.getY())) {
                        move.setAction(ActionType.THROW_GRENADE);
                        move.setX(trooper.getX());
                        move.setY(trooper.getY());
                        return true;
                    }

                    if (visible) {
                        move.setAction(ActionType.SHOOT);
                        move.setX(trooper.getX());
                        move.setY(trooper.getY());
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private static boolean findBonus(Trooper self, World world, Move move, CellType[][] cells) {
        Direction direction = direction(self, world, cells);
        if (direction == null) return false;
        move.setAction(ActionType.MOVE);
        move.setDirection(direction);
        return true;
    }

    private void move(Trooper self, World world, Move move) {
        CellType[][] cells = world.getCells();
        int halfWidth = world.getWidth() / 2;
        int halfHeight = world.getHeight() / 2;
        int offsetX = self.getX() > halfWidth ? -1 : (self.getX() < halfWidth ? 1 : 0);
        int offsetY = self.getY() > halfHeight ? -1 : (self.getY() < halfHeight ? 1 : 0);
        boolean freeX = offsetX != 0 && cells[self.getX() + offsetX][self.getY()] == CellType.FREE;
        boolean freeY = offsetY != 0 && cells[self.getX()][self.getY() + offsetY] == CellType.FREE;
        move.setAction(ActionType.MOVE);
        if (!this.wasInCenter && (freeX || freeY)) {
            if (freeX && freeY) {
                if (RND.nextBoolean()) {
                    move.setX(self.getX() + offsetX);
                    move.setY(self.getY());
                } else {
                    move.setX(self.getX());
                    move.setY(self.getY() + offsetY);
                }
            } else if (freeX) {
                move.setX(self.getX() + offsetX);
                move.setY(self.getY());
            } else {
                move.setX(self.getX());
                move.setY(self.getY() + offsetY);
            }
        } else {
            this.wasInCenter = true;
            if (RND.nextBoolean()) {
                move.setDirection(RND.nextBoolean() ? Direction.NORTH : Direction.SOUTH);
            } else {
                move.setDirection(RND.nextBoolean() ? Direction.WEST : Direction.EAST);
            }
        }

    }

    private static boolean hasNeighborhood(World world, int x, int y) {
        for (Trooper trooper : world.getTroopers()) {
            if (!trooper.isTeammate() && StrictMath.abs(trooper.getX() - x) + StrictMath.abs(trooper.getY() - y) == 1) {
                return true;
            }
        }
        return false;
    }

    private static Direction direction(Trooper trooper, World world, CellType[][] cells) {
        Direction direction = null;

        for (Bonus bonus : world.getBonuses()) {
            switch (bonus.getType()) {
                case GRENADE:
                    if (trooper.isHoldingGrenade()) {
                        continue;
                    }
                    break;
                case MEDIKIT:
                    if (trooper.isHoldingMedikit()) {
                        continue;
                    }
                    break;
                case FIELD_RATION:
                    if (trooper.isHoldingFieldRation()) {
                        continue;
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported bonus type: " + bonus.getType() + '.');
            }

            if (bonus.getY() == trooper.getY()) {
                if (bonus.getX() == trooper.getX() + 1 || bonus.getX() == trooper.getX() + 2
                        && cells[trooper.getX() + 1][trooper.getY()] == CellType.FREE)
                    direction = Direction.EAST;

                if (bonus.getX() == trooper.getX() - 1 || bonus.getX() == trooper.getX() - 2
                        && cells[trooper.getX() - 1][trooper.getY()] == CellType.FREE)
                    direction = Direction.WEST;
            }

            if (bonus.getX() == trooper.getX()) {
                if (bonus.getY() == trooper.getY() + 1 || bonus.getY() == trooper.getY() + 2 && cells[trooper.getX()][trooper.getY() + 1] == CellType.FREE) {
                    direction = Direction.SOUTH;
                }

                if (bonus.getY() == trooper.getY() - 1 || bonus.getY() == trooper.getY() - 2 && cells[trooper.getX()][trooper.getY() - 1] == CellType.FREE) {
                    direction = Direction.NORTH;
                }
            }
        }

        return direction;
    }

}
