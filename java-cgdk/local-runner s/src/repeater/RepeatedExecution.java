package repeater;

import com.google.common.base.Preconditions;
import org.apache.log4j.Logger;

import java.io.IOException;

public class RepeatedExecution {

    private static final Logger LOGGER = Logger.getLogger(RepeatedExecution.class);


    private RepeatedExecution() {
        throw new UnsupportedOperationException();
    }

    public static void sleep(long delayTime) {
        try {
            Thread.sleep(delayTime);
        } catch (InterruptedException ignored) {
        }

    }

    public static <T> T run(RepeatedOperation<T> operation, int attemptCount, RepeatStrategy strategy) throws IOException {
        ensure(operation, attemptCount, strategy);
        int iteration = 1;

        while (true) {
            if (iteration <= attemptCount) {
                try {
                    return operation.run();
                } catch (Exception e) {
                    if (iteration < attemptCount) {
                        LOGGER.warn("Execution iteration #" + iteration + " has been failed: " + e.getMessage(), e);
                        sleep(strategy.getDelay(iteration));
                        ++iteration;
                        continue;
                    }

                    LOGGER.error("Execution iteration #" + iteration + " has been failed: " + e.getMessage(), e);
                    throw e;
                }
            }

            throw new RuntimeException("This line shouldn\'t be executed.");
        }
    }

    private static void ensure(RepeatedOperation<?> operation, int attemptCount, RepeatStrategy strategy) {
        Preconditions.checkNotNull(operation, "Argument \'operation\' can\'t be \'null\'.");
        Preconditions.checkArgument(attemptCount >= 1, "Argument \'attemptCount\' should be positive.");
        Preconditions.checkNotNull(strategy, "Argument \'strategy\' can\'t be \'null\'.");
    }

}
