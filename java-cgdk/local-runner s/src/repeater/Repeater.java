package repeater;

import java.io.IOException;

public class Repeater {

    public static <T> T run(RepeatedOperation<T> operation) throws IOException {
        return run(operation, 9);
    }

    public static <T> T run(RepeatedOperation<T> operation, int attemptCount) throws IOException {
        return run(operation, attemptCount, 50L, StrategyType.SQUARE);
    }

    public static <T> T run(RepeatedOperation<T> operation, int attemptCount, long delayTimeMillis, StrategyType strategyType) throws IOException {
        try {
            return RepeatedExecution.run(operation, attemptCount, new RepeatStrategy(delayTimeMillis, strategyType));
        } catch (Error error) {
            throw error;
        } catch (Throwable throwable) {
            throw new IOException(throwable);
        }
    }

}
