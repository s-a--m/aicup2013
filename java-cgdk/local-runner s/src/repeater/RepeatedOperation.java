package repeater;


import java.io.IOException;

public interface RepeatedOperation<T> {

    T run() throws IOException;
}
