package repeater;

import com.google.common.base.Preconditions;

public class RepeatStrategy {

    private final long delayTimeMillis;
    private final StrategyType type;


    public RepeatStrategy(long delayTimeMillis, StrategyType type) {
        ensure(delayTimeMillis, type);
        this.delayTimeMillis = delayTimeMillis;
        this.type = type;
    }

    private static void ensure(long delayTimeMillis, StrategyType type) {
        Preconditions.checkArgument(delayTimeMillis >= 1L, "Argument \'delayTimeMillis\' should be positive.");
        Preconditions.checkNotNull(type, "Argument \'type\' can\'t be \'null\'.");
    }

    public long getDelay(int attemptNumber) {
        if (attemptNumber < 1) {
            throw new IllegalArgumentException("Argument \'attemptNumber\' should be positive.");
        } else {
            switch (this.type) {
                case CONSTANT:
                    return this.delayTimeMillis;
                case LINEAR:
                    return this.delayTimeMillis * (long) attemptNumber;
                case SQUARE:
                    return this.delayTimeMillis * (long) attemptNumber * (long) attemptNumber;
                default:
                    throw new IllegalArgumentException("Unknown strategy type \'" + this.type + "\'.");
            }
        }
    }
}
