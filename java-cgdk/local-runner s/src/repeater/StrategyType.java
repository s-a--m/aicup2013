package repeater;


public enum StrategyType {
    CONSTANT,
    LINEAR,
    SQUARE
}
