package renderers;

import adapters.WorldAdapter;
import com.google.common.base.Strings;
import com.google.common.primitives.Ints;
import core.Effect;
import core.helpers.VectorHelper;
import core.tuple.Location;
import model.*;
import org.apache.commons.lang.StringUtils;
import players.Movable;
import repeater.RepeatedExecution;

import javax.vecmath.Vector2d;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ScreenRenderer implements Renderer {

    private static final long[] INTERVALS = new long[]{10L, 20L, 80L, 320L, 640L, 1280L, 2560L};
    private static final Color TRANSPARENT = new Color(0, 0, 0, 85);
    private final Panel panel;
    private final TextArea textLog;
    private final BlockingQueue<Frame> frames;
    private final AtomicLong intervalIndex = new AtomicLong(320L);
    private final AtomicBoolean pause = new AtomicBoolean();
    private final AtomicInteger FPS = new AtomicInteger();
    private final Lock lock = new ReentrantLock();
    private final Condition condition;
    private final MovableAdapter movable;
    private final int scaleX;
    private final int scaleY;
    private final int width;
    private final int height;


    public ScreenRenderer(int width, int height, final Map<String, String> properties) {
        this.condition = this.lock.newCondition();
        this.movable = new MovableAdapter(this);
        String scaleStr = properties.get("render-to-screen-scale");
        if (StringUtils.isBlank(scaleStr)) {
            scaleStr = "1.0";
        }

        double scale = Double.parseDouble(scaleStr);
        if (scale >= 0.5D && scale <= 2.0D) {
            if (Boolean.parseBoolean(properties.get("keyboard-player"))) {
                this.intervalIndex.set(20L);
            }

            this.scaleX = Ints.checkedCast(StrictMath.round(32 * scale));
            this.scaleY = Ints.checkedCast(StrictMath.round(32 * scale));
            this.width = width;
            this.height = height;
            this.textLog = new TextArea(30, 75);
            this.initLogFrame();
            this.panel = new Panel();
            this.setSize(width, height);
            this.frames = Boolean.parseBoolean(properties.get("render-to-screen-sync"))
                    ? new LinkedBlockingQueue<Frame>(1)
                    : new LinkedBlockingQueue<Frame>();
            (new Thread(new Runnable() {

                public void run() {
                    long last = System.currentTimeMillis();
                    long start = last;

                    try {
                        WorldAdapter worldAdapter = null;

                        Frame frame;
                        for (int i = 0; (frame = this.getFrame()) != null && frame.getWorld() != null; ++i) {
                            last = this.sleep(last, intervalIndex.get());
                            long elapsed = System.currentTimeMillis();
                            if (elapsed - start > 1000L) {
                                start = System.currentTimeMillis();
                                FPS.set(i);
                                i = 0;
                            }

                            draw(frame);
                            worldAdapter = frame.getWorld();
                        }

                        if (worldAdapter != null) {
                            drawScoreboard(worldAdapter, panel.getGraphics());
                        }

                        Thread.sleep(TimeUnit.SECONDS.toMillis(30L));
                    } catch (InterruptedException ignored) {
                    }

                    System.exit(0);
                }

                private Frame getFrame() throws InterruptedException {
                    Frame worldAdapter;
                    do {
                        worldAdapter = Boolean.parseBoolean(properties.get("debug"))
                                ? frames.poll(10L, TimeUnit.MINUTES)
                                : frames.poll(30L, TimeUnit.SECONDS);
                    }
                    while (worldAdapter != null && worldAdapter.getIndex() > 0 && intervalIndex.get() == INTERVALS[0]);

                    return worldAdapter;
                }

                private long sleep(long last, long interval) {
                    long now = System.currentTimeMillis();
                    if (now - last < interval) {
                        RepeatedExecution.sleep(interval - now + last);
                    }

                    return System.currentTimeMillis();
                }
            })).start();
        } else {
            throw new IllegalArgumentException("Illegal scale of screen renderer: " + scale + '.');
        }
    }

    private void initLogFrame() {
        this.textLog.setEditable(false);
        final java.awt.Frame logFrame = new java.awt.Frame("Text log");
        logFrame.add(this.textLog);
        logFrame.setVisible(true);
        logFrame.pack();
        logFrame.setResizable(true);
        logFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent event) {
                logFrame.setVisible(false);
            }
        });
    }

    private void setSize(int width, int height) {
        KeyAdapter listener = new KeyAdapter() {
            public void keyPressed(KeyEvent event) {
                if (!event.isConsumed()) {
                    boolean processed = true;
                    if (event.getKeyChar() == ' ') {
                        boolean p;
                        do {
                            p = pause.get();
                        } while (!pause.compareAndSet(p, !p));
                    } else {
                        int interval;
                        long index;
                        if (event.getKeyCode() == KeyEvent.VK_UP) {
                            index = intervalIndex.get();
                            interval = Arrays.binarySearch(INTERVALS, index);
                            if (interval > 0) {
                                intervalIndex.compareAndSet(index, INTERVALS[interval - 1]);
                            }
                        } else if (event.getKeyCode() == KeyEvent.VK_DOWN) {
                            index = intervalIndex.get();
                            interval = Arrays.binarySearch(INTERVALS, index);
                            if (interval < INTERVALS.length - 1) {
                                intervalIndex.compareAndSet(index, INTERVALS[interval + 1]);
                            }
                        } else if (event.getKeyCode() == KeyEvent.VK_W) {
                            movable.setMove(ActionType.MOVE, Direction.NORTH, null, null);
                        } else if (event.getKeyCode() == KeyEvent.VK_S) {
                            movable.setMove(ActionType.MOVE, Direction.SOUTH, null, null);
                        } else if (event.getKeyCode() == KeyEvent.VK_A) {
                            movable.setMove(ActionType.MOVE, Direction.WEST, null, null);
                        } else if (event.getKeyCode() == KeyEvent.VK_D) {
                            movable.setMove(ActionType.MOVE, Direction.EAST, null, null);
                        } else if (event.getKeyCode() == KeyEvent.VK_E) {
                            movable.setMove(ActionType.END_TURN, null, null, null);
                        } else if (event.getKeyCode() == KeyEvent.VK_F) {
                            movable.setMove(ActionType.EAT_FIELD_RATION, null, null, null);
                        } else if (event.getKeyCode() == KeyEvent.VK_R) {
                            movable.setMove(ActionType.RAISE_STANCE, null, null, null);
                        } else if (event.getKeyCode() == KeyEvent.VK_V) {
                            movable.setMove(ActionType.LOWER_STANCE, null, null, null);
                        } else if (event.getKeyCode() == KeyEvent.VK_Q) {
                            movable.setMove(ActionType.REQUEST_ENEMY_DISPOSITION, null, null, null);
                        } else {
                            processed = false;
                        }
                    }

                    if (processed) {
                        event.consume();
                    }

                }
            }
        };
        this.panel.setSize(this.scaleX * width, this.scaleY * height);
        this.panel.setPreferredSize(new Dimension(this.scaleX * width, this.scaleY * height));
        this.panel.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent event) {
                if (event.getClickCount() == 2) {
                    int x = event.getX() / scaleX;
                    int y = event.getY() / scaleY;
                    ActionType actionType;
                    if (event.isControlDown()) {
                        actionType = ActionType.THROW_GRENADE;
                    } else if (event.isShiftDown()) {
                        actionType = ActionType.USE_MEDIKIT;
                    } else if (event.isAltDown()) {
                        actionType = ActionType.HEAL;
                    } else {
                        actionType = ActionType.SHOOT;
                    }

                    movable.setMove(actionType, null, x, y);
                }

            }
        });
        this.panel.addKeyListener(listener);
        java.awt.Frame frame = new java.awt.Frame("CodeTroopers 2013");
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent event) {
                System.exit(0);
            }
        });
        frame.addKeyListener(listener);
        frame.add(this.panel);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        this.drawWaitMessage();
    }

    public void render(WorldAdapter worldAdapter) {
        this.drawWorld(worldAdapter);
    }

    public void close() {
        this.drawWorld(null);
    }

    private void drawWorld(WorldAdapter world) {
        while (true) {
            try {
                if (world != null && world.getTrooper() != null) {
                    for (int i = 0; i < 4; ++i) {
                        this.frames.put(new Frame(world, i));
                    }
                } else {
                    this.frames.put(new Frame(world, 0));
                }

                return;
            } catch (InterruptedException ignored) {
            }
        }
    }

    private void drawWaitMessage() {
        Graphics graphics = this.panel.getGraphics();
        graphics.setFont(new Font("Times New Roman", Font.BOLD, 30));
        FontMetrics metrics = graphics.getFontMetrics();
        String message = "Waiting for game client to connect...";
        int stringWidth = metrics.stringWidth(message);
        graphics.drawString(message, (this.panel.getWidth() - stringWidth) / 2, this.panel.getHeight() / 2 - metrics.getHeight());
        message = "Ожидание подключения стратегии...";
        stringWidth = metrics.stringWidth(message);
        graphics.drawString(message, (this.panel.getWidth() - stringWidth) / 2, this.panel.getHeight() / 2);
    }

    private void draw(Frame frame) {
        while (this.pause.get()) {
            RepeatedExecution.sleep(this.intervalIndex.get());
        }

        BufferedImage image = new BufferedImage(this.panel.getWidth(), this.panel.getHeight(), 1);
        Graphics graphics = image.getGraphics();
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, image.getWidth(), image.getHeight());
        graphics.setColor(Color.BLACK);
        this.drawCells(graphics);
        this.fillMap(frame.getWorld(), graphics);
        this.drawUnits(frame, graphics);
        this.drawEffects(frame, graphics);
        this.drawHUD(frame.getWorld(), graphics);
        this.panel.getGraphics().drawImage(image, 0, 0, null);
    }

    private void drawCells(Graphics graphics) {
        Color color = graphics.getColor();
        graphics.setColor(TRANSPARENT);
        BasicStroke stroke = new BasicStroke(0.5F, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER, 10.0F, new float[]{0.1F, 5.0F}, 0.0F);

        for (int x = 1; x < this.width; ++x) {
            drawLine(graphics, (double) (this.scaleX * x), 0.0D, (double) (this.scaleX * x), (double) this.panel.getHeight(), stroke);
        }

        for (int y = 1; y < this.height; ++y) {
            drawLine(graphics, 0.0D, (double) (this.scaleY * y), (double) this.panel.getWidth(), (double) (this.scaleY * y), stroke);
        }

        graphics.setColor(color);
    }

    private void fillMap(WorldAdapter worldAdapter, Graphics graphics) {
        Color color = graphics.getColor();
        CellType[][] cells = worldAdapter.getCells();

        for (int x = 0; x < this.width; ++x) {
            for (int y = 0; y < this.height; ++y) {
                CellType cellType = cells[x][y];
                if (cellType != CellType.FREE) {
                    Color c = Color.WHITE;
                    double fade = 1.0D - 0.1875D * (double) (cellType.ordinal());
                    c = new Color((int) ((double) c.getRed() * fade), (int) ((double) c.getGreen() * fade), (int) ((double) c.getBlue() * fade));
                    graphics.setColor(c);
                    graphics.fillRect(this.scaleX * x, this.scaleY * y, this.scaleX, this.scaleY);
                }
            }
        }

        graphics.setColor(color);
    }

    private void drawScoreboard(WorldAdapter world, Graphics graphics) {
        graphics.setFont(new Font("Courier New", Font.BOLD, 30));
        Player[] players = world.getPlayers();
        Arrays.sort(players, new Comparator<Player>() {
            @Override
            public int compare(Player left, Player right) {
                return Integer.valueOf(right.getScore()).compareTo(left.getScore());
            }
        });
        int lastIndex = 1;
        double x = (double) this.panel.getWidth() / 2.0D - 250.0D;
        double y = (double) this.panel.getHeight() / 2.0D - 150.0D;
        Color color = graphics.getColor();

        for (int i = 0; i < players.length; ++i) {
            Player player = players[i];
            int index;
            if (i != 0 && player.getScore() == players[i - 1].getScore()) {
                index = lastIndex;
            } else {
                index = i + 1;
                lastIndex = index;
            }

            graphics.setColor(getColorOfPlayer(player.getId()));
            graphics.drawString(String.format("%d. %-20s: %d", index, player.getName(), player.getScore()),
                    (int) x, (int) (y + 75.0D * (double) i));
        }

        graphics.setColor(color);
    }

    private void drawUnits(Frame frame, Graphics graphics) {

        for (Bonus bonus : frame.getWorld().getBonuses()) {
            this.drawBonus(graphics, bonus);
        }

        Color color = graphics.getColor();
        Trooper[] troopers = frame.getWorld().getTroopers();
        Arrays.sort(troopers, new Comparator<Trooper>() {
            @Override
            public int compare(Trooper left, Trooper right) {
                return left.getY() > right.getY()
                        ? 1 : (left.getY() < right.getY()
                        ? -1 : (left.getX() > right.getX()
                        ? 1 : (left.getX() < right.getX()
                        ? -1 : 0)));
            }
        });

        for (Trooper trooper : troopers) {
            this.drawTrooper0(graphics, trooper);
            this.drawTrooper1(graphics, trooper);
            this.drawTrooper2(graphics, trooper);
        }

        this.drawLog(frame, graphics);
        graphics.setColor(color);
    }

    private void drawEffects(Frame frame, Graphics graphics) {
        Color color = graphics.getColor();

        for (Effect effect : frame.getWorld().getEffects()) {
            this.drawEffect(frame, graphics, effect);
        }

        graphics.setColor(color);
    }

    private void drawEffect(Frame frame, Graphics graphics, Effect effect) {
        if (effect.getAmount() == 0) {
            throw new IllegalArgumentException("Effect \'amount\' is zero.");
        } else {
            if (frame.getIndex() >= 2) {
                if (frame.getIndex() == 2) {
                    graphics.setFont(new Font("Courier New", Font.PLAIN, 12));
                } else {
                    graphics.setFont(new Font("Courier New", Font.BOLD, 16));
                }

                FontMetrics metrics = graphics.getFontMetrics();
                int x = effect.getX();
                int y = effect.getY() == 0 ? 1 : effect.getY() - 1;
                String s;
                switch (effect.getType()) {
                    case HITPOINTS_CHANGE:
                        graphics.setColor(effect.getAmount() > 0 ? Color.GREEN : Color.RED);
                        s = (effect.getAmount() > 0 ? "+" : "") + effect.getAmount();
                        graphics.drawString(s, (int) (((double) x + 0.5D) * (double) this.scaleX - 0.5D * (double) metrics.stringWidth(s)), (int) (((double) y + 0.5D) * (double) this.scaleY - 0.3D * (double) metrics.getHeight()));
                        break;
                    case ACTION_POINTS_CHANGE:
                        graphics.setColor(Color.BLACK);
                        s = (effect.getAmount() > 0 ? "+" : "") + effect.getAmount();
                        graphics.drawString(s, (int) (((double) x + 0.5D) * (double) this.scaleX - 0.5D * (double) metrics.stringWidth(s)), (int) (((double) y + 0.5D) * (double) this.scaleY + 0.7D * (double) metrics.getHeight()));
                        break;
                    default:
                        throw new IllegalArgumentException("Unsupported effect type: " + effect.getType() + '.');
                }
            }
        }
    }

    private void drawTrooper0(Graphics graphics, Trooper trooper) {
        Color color = getColorOfPlayer(trooper.getPlayerId());
        graphics.setColor(color);
        graphics.fillArc((int) (((double) trooper.getX() + 0.01D) * (double) this.scaleX), (int) (((double) trooper.getY() + 0.01D) * (double) this.scaleY), (int) ((double) this.scaleX * 0.8D), (int) ((double) this.scaleY * 0.8D), 0, 360);
        if (trooper.getHitpoints() < trooper.getMaximalHitpoints()) {
            double hitpointsPercents = (double) trooper.getHitpoints() / (double) trooper.getMaximalHitpoints();
            graphics.setColor(Color.WHITE);
            graphics.fillRect(trooper.getX() * this.scaleX, trooper.getY() * this.scaleY, this.scaleX, (int) ((0.01D + 0.8D * (1.0D - hitpointsPercents)) * (double) this.scaleY));
            graphics.setColor(color);
        }

        graphics.drawArc((int) (((double) trooper.getX() + 0.01D) * (double) this.scaleX), (int) (((double) trooper.getY() + 0.01D) * (double) this.scaleY), (int) ((double) this.scaleX * 0.8D), (int) ((double) this.scaleY * 0.8D), 0, 360);
        Vector2d from = VectorHelper.rotate(new Vector2d(0.4D, 0.0D), 0 - 0.3490658503988659D);
        Vector2d to = VectorHelper.rotate(new Vector2d(0.4D, 0.0D), 0 + 0.3490658503988659D);
        graphics.fillArc((int) (((double) trooper.getX() + 0.5D + from.x - 0.5D * 0.32D) * (double) this.scaleX), (int) (((double) trooper.getY() + 0.5D + from.y - 0.5D * 0.32D) * (double) this.scaleY), (int) ((double) this.scaleX * 0.32D), (int) ((double) this.scaleY * 0.32D), 0, 360);
        graphics.fillArc((int) (((double) trooper.getX() + 0.5D + to.x - 0.5D * 0.32D) * (double) this.scaleX), (int) (((double) trooper.getY() + 0.5D + to.y - 0.5D * 0.32D) * (double) this.scaleY), (int) ((double) this.scaleX * 0.32D), (int) ((double) this.scaleY * 0.32D), 0, 360);
        graphics.setColor(Color.BLACK);
        double x;
        double y;
        if (trooper.isHoldingGrenade()) {
            x = ((double) trooper.getX() + 0.25D - 0.12D) * (double) this.scaleX;
            y = (double) (trooper.getY() * this.scaleY);
            graphics.fillArc((int) x, (int) y, (int) ((double) this.scaleX * 0.12D * 2.0D), (int) ((double) this.scaleY * 0.12D * 2.0D), 0, 360);
        }

        if (trooper.isHoldingMedikit()) {
            x = ((double) trooper.getX() + 0.5D - 0.12D) * (double) this.scaleX;
            y = (double) (trooper.getY() * this.scaleY);
            graphics.fillRect((int) x, (int) y, (int) ((double) this.scaleX * 0.12D * 2.0D), (int) ((double) this.scaleY * 0.12D * 2.0D));
        }

        if (trooper.isHoldingFieldRation()) {
            x = ((double) trooper.getX() + 0.75D - 0.12D) * (double) this.scaleX;
            y = (double) (trooper.getY() * this.scaleY);
            graphics.fillPolygon(new int[]{(int) x, (int) (x + 0.12D * (double) this.scaleX), (int) (x + 2.0D * 0.12D * (double) this.scaleX), (int) (x + 0.12D * (double) this.scaleX)}, new int[]{(int) (y + 0.12D * (double) this.scaleY), (int) y, (int) (y + 0.12D * (double) this.scaleY), (int) (y + 2.0D * 0.12D * (double) this.scaleY)}, 4);
        }

    }

    private void drawLog(Frame frame, Graphics graphics) {
        Trooper trooper = frame.getWorld().getTrooper();
        if (trooper != null) {
            graphics.setColor(TRANSPARENT);
            if (frame.getIndex() % 2 == 0) {
                this.drawTrooperName(graphics, trooper);
            }

            Move move = frame.getWorld().getMove();
            if (move != null) {
                if (frame.getIndex() == 0) {
                    if (!this.textLog.getText().isEmpty()) {
                        this.textLog.append(String.format("%n"));
                    }

                    this.textLog.append(String.format("%s (%d, %d) of \'%s\' makes action \'%s\'",
                            toString(trooper.getType()),
                            trooper.getX(),
                            trooper.getY(),
                            getPlayerName(frame, trooper),
                            toString(move.getAction())));
                }

                Location location;
                if (move.getDirection() != null) {
                    location = new Location(trooper.getX() + move.getDirection().getOffsetX(), trooper.getY() + move.getDirection().getOffsetY());
                } else {
                    if (move.getX() < 0 || move.getX() >= frame.getWorld().getWidth() || move.getY() < 0 || move.getY() >= frame.getWorld().getHeight()) {
                        if (frame.getIndex() == 0) {
                            this.textLog.append(".");
                        }

                        return;
                    }

                    location = new Location(move.getX(), move.getY());
                }

                if (frame.getIndex() == 0) {
                    this.textLog.append(String.format(" (%d, %d).", location.getFirst(), location.getSecond()));
                }

                this.drawShoot(graphics, frame, trooper, move, location);
            }
        }
    }

    private static String getPlayerName(Frame frame, Trooper trooper) {
        for (Player player : frame.getWorld().getPlayers()) {
            if (trooper.getPlayerId() == player.getId()) {
                return player.getName();
            }
        }

        return null;
    }

    private static String toString(Enum e) {
        String str = e.name().toLowerCase().replace('_', ' ');
        return str.length() > 1
                ? Character.toUpperCase(str.charAt(0)) + str.substring(1)
                : Character.toString(Character.toUpperCase(str.charAt(0)));
    }

    private void drawShoot(Graphics graphics, Frame frame, Trooper trooper, Move move, Location location) {
        BasicStroke stroke = new BasicStroke(2.0F);
        if (move.getAction() == ActionType.SHOOT || move.getAction() == ActionType.THROW_GRENADE) {
            drawLine(graphics, (double) (location.getFirst() * this.scaleX), (double) (location.getSecond() * this.scaleY), ((double) location.getFirst().intValue() + 1.0D) * (double) this.scaleX, (double) (location.getSecond() * this.scaleY), stroke);
            drawLine(graphics, (double) (location.getFirst() * this.scaleX), ((double) location.getSecond().intValue() + 1.0D) * (double) this.scaleY, ((double) location.getFirst().intValue() + 1.0D) * (double) this.scaleX, ((double) location.getSecond().intValue() + 1.0D) * (double) this.scaleY, stroke);
            drawLine(graphics, (double) (location.getFirst() * this.scaleX), (double) (location.getSecond() * this.scaleY), (double) (location.getFirst() * this.scaleX), ((double) location.getSecond().intValue() + 1.0D) * (double) this.scaleY, stroke);
            drawLine(graphics, ((double) location.getFirst().intValue() + 1.0D) * (double) this.scaleX, (double) (location.getSecond() * this.scaleY), ((double) location.getFirst().intValue() + 1.0D) * (double) this.scaleX, ((double) location.getSecond().intValue() + 1.0D) * (double) this.scaleY, stroke);
            drawLine(graphics, ((double) location.getFirst().intValue() - 0.1D) * (double) this.scaleX, ((double) location.getSecond().intValue() + 0.5D) * (double) this.scaleY, ((double) location.getFirst().intValue() + 0.1D) * (double) this.scaleX, ((double) location.getSecond().intValue() + 0.5D) * (double) this.scaleY, stroke);
            drawLine(graphics, ((double) location.getFirst().intValue() + 0.9D) * (double) this.scaleX, ((double) location.getSecond().intValue() + 0.5D) * (double) this.scaleY, ((double) location.getFirst().intValue() + 1.1D) * (double) this.scaleX, ((double) location.getSecond().intValue() + 0.5D) * (double) this.scaleY, stroke);
            drawLine(graphics, ((double) location.getFirst().intValue() + 0.5D) * (double) this.scaleX, ((double) location.getSecond().intValue() - 0.1D) * (double) this.scaleY, ((double) location.getFirst().intValue() + 0.5D) * (double) this.scaleX, ((double) location.getSecond().intValue() + 0.1D) * (double) this.scaleY, stroke);
            drawLine(graphics, ((double) location.getFirst().intValue() + 0.5D) * (double) this.scaleX, ((double) location.getSecond().intValue() + 0.9D) * (double) this.scaleY, ((double) location.getFirst().intValue() + 0.5D) * (double) this.scaleX, ((double) location.getSecond().intValue() + 1.1D) * (double) this.scaleY, stroke);
            graphics.fillArc((int) (((double) location.getFirst().intValue() + 0.5D - 0.1D) * (double) this.scaleX), (int) (((double) location.getSecond().intValue() + 0.5D - 0.1D) * (double) this.scaleY), (int) (0.2D * (double) this.scaleX), (int) (0.2D * (double) this.scaleY), 0, 360);
            if (frame.getIndex() > 1) {
                BasicStroke stroke1 = new BasicStroke(0.5F, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER, 10.0F, new float[]{0.1F, 5.0F}, 0.0F);
                graphics.setColor(Color.BLACK);
                drawLine(graphics, ((double) trooper.getX() + 0.5D) * (double) this.scaleX, ((double) trooper.getY() + 0.5D) * (double) this.scaleY, ((double) location.getFirst().intValue() + 0.5D) * (double) this.scaleX, ((double) location.getSecond().intValue() + 0.5D) * (double) this.scaleY, stroke1);
            }
        }

    }

    private void drawTrooperName(Graphics graphics, Trooper trooper) {
        if (trooper.getY() > 0) {
            graphics.fillPolygon(new int[]{trooper.getX() * this.scaleX, (trooper.getX() + 1) * this.scaleX, (int) (((double) trooper.getX() + 0.5D) * (double) this.scaleX)}, new int[]{(int) (((double) trooper.getY() - 0.5D) * (double) this.scaleY), (int) (((double) trooper.getY() - 0.5D) * (double) this.scaleY), trooper.getY() * this.scaleY}, 3);
        } else {
            graphics.fillPolygon(new int[]{trooper.getX() * this.scaleX, (int) (((double) trooper.getX() + 0.5D) * (double) this.scaleX), (trooper.getX() + 1) * this.scaleX}, new int[]{(int) (((double) trooper.getY() + 1.5D) * (double) this.scaleY), (trooper.getY() + 1) * this.scaleY, (int) (((double) trooper.getY() + 1.5D) * (double) this.scaleY)}, 3);
        }

    }

    private void drawTrooper1(Graphics graphics, Trooper trooper) {
        Color color = getColorOfPlayer(trooper.getPlayerId());
        graphics.setColor(color);
        int x = (int) (((double) trooper.getY() + 1.0D - 0.25D * (double) (trooper.getStance().ordinal() + 1)) * (double) this.scaleY);
        drawLine(graphics,
                (double) (trooper.getX() * this.scaleX),
                (double) x,
                (double) ((int) (((double) trooper.getX() + 0.09999999999999998D) * (double) this.scaleX)),
                (double) x);
        drawLine(graphics,
                (double) ((int) (((double) trooper.getX() + 1.0D - 0.09999999999999998D) * (double) this.scaleX)),
                (double) x,
                (double) ((trooper.getX() + 1) * this.scaleX),
                (double) x);
    }

    private void drawTrooper2(Graphics graphics, Trooper trooper) {
        graphics.setColor(Color.BLACK);
        graphics.setFont(new Font("Courier New", 0, 12));
        FontMetrics metrics = graphics.getFontMetrics();
        String str = trooperName(trooper) + trooper.getActionPoints();
        graphics.drawString(str,
                (int) (((double) trooper.getX() + 0.5D) * (double) this.scaleX - 0.5D * (double) metrics.stringWidth(str)),
                (int) (((double) trooper.getY() + 0.5D) * (double) this.scaleY + 0.25D * (double) metrics.getHeight()));
    }

    private static String trooperName(Trooper trooper) {
        switch (trooper.getType()) {
            case COMMANDER:
                return "Co";
            case FIELD_MEDIC:
                return "Me";
            case SOLDIER:
                return "So";
            case SNIPER:
                return "Sn";
            case SCOUT:
                return "Sc";
            default:
                throw new IllegalArgumentException("Unsupported trooper type: " + trooper.getType() + '.');
        }
    }

    private void drawBonus(Graphics graphics, Bonus bonus) {
        double offset = 0.15000000000000002D;
        double fromX = ((double) bonus.getX() + offset) * (double) this.scaleX;
        double fromY = ((double) bonus.getY() + offset) * (double) this.scaleY;
        double toX = fromX + 0.7D * (double) this.scaleX;
        double toY = fromY + 0.7D * (double) this.scaleY;

        switch (bonus.getType()) {
            case GRENADE:
                this.drawGrenade(graphics, fromX, fromY, toX, toY, bonus);
                break;
            case MEDIKIT:
                this.drawMedikit(graphics, fromX, fromY, toX, toY);
                break;
            case FIELD_RATION:
                this.drawFieldRation(graphics, fromX, fromY, toX, toY);
                break;
            default:
                throw new IllegalArgumentException("Unsupported bonus type: " + bonus.getType() + '.');
        }
    }

    private void drawGrenade(Graphics graphics, double fromX, double fromY, double toX, double toY, Bonus bonus) {
        graphics.drawArc((int) fromX, (int) fromY, (int) ((double) this.scaleX * 0.7D), (int) ((double) this.scaleY * 0.7D), 0, 360);
        graphics.drawArc((int) (fromX + 0.25D * (double) this.scaleX * 0.7D), (int) (fromY + 0.25D * (double) this.scaleY * 0.7D), (int) (0.5D * (double) this.scaleX * 0.7D + 1.0D), (int) (0.5D * (double) this.scaleY * 0.7D + 1.0D), 0, 360);
        drawLine(graphics, 0.5D * (fromX + toX), (double) (bonus.getY() * this.scaleY), 0.5D * (fromX + toX), toY);
        drawLine(graphics, fromX, 0.5D * (fromY + toY), toX, 0.5D * (fromY + toY));
    }

    private void drawMedikit(Graphics graphics, double fromX, double fromY, double toX, double toY) {
        drawLine(graphics, fromX, fromY, toX, fromY);
        drawLine(graphics, toX, fromY, toX, toY);
        drawLine(graphics, toX, toY, fromX, toY);
        drawLine(graphics, fromX, toY, fromX, fromY);
        drawLine(graphics, 0.5D * (fromX + toX), fromY + 0.175D * (double) this.scaleY, 0.5D * (fromX + toX), toY - 0.175D * (double) this.scaleY);
        drawLine(graphics, fromX + 0.175D * (double) this.scaleX, 0.5D * (fromY + toY), toX - 0.175D * (double) this.scaleX, 0.5D * (fromY + toY));
    }

    private void drawFieldRation(Graphics graphics, double fromX, double fromY, double toX, double toY) {
        drawLine(graphics, fromX, fromY, toX, fromY);
        drawLine(graphics, toX, fromY, toX, toY);
        drawLine(graphics, toX, toY, fromX, toY);
        drawLine(graphics, fromX, toY, fromX, fromY);
        drawLine(graphics, fromX, 0.5D * (fromY + toY), toX, 0.5D * (fromY + toY));
        drawLine(graphics, fromX + 0.2333333333333333D * (double) this.scaleX, fromY, fromX + 0.2333333333333333D * (double) this.scaleX, toY);
        drawLine(graphics, fromX + 0.4666666666666666D * (double) this.scaleX, fromY, fromX + 0.4666666666666666D * (double) this.scaleX, toY);
    }

    private void drawHUD(WorldAdapter world, Graphics graphics) {
        graphics.setFont(new Font("Courier New", Font.BOLD, 15));
        Player[] players = world.getPlayers();
        Arrays.sort(players, new Comparator<Player>() {
            @Override
            public int compare(Player left, Player right) {
                return Long.valueOf(left.getId()).compareTo(right.getId());
            }
        });
        if (players.length > 0) {
            drawScore(graphics, 20.0D, 30.0D, players[0]);
        }

        if (players.length > 1) {
            drawScore(graphics, 20.0D, 60.0D, players[1]);
        }

        if (players.length > 2) {
            drawScore(graphics, 235.0D, 30.0D, players[2]);
        }

        if (players.length > 3) {
            drawScore(graphics, 235.0D, 60.0D, players[3]);
        }

        if (players.length > 4) {
            drawScore(graphics, 450.0D, 30.0D, players[4]);
        }

        if (players.length > 5) {
            drawScore(graphics, 450.0D, 60.0D, players[5]);
        }

        String moveIndex = String.valueOf(world.getMoveIndex());
        if (moveIndex.length() < 20) {
            moveIndex = Strings.repeat(" ", 20 - moveIndex.length()) + moveIndex;
        }

        graphics.drawString(moveIndex, (int) ((double) this.panel.getWidth() - 210.0D), (int) ((double) this.panel.getHeight() - 30.0D));
        String speed = "Speed: " + this.getIntervalName();
        if (speed.length() < 20) {
            speed = Strings.repeat(" ", 20 - speed.length()) + speed;
        }

        graphics.drawString(speed, (int) ((double) this.panel.getWidth() - 210.0D), 30);
        String FPS = "FPS: " + this.FPS.get();
        if (FPS.length() < 20) {
            FPS = Strings.repeat(" ", 20 - FPS.length()) + FPS;
        }

        graphics.drawString(FPS, (int) ((double) this.panel.getWidth() - 210.0D), 55);
    }

    private String getIntervalName() {
        long interval = this.intervalIndex.get();
        int intervalIndex = Arrays.binarySearch(INTERVALS, interval);
        switch (intervalIndex) {
            case 0:
                return "fast forward";
            case 1:
                return "very fast";
            case 2:
                return "fast";
            case 3:
                return "normal";
            case 4:
                return "slow";
            case 5:
                return "very slow";
            case 6:
                return "slideshow";
            default:
                throw new IllegalStateException(String.format("Illegal current screen interval index %d for interval %d.", intervalIndex, interval));
        }
    }

    private static void drawScore(Graphics graphics, double x, double y, Player player) {
        Color color = graphics.getColor();
        graphics.setColor(getColorOfPlayer(player.getId()));
        graphics.drawString(
                String.format("%-17s: %d", (player.isStrategyCrashed() ? "? " : "") + player.getName(),
                        player.getScore()), (int) x, (int) y);
        graphics.setColor(color);
    }

    private static void drawLine(Graphics graphics, double fromX, double fromY, double toX, double toY, Stroke stroke) {
        if (graphics instanceof Graphics2D) {
            Graphics2D graphics2D = (Graphics2D) graphics;
            Stroke old = null;
            if (stroke != null) {
                old = graphics2D.getStroke();
                graphics2D.setStroke(stroke);
            }

            graphics.drawLine((int) fromX, (int) fromY, (int) toX, (int) toY);
            if (stroke != null) {
                graphics2D.setStroke(old);
            }

        } else {
            throw new IllegalArgumentException("Argument \'graphics\' is not instance of Graphics2D.");
        }
    }

    private static void drawLine(Graphics graphics, double fromX, double fromY, double toX, double toY) {
        graphics.drawLine((int) fromX, (int) fromY, (int) toX, (int) toY);
    }

    private static Color getColorOfPlayer(long id) {
        switch (Ints.checkedCast(id)) {
            case 1:
                return Color.RED;
            case 2:
                return Color.BLUE;
            case 3:
                return Color.CYAN;
            case 4:
                return Color.ORANGE;
            default:
                throw new IllegalArgumentException("Can\'t get color for Player {id=" + id + "}.");
        }
    }

    public MovableAdapter getMovable() {
        return this.movable;
    }

    static final class Frame {

        private final WorldAdapter world;
        private final int index;


        private Frame(WorldAdapter world, int index) {
            this.world = world;
            this.index = index;
        }

        public WorldAdapter getWorld() {
            return this.world;
        }

        public int getIndex() {
            return this.index;
        }

    }

    public static final class MovableAdapter implements Movable {

        private Move move;
        // $FF: synthetic field
        final ScreenRenderer renderer;


        public MovableAdapter(ScreenRenderer renderer) {
            this.renderer = renderer;
        }

        public Move move() {
            this.renderer.lock.lock();

            Move move;
            try {
                this.renderer.condition.awaitUninterruptibly();
                move = this.move;
            } finally {
                this.renderer.lock.unlock();
            }

            return move;
        }

        public void setMove(ActionType action, Direction direction, Integer x, Integer y) {
            this.renderer.lock.lock();

            try {
                this.move = new Move();
                this.move.setAction(action);
                if (direction != null) this.move.setDirection(direction);
                if (x != null) this.move.setX(x);
                if (y != null) this.move.setY(y);
                this.renderer.condition.signalAll();
            } finally {
                this.renderer.lock.unlock();
            }

        }

    }
}
