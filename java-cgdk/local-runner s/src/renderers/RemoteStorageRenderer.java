package renderers;

import adapters.WorldAdapter;
import com.google.gson.GsonBuilder;
import core.helpers.WorldHelper;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.zip.GZIPOutputStream;

public class RemoteStorageRenderer implements Renderer {

    private final String storage;
    private WorldAdapter world;
    private int frame;
    private StringBuilder content = new StringBuilder();


    public RemoteStorageRenderer(String storage) throws IOException {
        this.storage = storage;
        this.create("");
    }

    public void render(WorldAdapter worldAdapter) throws IOException {
        this.content.append(WorldHelper.serializeWorld(worldAdapter, this.world)).append('\n');
        this.world = worldAdapter;
        ++this.frame;
        if (this.ensure(worldAdapter)) {
            this.append("", false);
        }

    }

    private boolean ensure(WorldAdapter worldAdapter) {
        return this.world != null && this.world.getMoveIndex() != worldAdapter.getMoveIndex() && worldAdapter.getMoveIndex() % 2 == 0;
    }

    private void create(String uri) throws IOException {
        String fullUri = this.storage + uri + "/begin";
        DefaultHttpClient httpClient = null;
        HttpPost post = null;
        HttpResponse response = null;

        try {
            httpClient = createHttpClient();
            post = new HttpPost(fullUri);
            response = httpClient.execute(post);
            if (response.getStatusLine().getStatusCode() != 200) {
                throw new IOException(String.format("Got unexpected response code %d from remote storage \'%s\' while creating new document.", response.getStatusLine().getStatusCode(), fullUri));
            }
        } finally {
            shutdown(httpClient, post, response);
        }

    }

    private void append(String uri, boolean close) throws IOException {
        append(this.content.toString(), this.storage, uri, close);
        this.content = new StringBuilder();
    }

    private static void append(String message, String storage, String uri, boolean close) throws IOException {
        String fullUri = storage + uri + '/' + (close ? "end" : "append");
        DefaultHttpClient httpClient = null;
        HttpPost post = null;
        HttpResponse response = null;

        try {
            httpClient = createHttpClient();
            post = new HttpPost(fullUri);
            ByteArrayOutputStream outputStream = null;
            GZIPOutputStream gzipOutputStream = null;
            ByteArrayInputStream inputStream = null;

            InputStreamEntity streamEntity;
            try {
                byte[] buffer = message.getBytes("UTF-8");
                outputStream = new ByteArrayOutputStream();
                gzipOutputStream = new GZIPOutputStream(outputStream);
                gzipOutputStream.write(buffer);
                gzipOutputStream.close();
                inputStream = new ByteArrayInputStream(outputStream.toByteArray());
                streamEntity = new InputStreamEntity(inputStream, (long) outputStream.size());
            } catch (UnsupportedEncodingException e) {
                throw new IllegalStateException("UTF-8 is unsupported.", e);
            } finally {
                IOUtils.closeQuietly(outputStream);
                IOUtils.closeQuietly(gzipOutputStream);
                IOUtils.closeQuietly(inputStream);
            }

            post.addHeader(new BasicHeader("Content-Encoding", "gzip"));
            post.setEntity(streamEntity);
            response = httpClient.execute(post);
            if (response.getStatusLine().getStatusCode() != 200) {
                throw new IOException(String.format("Got unexpected response code %d from remote storage \'%s\' while appending document.", new Object[]{response.getStatusLine().getStatusCode(), fullUri}));
            }
        } finally {
            shutdown(httpClient, post, response);
        }

    }

    private static void shutdown(HttpClient httpClient, HttpPost post, HttpResponse response) {
        if (response != null) {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                try {
                    entity.getContent().close();
                } catch (Exception ignored) {
                }
            }
        }

        if (post != null) post.abort();
        if (httpClient != null) httpClient.getConnectionManager().shutdown();
    }

    private static DefaultHttpClient createHttpClient() {
        BasicHttpParams params = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(params, (short) 10000);
        HttpConnectionParams.setSoTimeout(params, (short) 20000);
        return new DefaultHttpClient(params);
    }

    public void close() throws IOException {
        this.append("", true);

        try {
            this.create("-meta");
            append((new GsonBuilder()).create().toJson(new Object() {
                @SuppressWarnings("UnusedDeclaration")
                private final int frame = RemoteStorageRenderer.this.frame;
            }), this.storage, "-meta", true);
        } catch (RuntimeException ignored) {
        }

    }
}
