package renderers;

import adapters.WorldAdapter;

import java.io.Closeable;
import java.io.IOException;

public interface Renderer extends Closeable {

    void render(WorldAdapter worldAdapter) throws IOException;
}
