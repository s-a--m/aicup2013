package renderers;

import adapters.WorldAdapter;
import core.helpers.WorldHelper;
import org.apache.commons.io.FileUtils;

import java.io.*;

public class FileRenderer implements Renderer {

    private final Writer writer;
    private WorldAdapter world;


    public FileRenderer(File file) throws IOException {
        File parent = file.getParentFile();
        if (parent != null) {
            FileUtils.forceMkdir(parent);
        }

        this.writer = new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(file, false)), "UTF-8");
    }

    public void render(WorldAdapter worldAdapter) throws IOException {
        this.writer.write(WorldHelper.serializeWorld(worldAdapter, this.world));
        this.world = worldAdapter;
        this.writer.write('\n');
    }

    public void close() throws IOException {
        this.writer.close();
    }
}
