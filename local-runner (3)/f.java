package com.a.b.a.a.e.a.a;

import com.a.b.a.a.e.a.g;
import com.google.common.primitives.Ints;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.io.IOUtils;

public abstract class f
  implements e
{
  private static final ByteOrder a = ByteOrder.LITTLE_ENDIAN;
  private final AtomicBoolean b = new AtomicBoolean();
  private final int c;
  private ServerSocket d;
  private Socket e;
  private InputStream f;
  private OutputStream g;
  private final ByteArrayOutputStream h;
  private final File i;
  private OutputStream j;

  protected f(Map paramMap, File paramFile)
  {
    if (Boolean.parseBoolean((String)paramMap.get("debug")))
      this.c = Ints.checkedCast(TimeUnit.MINUTES.toMillis(10L));
    else
      this.c = Ints.checkedCast(TimeUnit.SECONDS.toMillis(10L));
    this.h = new ByteArrayOutputStream(1048576);
    this.i = paramFile;
  }

  public void a(int paramInt)
  {
    try
    {
      this.d = new ServerSocket(paramInt);
      this.d.setSoTimeout(this.c);
      this.d.setReceiveBufferSize(1048576);
    }
    catch (IOException localIOException)
    {
      throw new g(String.format("Can't start %s.", new Object[] { getClass() }), localIOException);
    }
  }

  public void a()
  {
    IOUtils.closeQuietly(this.e);
    try
    {
      this.e = this.d.accept();
      this.e.setSoTimeout(this.c);
      this.e.setSendBufferSize(1048576);
      this.e.setReceiveBufferSize(1048576);
      this.e.setTcpNoDelay(true);
    //  System.out.println("tcp no delay");           
      this.f = this.e.getInputStream();
      this.g = this.e.getOutputStream();
      if (this.i != null)
        this.j = new FileOutputStream(this.i);
    }
    catch (IOException localIOException)
    {
      throw new g("Can't accept remote process connection.", localIOException);
    }
  }

  protected final void a(Object paramObject, boolean paramBoolean)
  {
    if (paramObject == null)
    {
      if (paramBoolean)
        c(-1);
    }
    else
    {
      int k = Array.getLength(paramObject);
      if (paramBoolean)
        c(k);
      Class localClass = paramObject.getClass().getComponentType();
      for (int m = 0; m < k; m++)
      {
        Object localObject = Array.get(paramObject, m);
        if (localClass.isArray())
          a(localObject, paramBoolean);
        else if (localClass.isEnum())
          a((Enum)localObject);
        else if (localClass == String.class)
          a((String)localObject);
        else if (localObject == null)
          a(false);
        else if ((localClass == Boolean.class) || (localClass == Boolean.TYPE))
          a(((Boolean)localObject).booleanValue());
        else if ((localClass == Integer.class) || (localClass == Integer.TYPE))
          c(((Integer)localObject).intValue());
        else if ((localClass == Long.class) || (localClass == Long.TYPE))
          a(((Long)localObject).longValue());
        else if ((localClass == Double.class) || (localClass == Double.TYPE))
          a(((Double)localObject).doubleValue());
        else
          throw new IllegalArgumentException("Unsupported array item class: " + localClass + '.');
      }
    }
  }

  protected final Enum a(Class paramClass)
  {
    int k = d(1)[0];
    Enum[] arrayOfEnum = (Enum[])paramClass.getEnumConstants();
    int m = 0;
    int n = arrayOfEnum.length;
    while (m < n)
    {
      Enum localEnum = arrayOfEnum[m];
      if (localEnum.ordinal() == k)
        return localEnum;
      m++;
    }
    return null;
  }

  protected final void a(Enum paramEnum)
  {
    a(new byte[] { paramEnum == null ? -1 : com.a.a.a.d.f.a(paramEnum.ordinal()) });
  }

  protected final String g()
  {
    int k = i();
    if (k < 0)
      return null;
    try
    {
      return new String(d(k), "UTF-8");
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      throw new g("UTF-8 is unsupported.", localUnsupportedEncodingException);
    }
  }

  protected final void a(String paramString)
  {
    if (paramString == null)
    {
      c(-1);
      return;
    }
    byte[] arrayOfByte;
    try
    {
      arrayOfByte = paramString.getBytes("UTF-8");
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      throw new g("UTF-8 is unsupported.", localUnsupportedEncodingException);
    }
    c(arrayOfByte.length);
    a(arrayOfByte);
  }

  protected final boolean h()
  {
    return d(1)[0] != 0;
  }

  protected final void a(boolean paramBoolean)
  {
    a(new byte[] { paramBoolean ? (byte)1 : (byte)0 });
  }

  protected final int i()
  {
    return ByteBuffer.wrap(d(4)).order(a).getInt();
  }

  protected final void c(int paramInt)
  {
    a(ByteBuffer.allocate(4).order(a).putInt(paramInt).array());
  }

  protected final void a(long paramLong)
  {
    a(ByteBuffer.allocate(8).order(a).putLong(paramLong).array());
  }

  protected final void a(double paramDouble)
  {
    a(Double.doubleToLongBits(paramDouble));
  }

  protected final byte[] d(int paramInt)
  {
    k();
    try
    {
      return IOUtils.toByteArray(this.f, paramInt);
    }
    catch (IOException localIOException)
    {
      throw new g(String.format("Can't read %d bytes from input stream.", new Object[] { Integer.valueOf(paramInt) }), localIOException);
    }
  }

  protected final void a(byte[] paramArrayOfByte)
  {
    k();
    try
    {
      this.h.write(paramArrayOfByte);
    }
    catch (IOException localIOException)
    {
      throw new g(String.format("Can't write %d bytes into output stream.", new Object[] { Integer.valueOf(paramArrayOfByte.length) }), localIOException);
    }
  }

  protected final void j()
  {
    k();
    try
    {
      byte[] arrayOfByte = this.h.toByteArray();
      this.h.reset();
      this.g.write(arrayOfByte);
      this.g.flush();
      if (this.j != null)
        this.j.write(arrayOfByte);
    }
    catch (IOException localIOException)
    {
      throw new g("Can't flush output stream.", localIOException);
    }
  }

  private void k()
  {
    if (this.b.get())
      throw new IllegalStateException(String.format("%s is stopped.", new Object[] { getClass() }));
  }

  public void f()
  {
    if (!this.b.compareAndSet(false, true))
      return;
    IOUtils.closeQuietly(this.j);
    IOUtils.closeQuietly(this.e);
    IOUtils.closeQuietly(this.d);
  }
}

/* Location:           /home/sss/Загрузки/aicup/local-runner (3)/local-runner.jar
 * Qualified Name:     com.a.b.a.a.e.a.a.f
 * JD-Core Version:    0.6.2
 */